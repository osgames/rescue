package rescue;

import rescue.spaceobjects.*;

import java.util.Vector;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.awt.Graphics;
import java.util.Hashtable;
import java.net.URL;

public class MissionObject {

    private String type;// THIS IS THE TYPE OF OBJECT
    private String name;// THIS IS THE _TYPE_ OF _THAT_ OBJECT
    private int number;
    public static final int infoImageSize=150;

    private Vector myObjects;
    //private Ship player;

    private Vector names;
    private Hashtable URLs; // NEW LOAD LATER SYSTEM
    private Hashtable images;
    private Hashtable icons;

    private Vector types;
    private Vector amounts;

    private int min;
    private int max;
    private int presetCadet;
    private int presetLieutenant;
    private int presetCaptain;
    private int presetAdmiral;

    private int INFOwarp;
    private double INFOimpulse;
    private int INFOjump;
    private int INFOturn;

    private int INFOphaser;
    private int INFOtorpedos;
    private int INFOtractor;
    private int INFOcloak;

    private int INFOmaxtotalenergy;
    private int INFOmaxphaserbanks;
    private int INFOmaxtorpedosleft;
    private int INFOmaxshieldpower;

    private URL bigImageURL;
    private URL smallImageURL;
    private URL infoImageURL;

    private BufferedImage imageA;
    private BufferedImage imageB;
    private BufferedImage imageC;
    private BufferedImage imageD;

    public MissionObject(String n, URL i,URL ismall, URL info, String num, String t) {

	name=n;
	number= Integer.parseInt( num );
	type=t;

	names =  new Vector();

	types = new Vector();
	amounts = new Vector();

	images = new Hashtable();
	icons = new Hashtable();
	URLs = new Hashtable();

	bigImageURL = i;
	smallImageURL = ismall;
	infoImageURL = info;

	// ##### DEFAULT VALUE

/*
 	INFOwarp=4;
	INFOimpulse=1;
	INFOjump=1;
	INFOturn=1;

	INFOphaser=1;
	INFOtorpedos=1;
	INFOtractor=50;
	INFOcloak=0;

	INFOmaxtotalenergy=10000;
	INFOmaxphaserbanks=2500;
	INFOmaxtorpedosleft=18;
	INFOmaxshieldpower=50;
*/

 	INFOwarp=0;
	INFOimpulse=0;
	INFOjump=1;
	INFOturn=1;

	INFOphaser=0;
	INFOtorpedos=0;
	INFOtractor=0;
	INFOcloak=0;

	INFOmaxtotalenergy=0;
	INFOmaxphaserbanks=0;
	INFOmaxtorpedosleft=0;
	INFOmaxshieldpower=0;

    }

    public void loadImagesForSetup() throws IOException {

	if (imageA==null) {

		imageA = ImageIO.read( RescueIO.getInputStream(bigImageURL) );
		imageB = ImageIO.read( RescueIO.getInputStream(smallImageURL) );

		{
			imageC = new BufferedImage(20,20,BufferedImage.TYPE_INT_ARGB); // BGR
			Graphics g = imageC.getGraphics();
			g.drawImage(imageA,0,0,20,20,null);
			g.drawImage(imageB,20-imageB.getWidth(),20-imageB.getHeight(),null);
		}

	}

    }

    public void loadImagesForGame() throws IOException {

	for (int c=0;c<names.size();c++) {

		String a = (String)names.get(c);

		URL url = (URL)URLs.get(a);

		if (url!=null) {

			BufferedImage iA = ImageIO.read( RescueIO.getInputStream(url) );

			BufferedImage iC = new BufferedImage(20,20,BufferedImage.TYPE_INT_ARGB); // BGR
			Graphics g = iC.getGraphics();
			g.drawImage(iA,0,0,20,20,null);
			g.drawImage(imageB,20-imageB.getWidth(),20-imageB.getHeight(),null);

			images.put(a,iA);
			icons.put(a,iC);

		}

	}

	if (infoImageURL!=null) {

		imageD = ImageIO.read( RescueIO.getInputStream(infoImageURL) );

	}
	else {

		imageD = new BufferedImage(infoImageSize,infoImageSize,BufferedImage.TYPE_INT_BGR); // BGR
		Graphics g = imageD.getGraphics();
		g.drawImage(imageA,0,0,infoImageSize,infoImageSize,null);

	}


    }

    public BufferedImage getIcon() {
	return imageC;
    }

    public void setMinMax(String n,String m) {
	min=Integer.parseInt(n);
	max=Integer.parseInt(m);
    }

    public void setPreset(String n,String v) {

	if (n.equals("cadet")) {
	    presetCadet=Integer.parseInt(v);
	}
	else if (n.equals("lieutenant")) {
	    presetLieutenant=Integer.parseInt(v);
	}
	else if (n.equals("captain")) {
	    presetCaptain=Integer.parseInt(v);
	}
	else if (n.equals("admiral")) {
	    presetAdmiral=Integer.parseInt(v);
	}

    }

    public int getCurrentPreset(int a) {

	if (a==1) {

	    return presetCadet;

	}
	else if (a==2) {

	    return presetLieutenant;

	}
	else if (a==3) {

	    return presetCaptain;

	}
	//else if (a==4) {

	    return presetAdmiral;

	//}

    }

    public void setNumber(int n) {
	number=n;
    }
    public int getNumber() {
	return number;
    }

    public void addLike(String a,String b) {

	types.add(a);
	amounts.add( new Integer(b) );

    }

    public void addName(String a, URL b) throws Exception {
	names.add(a);
	if (b!=null) {
		URLs.put(a,b);
	}
    }

    // ###################### SETTING INFO

    public void setInfoWarp(String a) {
	INFOwarp = Integer.parseInt(a);
    }
    public void setInfoImpulse(String a) {
	INFOimpulse = Double.parseDouble(a);
    }
    public void setInfoJump(String a) {
	INFOjump = Integer.parseInt(a);
    }
    public void setInfoTurn(String a) {
	INFOturn = Integer.parseInt(a);
    }


    public void setInfoPhaser(String a) {
	INFOphaser = Integer.parseInt(a);
    }
    public void setInfoTorpedos(String a) {
	INFOtorpedos = Integer.parseInt(a);
    }

    public void setInfoTractor(String a) {
	INFOtractor = Integer.parseInt(a);
    }
    public void setInfoCloak(String a) {
	INFOcloak = Integer.parseInt(a);
    }


    public void setInfoMaxTotalEnergy(String a) {
	INFOmaxtotalenergy = Integer.parseInt(a);
    }
    public void setInfoMaxPhaserBanks(String a) {
	INFOmaxphaserbanks = Integer.parseInt(a);
    }
    public void setInfoMaxTorpedosLeft(String a) {
	INFOmaxtorpedosleft = Integer.parseInt(a);
    }
    public void setInfoMaxShieldPower(String a) {
	INFOmaxshieldpower = Integer.parseInt(a);
    }

    public int makeSpaceObjects(ImageObject[] v, boolean cluster, int position, Vector t) {

/*
	System.out.print("bob 1 "+so.size()+"\n");
	try {
	BufferedImage highlight1 = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_BGR );
	highlight1.getGraphics().drawImage(image,0,0,null);
	BufferedImage highlight2 = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_4BYTE_ABGR  );
	java.awt.image.RescaleOp HighLight = new java.awt.image.RescaleOp(1.5f, 1.0f, null);
	HighLight.filter( highlight1 , highlight2 );
	//image.getGraphics().drawImage(highlight2,0,0,null);
	image=highlight2;
	}
	catch(Exception e) { System.out.print(e.getMessage() +"\n"); }
	System.out.print("bob 2\n");
*/

	  myObjects = new Vector();
	  java.util.Enumeration e = names.elements();
	  String n="";

	  int x=(int)(Math.random() * RunRescue.WIDTH);
	  int y=(int)(Math.random() * RunRescue.HEIGHT);

	  for(int i=0; i< number ; i++) {

	    ImageObject so=null;

	    if (cluster && type.equals("ship")) {
		x=x+(int)(Math.random() * RunRescue.WIDTH/10)-RunRescue.WIDTH/20;
		y=y+(int)(Math.random() * RunRescue.HEIGHT/10)-RunRescue.HEIGHT/20;
	    }
	    else {
		x=(int)(Math.random() * RunRescue.WIDTH);
		y=(int)(Math.random() * RunRescue.HEIGHT);
	    }

	    BufferedImage iA = imageA;
	    BufferedImage iB = imageB;
	    BufferedImage iC = imageC;
	    BufferedImage iD = imageD;

	    if (number==1) { n=name; }
	    else if ( e.hasMoreElements() ) {
		n= (String)e.nextElement();
		BufferedImage newImage = (BufferedImage)images.get(n);
		if (newImage!=null) {

		    iA = newImage;
		    iC = (BufferedImage)icons.get(n);

	    	    iD = new BufferedImage(infoImageSize,infoImageSize,BufferedImage.TYPE_INT_BGR);
	    	    Graphics g = iD.getGraphics();
	    	    g.drawImage(iA,0,0,infoImageSize,infoImageSize,null);


		}
	    }
	    else { n=name+" "+(i+1); }

	    if (type.equals("ship")) {
		so = new Ship(position,name,n,x,y,v,iA,iB,iC,iD,t);

		//if (type.equals("player")) {
		    //player = (Ship)so;
		    //player.setPlayer(true);
		//}

		((Ship)so).setInfo(INFOwarp,INFOimpulse,INFOjump,INFOturn);

	    }
	    else if (type.equals("base")) {
		so = new Base(position,name,n,x,y,v,iA,iB,iC,iD,t);
	    }
	    else if (type.equals("planet")) {
		so = new Planet(position,name,n,x,y,v,iA,iB,iC,iD,t);
	    }
	    else if (type.equals("wormhole")) {
		so = new Wormhole(position,name,n,x,y,v,iA,iB,iC,iD,t);
	    }

	    if (so instanceof MannedObject) {
		((MannedObject)so).setInfo(INFOphaser,INFOtorpedos,INFOtractor,INFOcloak,INFOmaxtotalenergy,INFOmaxphaserbanks,INFOmaxtorpedosleft,INFOmaxshieldpower);
	    }

	    v[position] = so; position++;
	    myObjects.add( so );
	  }

	return position;

    }

    public void setLikes(ImageObject[] v) {

	//Vector tmp = new Vector();

	int[] tmp = new int[v.length];

	//for(int i=0; i< v.length ; i++) {
	    //tmp.add( new Integer(0) );
	    //tmp[i]=0;
	//}

	String who;
	int amount;

	for(int i=0; i< types.size() ; i++) {

	    who=(String)types.elementAt(i);
	    amount=((Integer)amounts.elementAt(i)).intValue();

	    for(int c=0; c< v.length ; c++) {

		if (v[c] instanceof MannedObject && ((MannedObject)v[c]).getType().equals(who) ) {
		    tmp[c] = amount;
		}

	    }
	}


	for(int i=0; i< myObjects.size() ; i++) {

	    if (myObjects.elementAt(i) instanceof MannedObject) { ((MannedObject)myObjects.elementAt(i)).setLikes(tmp); }

	}

    }

    //public Ship getPlayer() {
	//return player;
    //}
    public String getName() {
	return name;
    }

    public boolean isAllies(String p) {

	for(int i=0; i< types.size() ; i++) {

	    if ( ((String)types.elementAt(i)).equals( p ) ) {

		if ( ((Integer)amounts.elementAt(i)).intValue() >0 ) { return true; }
		else { return false; }

	    }

	}
	return false;

    }

    public boolean isEnemies(String p) {

	for(int i=0; i< types.size() ; i++) {

	    if ( ((String)types.elementAt(i)).equals( p ) ) {

		if ( ((Integer)amounts.elementAt(i)).intValue() <0 ) { return true; }
		else { return false; }

	    }

	}
	return false;

    }

    public int getMin() {
	return min;
    }
    public int getMax() {
	return max;
    }

    public int getCurrentRank(String p,int a) {


	if ( isEnemies(p) ) { // ememie or planet (whitich is friend but its harder to play with more as more work is needed)

	    if (presetAdmiral == 0 && a > 0) { return 8; } // big score for borg cube
	    if (presetAdmiral == 0 && a == 0) { return -1; } // ignore this score
	    if (a >= presetAdmiral) { return 4; }
	    if (a >= presetCaptain) { return 3; }
	    if (a >= presetLieutenant) { return 2; }
	    if (a >= presetCadet) { return 1; }
	    return 0;

	}

	return -1;

/*
	if ( type.equals("planet") ) {
	    return -1;
	}
	else if (isAllies(p)) { // friend

	    if (a <= presetAdmiral) { return 14; }
	    if (a <= presetCaptain) { return 13; }
	    if (a <= presetLieutenant) { return 12; }
	    if (a <= presetCadet) { return 11; }  
	    return 10;

	}
*/


    }

}
