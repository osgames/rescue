package rescue;

/**
 * <p> Rescue Listener </p>
 * @author Yura Mamyrin
 */

public interface RescueListener {

    public void update();
    public void newGame(MissionLoader cm);
    public void closeGame();
    public void dead();

}
