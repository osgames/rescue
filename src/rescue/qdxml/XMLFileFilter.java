package rescue.qdxml;

import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.ResourceBundle;

public class XMLFileFilter extends FileFilter {

    public XMLFileFilter() {
    }

    public boolean accept(File pathname) {
   	// The name of a directory is also acceptable
   	if(pathname.isDirectory()) {
	    return true;
	}
   	String fileName = pathname.getName().toLowerCase();
   	if(fileName.endsWith(".xml"))
      	return true;
      else
      	return false;
    }

    public String getDescription() {
   	return "XML Mission for Rescue! Max";
    }
}
