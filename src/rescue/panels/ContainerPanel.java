package rescue.panels;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.Point;

public class ContainerPanel extends JPanel {

    private Point defaultlocation;
    private double ratio;

    // for creating top leval containers
    public ContainerPanel(String a,String b,String c) {

	setName(a);
	defaultlocation = new Point( Integer.parseInt(b), Integer.parseInt(c) );

	setLayout(new java.awt.BorderLayout() );

    }

    // for creating containers inside containers
    public ContainerPanel() {

	setLayout(new java.awt.BorderLayout() );

    }

    public Point getPreferredLocation() {

	return defaultlocation;

    }

    public void setBounds(int x,int y,int width,int height) {

	if (ratio==0) {

	    Dimension d = getPreferredSize();

	    ratio=d.getWidth()/d.getHeight();

	}

	if (width == getWidth() && height == getHeight() ){}

	else if (width > getWidth() ) { height=(int)(width/ratio); }
	else if (height > getHeight() ) { width=(int)(height*ratio); }

	else if (width < getWidth() && height <= (int)(width/ratio) ) { height=(int)(width/ratio); }
	else if (height < getHeight() && width <= (int)(height*ratio) ) { width=(int)(height*ratio); }

	else { return; }

	super.setBounds(x,y,width,height);

    }

}
