package rescue.panels;

import rescue.*;

import java.awt.Canvas;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SmallMapPanel extends JPanel implements MouseListener {

    private RunRescue rescue;

    public SmallMapPanel(RunRescue rr,int w, int h) {

	rescue=rr;

	this.addMouseListener(this);

	Dimension size = new Dimension(w,h);

	setMinimumSize(size);
	setMaximumSize(size);
	setPreferredSize(size);

    }

    public void paintComponent(Graphics g) {

	g.setColor(Color.BLACK);
	g.fillRect(0,0,getWidth(),getHeight());

	g.setColor( Color.WHITE );

	if (rescue.getShip() !=null && rescue.getShip().getSystem(5)==0) {

	    g.drawLine((getWidth()/2),(getHeight()/2)+10,(getWidth()/2),(getHeight()/2)-10);
	    g.drawLine((getWidth()/2)+10,(getHeight()/2),(getWidth()/2)-10,(getHeight()/2));

	    rescue.drawShipsSmall(g,getWidth(),getHeight());

	}
	else {

	    g.drawString("-- OFFLINE --",(getWidth()/2)-30,(getHeight()/2)+3);

	}

    }

    public void list() {

	repaint();

    }

    //**********************************************************************
    //                     MouseListener Interface
    //**********************************************************************

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {

	boolean rightClick = (e.getModifiers() == java.awt.event.InputEvent.BUTTON1_MASK);
	boolean middleClick = (e.getModifiers() == java.awt.event.InputEvent.BUTTON2_MASK );
	boolean leftClick = ( e.getModifiers() == java.awt.event.InputEvent.BUTTON3_MASK );

	if (rightClick || leftClick) {
	    rescue.shipWarp( e.getX(),e.getY(),getWidth(),getHeight());
	}
	if (leftClick) {
	    rescue.shipWarpOn();
	}
	else if (middleClick) {

	    double angle = Math.atan( ((double)(e.getX()-(getWidth()/2)))/((double)((getHeight()/2)-e.getY())) );

	    if ( e.getY() > (getHeight()/2) ) {
		angle = angle + Math.toRadians(180);
	    }
	    if (angle < 0) {
		angle = angle + Math.toRadians(360);
	    }

	    rescue.shipManWarp( angle );


	}

    }

}
