package rescue.panels;

import rescue.spaceobjects.*;
import rescue.*;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;

public class ShipInfoPanel extends JPanel {

    private ImageObject mySO;
    private RunRescue rescue;

    public ShipInfoPanel(int w, int h, RunRescue r) {

	rescue=r;

	Dimension size = new Dimension(w,h);

	setMinimumSize(size);
	setMaximumSize(size);
	setPreferredSize(size);

    }

    public void paintComponent(Graphics g) {

	g.setColor( Color.BLACK );
	g.fillRect(0,0,getWidth(),getHeight());

	g.setColor( Color.WHITE );

	int scan = rescue.getScan();

	if (mySO!=null && (mySO instanceof Wormhole || (scan >= ((MannedObject)mySO).getCloakPower())) && rescue.getShip().getSystem(5)==0 ) {

	    g.drawImage(mySO.getInfoImage() ,getWidth()-MissionObject.infoImageSize,getHeight()-MissionObject.infoImageSize,this);

	    g.drawString("Name: "+mySO.getName(),4,14);
	    g.drawString("Location: "+mySO.x+" "+mySO.y,4,29);
	    g.drawString("Distance: "+mySO.getDistanceToPlayer(),4,44);

	    if (!(mySO instanceof Wormhole)) {

		if (scan > 0) {

		    int a = ((MannedObject)mySO).getTotalEnergy();
		    int b = ((MannedObject)mySO).getShieldPower();

		    g.drawRect(2,48,9,124);
		    g.drawRect(14,48,9,124);

		    g.drawString("Energy: " + a + " Shields: " + b,27,170);

		    int acolor = (int)(123*( a / (double)((MannedObject)mySO).getMaxtotalenergy() ));
		    int bcolor = (int)(123*( b / (double)((MannedObject)mySO).getMaxshieldpower() ));

		    if (scan > 1) {

			g.drawString("Tpdos: "+((MannedObject)mySO).getTorpedosLeft()+" Phase: "+((MannedObject)mySO).getPhaserBanks(),27,155);

		    }

		    g.setColor(Color.GREEN.darker() );
		    g.fillRect(3,49+(123-acolor),8,acolor);
		    g.setColor(Color.YELLOW.darker() );
		    g.fillRect(15,49+(123-bcolor),8,bcolor);

		}

	    }

	}
	else {

	    g.drawString("-- NO OBJECT --",(getWidth()/2)-40,(getHeight()/2)+3);

	}

    }

    public void setSpaceObject(ImageObject so) {

	mySO = so;

    }

    public void list() {

	if (mySO!=null && !(mySO instanceof Wormhole) && !((MannedObject)mySO).isAlive() && !(mySO instanceof Planet)) { mySO=null; }

	// YURA:TODO should check if the ship has been killed
	// only need to repaint if SO has changed or info about it has changed
	repaint();

    }

}
