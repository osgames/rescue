package rescue.panels;

import rescue.*;
import rescue.spaceobjects.Ship;
import rescue.spaceobjects.ImageObject;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputListener;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.Point;
import javax.swing.JTextField;
import java.net.URL;
import java.util.Vector;
import javax.swing.Action;

public class InterfacePanel extends JPanel implements MouseInputListener {

    private RunRescue rescue;

    private MapButton[] buttonImages;
    private Vector infoTexts;
    private Vector infoBars;
    private int width;
    private int height;
    private int[][] map;

    private BufferedImage img;
    private BufferedImage imgON;
    private int cc;
    private int pp;
    private int tt;

    private infoText INFOTEXT_MAN_IMPULSE;
    private infoText INFOTEXT_MAN_WARP;

    private infoText INFOTEXT_IMPULSE_SPEED;
    private infoText INFOTEXT_IMPULSE_AUTO;
    private infoText INFOTEXT_WARP_SPEED;
    private infoText INFOTEXT_WARP_AUTO;

    private infoText INFOTEXT_PHASER;
    private infoText INFOTEXT_TORPEDO;

    private infoText INFOTEXT_SHIELDS_UP;
    private infoText INFOTEXT_SHIELDS_POWER;

    private infoText INFOTEXT_TOTAL_POWER;
    private infoText INFOTEXT_PHASER_BANKS;
    private infoText INFOTEXT_TORPEDOS_LEFT;

    private infoText INFOTEXT_SYSTEM_WARPDRIVE;
    private infoText INFOTEXT_SYSTEM_IMPULSEDRIVE;
    private infoText INFOTEXT_SYSTEM_PHASERBANKS;
    private infoText INFOTEXT_SYSTEM_TORPEDOTUBES;
    private infoText INFOTEXT_SYSTEM_LONGRANGESCAN;
    private infoText INFOTEXT_SYSTEM_SENSORSCAN;
    private infoText INFOTEXT_SYSTEM_SHIELDS;
    private infoText INFOTEXT_SYSTEM_COMPUTER;
    private infoText INFOTEXT_SYSTEM_TRANSPORTERS;
    private infoText INFOTEXT_SYSTEM_TRACTORBEAM;

    private infoText INFOTEXT_OUTPOSTS_RESCUED;
    private infoText INFOTEXT_OUTPOSTS_DESTROYED;
    private infoText INFOTEXT_OUTPOSTS_REMAINING;
    private infoText INFOTEXT_OTHERSHIPS_FRIENDLIES;
    private infoText INFOTEXT_OTHERSHIPS_ENEMIES;
    private infoText INFOTEXT_OTHERSHIPS_DESTROYED;

    private infoText INFOTEXT_COLONIES_ONBOARD;
    private infoText INFOTEXT_NUMBER_AWAY_TEAMS;

    private infoText INFOTEXT_SCAN;
    private infoText INFOTEXT_CONDITION;

    private infoBar INFOBAR_TOTAL_POWER;
    private infoBar INFOBAR_PHASER_BANKS;
    private infoBar INFOBAR_TORPEDOS_LEFT;
    private infoBar INFOBAR_TRACTOR_BEAM;
    private infoBar INFOBAR_SHIELD_POWER;

    public InterfacePanel(RunRescue rr,int w,int h) {
	this(rr);

	width = w;
	height = h;

    }

    public InterfacePanel(RunRescue rr) {

	rescue=rr;

	width = -1;
	height = -1;

	setLayout(null);

	//javax.swing.JLabel test = new javax.swing.JLabel("TEST");
	//test.setBounds(35,85,50,20);
	//add(test);

	cc=255;
	pp=255;
	tt=255;

	infoTexts = new Vector();
	infoBars = new Vector();

	map=null;

    }

/*
    public void setupImage(String i) {

	try {
	    backImage = ImageIO.read( new File(i) );
	}
	catch (Exception e) { }


	impulseMan = new JTextField("0");
	impulseMan.setBounds(64,29,25,13);
	impulseMan.setBorder(null);
	impulseMan.setOpaque(false);

	warpMan = new JTextField("0");
	warpMan.setBounds(164,29,25,13);
	warpMan.setBorder(null);
	warpMan.setOpaque(false);

	add(impulseMan);
	add(warpMan);

    }

    public int insideButton(Point p) {


	if ( map[p.x][p.y] != 255 ) { return map[p.x][p.y]; }

	Rectangle cr;

	for (int i = 0; i < Rectangles.size() ; i++) {

	    if ( ((Rectangle)Rectangles.elementAt(i)).contains(p) ) {
		return i;
	    }

	}

	return 0;

    }

    public void setManImpulse(double a) {

	impulseMan.setText( (int)( Math.toDegrees(a) )+""); // Math.round

    }


	if (cc < buttonImages.length ) {

	    if (pressed) { g.drawImage(buttonImages[cc].getDownImage() ,buttonImages[cc].getX1() ,buttonImages[cc].getY1() ,this); }
	    else { g.drawImage(buttonImages[cc].getUpImage() ,buttonImages[cc].getX1() ,buttonImages[cc].getY1() ,this); }

	}

	if (presseddown!=null) {
	    if (presseddown==highlight) { g.drawImage(presseddown.getDownImage() ,presseddown.getX1() ,presseddown.getY1() ,this); }
	}
	else if (highlight!=null) {
	    g.drawImage(highlight.getUpImage() ,highlight.getX1() ,highlight.getY1() ,this);
	}

*/

    public void setupImages(URL sin_on,URL sin_off,URL sin_up,URL sin_down,URL sin_map, int nob) throws Exception {

	BufferedImage in_on = ImageIO.read( RescueIO.getInputStream(sin_on) );

	if ((width != -1 && in_on.getWidth() != width) || (height != -1 && in_on.getHeight() != height)) {
	    throw new Exception("image \""+sin_on+"\" does not mach panel size");
	}


	width = in_on.getWidth();
	height = in_on.getHeight();

	Dimension size = new Dimension( width, height );

	setPreferredSize(size);
	setMinimumSize(size);
	setMaximumSize(size);


	BufferedImage in_off = ImageIO.read( RescueIO.getInputStream(sin_off) );

	img = in_off;
	imgON = in_on; // needed for infoBars

	if (nob > 0) {

	  addMouseListener(this);
	  addMouseMotionListener(this);

	  BufferedImage in_up = ImageIO.read( RescueIO.getInputStream(sin_up) );
	  BufferedImage in_down = ImageIO.read( RescueIO.getInputStream(sin_down) );
	  BufferedImage in_map = ImageIO.read( RescueIO.getInputStream(sin_map) );

	  //create map

	  int none = new Color(0, 0, 0, 0).getRGB();

	  //int noc = 6; // YURA: this number is unknown (10 means buttons go from 0-9)

	  int ppX = in_map.getWidth();
	  int ppY = in_map.getHeight();

	  map = new int[ppX][ppY];

	  buttonImages = new MapButton[nob];
	  for (int c=0; c < nob; c++) {
	      buttonImages[c] = new MapButton(ppX,ppY);
	  }

	  // create a very big 2d array with all the data from the image map
	  for(int y=0; y < ppY; y++) {

	    for(int x=0; x < ppX; x++) {

		int num= (in_map.getRGB(x,y))&0xff;

		// if ( num >= noc && num !=255 ) System.out.print("map error: "+x+" "+y+"\n"); // testing map

		if ( num < nob ) {

		    map[x][y]=num;

		    if (x < buttonImages[num].getX1() ) { buttonImages[num].setX1(x); }
		    if (x > buttonImages[num].getX2() ) { buttonImages[num].setX2(x); }

		    if (y < buttonImages[num].getY1() ) { buttonImages[num].setY1(y); }
		    if (y > buttonImages[num].getY2() ) { buttonImages[num].setY2(y); }
		}
		else {

		    map[x][y]=255;

		}

	    }
	  }

	  // create the bufferd image for each country
	  for (int c=0; c < buttonImages.length ; c++) {

	    int x1=buttonImages[c].getX1();
	    int x2=buttonImages[c].getX2();
	    int y1=buttonImages[c].getY1();
	    int y2=buttonImages[c].getY2();
	    int w=buttonImages[c].getWidth();
	    int h=buttonImages[c].getHeight();

	    //System.out.print( "Country: "+ c +" X1: "+ x1 +" Y1: "+y1 +" X2: "+x2 +" Y2: "+y2 +" Width: "+ w +" Height: "+ h +"\n");

	    BufferedImage i_off = new BufferedImage(w, h, java.awt.image.BufferedImage.TYPE_INT_ARGB );
	    i_off.getGraphics().drawImage( in_off.getSubimage(x1, y1, w, h) ,0,0,this);

	    BufferedImage i_on = new BufferedImage(w, h, java.awt.image.BufferedImage.TYPE_INT_ARGB );
	    i_on.getGraphics().drawImage( in_on.getSubimage(x1, y1, w, h) ,0,0,this);

	    BufferedImage i_up = new BufferedImage(w, h, java.awt.image.BufferedImage.TYPE_INT_ARGB );
	    i_up.getGraphics().drawImage( in_up.getSubimage(x1, y1, w, h) ,0,0,this);

	    BufferedImage i_down = new BufferedImage(w, h, java.awt.image.BufferedImage.TYPE_INT_ARGB );
	    i_down.getGraphics().drawImage( in_down.getSubimage(x1, y1, w, h) ,0,0,this);

	    buttonImages[c].setOffImage(i_off);
	    buttonImages[c].setOnImage(i_on);
	    buttonImages[c].setUpImage(i_up);
	    buttonImages[c].setDownImage(i_down);

	    for(int y=y1; y <= y2; y++) {
		for(int x=0; x <= w-1; x++) {
		    if (map[x+x1][y] != (c) ) {
			i_off.setRGB( x, (y-y1), none); // clear the un-needed area!
			i_on.setRGB( x, (y-y1), none); // clear the un-needed area!
			i_up.setRGB( x, (y-y1), none); // clear the un-needed area!
			i_down.setRGB( x, (y-y1), none); // clear the un-needed area!
		    }
		}
	    }

	  }

	}

    }

    // this method needs to throw because the getAction method throws runtime exceptions
    public void setCommandValue(String c,int v,String t) throws Exception {

	Action action = rescue.getAction(c);
	buttonImages[v].setAction(action);
	buttonImages[v].setToolTip(t);

    }

    public void setInfoText(String c,int x,int y,int w,int h,int textX,int textY,Color z) throws Exception {

	BufferedImage back = img.getSubimage(x,y,w,h);
	infoText v = new infoText(back,x,y,textX,textY,z);
	infoTexts.add(v);

	if (c.equals("impulse_speed")) {
	    INFOTEXT_IMPULSE_SPEED=v;
	}
	else if (c.equals("impulse_auto")) {
	    INFOTEXT_IMPULSE_AUTO=v;
	}
	else if (c.equals("warp_speed")) {
	    INFOTEXT_WARP_SPEED=v;
	}
	else if (c.equals("warp_auto")) {
	    INFOTEXT_WARP_AUTO=v;
	}
	else if (c.equals("phaser")) {
	    INFOTEXT_PHASER=v;
	}
	else if (c.equals("torpedo")) {
	    INFOTEXT_TORPEDO=v;
	}
	else if (c.equals("shields_up")) {
	    INFOTEXT_SHIELDS_UP=v;
	}
	else if (c.equals("shields_power")) {
	    INFOTEXT_SHIELDS_POWER=v;
	}
	else if (c.equals("total_energy")) {
	    INFOTEXT_TOTAL_POWER=v;
	}
	else if (c.equals("phaser_banks")) {
	    INFOTEXT_PHASER_BANKS=v;
	}
	else if (c.equals("torpedos_left")) {
	    INFOTEXT_TORPEDOS_LEFT=v;
	}
	else if (c.equals("system_warpdrive")) {
	    INFOTEXT_SYSTEM_WARPDRIVE=v;
	}
	else if (c.equals("system_impulsedrive")) {
	    INFOTEXT_SYSTEM_IMPULSEDRIVE=v;
	}
	else if (c.equals("system_phaserbanks")) {
	    INFOTEXT_SYSTEM_PHASERBANKS=v;
	}
	else if (c.equals("system_torpedotubes")) {
	    INFOTEXT_SYSTEM_TORPEDOTUBES=v;
	}
	else if (c.equals("system_longrangescan")) {
	    INFOTEXT_SYSTEM_LONGRANGESCAN=v;
	}
	else if (c.equals("system_sencorscan")) {
	    INFOTEXT_SYSTEM_SENSORSCAN=v;
	}
	else if (c.equals("system_shields")) {
	    INFOTEXT_SYSTEM_SHIELDS=v;
	}
	else if (c.equals("system_computer")) {
	    INFOTEXT_SYSTEM_COMPUTER=v;
	}
	else if (c.equals("system_transporters")) {
	    INFOTEXT_SYSTEM_TRANSPORTERS=v;
	}
	else if (c.equals("system_tractorbeam")) {
	    INFOTEXT_SYSTEM_TRACTORBEAM=v;
	}
	else if (c.equals("outposts_rescued")) {
	    INFOTEXT_OUTPOSTS_RESCUED=v;
	}
	else if (c.equals("outposts_destroyed")) {
	    INFOTEXT_OUTPOSTS_DESTROYED=v;
	}
	else if (c.equals("outposts_remaining")) {
	    INFOTEXT_OUTPOSTS_REMAINING=v;
	}
	else if (c.equals("otherships_friendlies")) {
	    INFOTEXT_OTHERSHIPS_FRIENDLIES=v;
	}
	else if (c.equals("otherships_enemies")) {
	    INFOTEXT_OTHERSHIPS_ENEMIES=v;
	}
	else if (c.equals("otherships_destroyed")) {
	    INFOTEXT_OTHERSHIPS_DESTROYED=v;
	}
	else if (c.equals("colonies_onboard")) {
	    INFOTEXT_COLONIES_ONBOARD=v;
	}
	else if (c.equals("number_away_teams")) {
	    INFOTEXT_NUMBER_AWAY_TEAMS=v;
	}
	else if (c.equals("scan")) {
	    INFOTEXT_SCAN=v;
	}
	else if (c.equals("condition")) {
	    INFOTEXT_CONDITION=v;
	}
	else if (c.equals("man_impulse")) {
	    INFOTEXT_MAN_IMPULSE=v;
	}
	else if (c.equals("man_warp")) {
	    INFOTEXT_MAN_WARP=v;
	}
	else {
	    throw new Exception("unknown infotext "+c);
	}

    }

    public void setInfoBar(String c,int x,int y,int w,int h,String d) throws Exception {

	BufferedImage i1 = img.getSubimage(x,y,w,h);
	BufferedImage i2 = imgON.getSubimage(x,y,w,h);
	infoBar v = new infoBar(i1,i2,x,y,d);
	infoBars.add(v);

	if (c.equals("total_energy")) {
	    INFOBAR_TOTAL_POWER=v;
	}
	else if (c.equals("phaser_banks")) {
	    INFOBAR_PHASER_BANKS=v;
	}
	else if (c.equals("torpedos_left")) {
	    INFOBAR_TORPEDOS_LEFT=v;
	}
	else if (c.equals("tractor_beam")) {
	    INFOBAR_TRACTOR_BEAM=v;
	}
	else if (c.equals("shield_power")) {
	    INFOBAR_SHIELD_POWER=v;
	}
	else {
	    throw new Exception("unknown infobar "+c);
	}


    }

    public void list() {

	Ship ship = rescue.getShip();
	Graphics g = getGraphics();


	if (buttonImages!=null) {

		for (int c=0;c<buttonImages.length;c++) {

			if (buttonImages[c].checkChanged() ) {

				g.drawImage(buttonImages[c].getImage() ,buttonImages[c].getX1() ,buttonImages[c].getY1() ,this);

			}
		}

	}


	// ############################# INFO TEXT




	if (INFOTEXT_IMPULSE_SPEED!=null && INFOTEXT_IMPULSE_SPEED.checkOn( ship.getImpulseSpeed() ) ) {

	    INFOTEXT_IMPULSE_SPEED.draw(g,true);

	}

	if (INFOTEXT_IMPULSE_AUTO!=null ) {

	    Point i = ship.getAutoImpulse();
	    String impulse;

	    if (i==null) {
		impulse = "  -- NOT SET --";
	    }
	    else if (i instanceof ImageObject) {
		impulse = ((ImageObject)i).getName();
		if (impulse.length() > 13) { impulse = impulse.substring(0,11) + "..."; }
	    }
	    else {
		impulse = i.x+","+i.y;
	    }

	    if ( INFOTEXT_IMPULSE_AUTO.checkOn( impulse ) ) {

		INFOTEXT_IMPULSE_AUTO.draw(g,true);
	    }

	}

	if (INFOTEXT_WARP_SPEED!=null && INFOTEXT_WARP_SPEED.checkOn( ship.getWarpSpeed() ) ) {

	    INFOTEXT_WARP_SPEED.draw(g,true);

	}

	if (INFOTEXT_WARP_AUTO!=null) {

	    Point w = ship.getAutoWarp();
	    String warp;

	    if (w==null) {
		warp = "  -- NOT SET --";
	    }
	    else if (w instanceof ImageObject) {
		warp = ((ImageObject)w).getName();
		if (warp.length() > 13) { warp = warp.substring(0,11) + "..."; }
	    }
	    else {
		warp = w.x+","+w.y;
	    }

	    if ( INFOTEXT_WARP_AUTO.checkOn( warp ) ) {

		INFOTEXT_WARP_AUTO.draw(g,true);

	    }

	}

	if (INFOTEXT_PHASER!=null && INFOTEXT_PHASER.checkOn( ship.getPhaserIntensity() ) ) {

	    INFOTEXT_PHASER.draw(g,true);

	}

	if (INFOTEXT_TORPEDO!=null && INFOTEXT_TORPEDO.checkOn( ship.getTorpedoSalvo() ) ) {

	    INFOTEXT_TORPEDO.draw(g,true);

	}

	if (INFOTEXT_SHIELDS_UP!=null && INFOTEXT_SHIELDS_UP.checkOn( (ship.getShieldsUp())?("ON"):("OFF") ) ) {

	    INFOTEXT_SHIELDS_UP.draw(g,true);

	}

	if (INFOTEXT_SHIELDS_POWER!=null && INFOTEXT_SHIELDS_POWER.checkOn( ((int)((ship.getShieldPower()/(double)ship.getMaxshieldpower())*100))+"" ) ) {

	    INFOTEXT_SHIELDS_POWER.draw(g,true);

	}

	if (INFOTEXT_TOTAL_POWER!=null && INFOTEXT_TOTAL_POWER.checkOn( ship.getTotalEnergy()+"" ) ) {

	    INFOTEXT_TOTAL_POWER.draw(g,true);

	}

	if (INFOTEXT_PHASER_BANKS!=null && INFOTEXT_PHASER_BANKS.checkOn( ship.getPhaserBanks()+"" ) ) {

	    INFOTEXT_PHASER_BANKS.draw(g,true);

	}

	if (INFOTEXT_TORPEDOS_LEFT!=null && INFOTEXT_TORPEDOS_LEFT.checkOn( ship.getTorpedosLeft()+"" ) ) {

	    INFOTEXT_TORPEDOS_LEFT.draw(g,true);

	}

	if (INFOTEXT_SYSTEM_WARPDRIVE!=null && INFOTEXT_SYSTEM_WARPDRIVE.checkOn( ship.getSystemText(1) ) ) {

	    INFOTEXT_SYSTEM_WARPDRIVE.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_IMPULSEDRIVE!=null && INFOTEXT_SYSTEM_IMPULSEDRIVE.checkOn( ship.getSystemText(2) ) ) {

	    INFOTEXT_SYSTEM_IMPULSEDRIVE.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_PHASERBANKS!=null && INFOTEXT_SYSTEM_PHASERBANKS.checkOn( ship.getSystemText(3) ) ) {

	    INFOTEXT_SYSTEM_PHASERBANKS.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_TORPEDOTUBES!=null && INFOTEXT_SYSTEM_TORPEDOTUBES.checkOn( ship.getSystemText(4) ) ) {

	    INFOTEXT_SYSTEM_TORPEDOTUBES.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_LONGRANGESCAN!=null && INFOTEXT_SYSTEM_LONGRANGESCAN.checkOn( ship.getSystemText(5) ) ) {

	    INFOTEXT_SYSTEM_LONGRANGESCAN.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_SENSORSCAN!=null && INFOTEXT_SYSTEM_SENSORSCAN.checkOn( ship.getSystemText(6) ) ) {

	    INFOTEXT_SYSTEM_SENSORSCAN.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_SHIELDS!=null && INFOTEXT_SYSTEM_SHIELDS.checkOn( ship.getSystemText(7) ) ) {

	    INFOTEXT_SYSTEM_SHIELDS.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_COMPUTER!=null && INFOTEXT_SYSTEM_COMPUTER.checkOn( ship.getSystemText(8) ) ) {

	    INFOTEXT_SYSTEM_COMPUTER.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_TRANSPORTERS!=null && INFOTEXT_SYSTEM_TRANSPORTERS.checkOn( ship.getSystemText(9) ) ) {

	    INFOTEXT_SYSTEM_TRANSPORTERS.draw(g,true);

	}
	if (INFOTEXT_SYSTEM_TRACTORBEAM!=null && INFOTEXT_SYSTEM_TRACTORBEAM.checkOn( ship.getSystemText(10) ) ) {

	    INFOTEXT_SYSTEM_TRACTORBEAM.draw(g,true);

	}

	if (INFOTEXT_OUTPOSTS_RESCUED!=null && INFOTEXT_OUTPOSTS_RESCUED.checkOn( rescue.getBaseNumbers(1)+"" ) ) {

	    INFOTEXT_OUTPOSTS_RESCUED.draw(g,true);

	}
	if (INFOTEXT_OUTPOSTS_DESTROYED!=null && INFOTEXT_OUTPOSTS_DESTROYED.checkOn( rescue.getBaseNumbers(2)+"" ) ) {

	    INFOTEXT_OUTPOSTS_DESTROYED.draw(g,true);

	}
	if (INFOTEXT_OUTPOSTS_REMAINING!=null && INFOTEXT_OUTPOSTS_REMAINING.checkOn( rescue.getBaseNumbers(3)+"" ) ) {

	    INFOTEXT_OUTPOSTS_REMAINING.draw(g,true);

	}
	if (INFOTEXT_OTHERSHIPS_FRIENDLIES!=null && INFOTEXT_OTHERSHIPS_FRIENDLIES.checkOn( rescue.getShipNumbers(1)+"" ) ) {

	    INFOTEXT_OTHERSHIPS_FRIENDLIES.draw(g,true);

	}
	if (INFOTEXT_OTHERSHIPS_ENEMIES!=null && INFOTEXT_OTHERSHIPS_ENEMIES.checkOn( rescue.getShipNumbers(2)+"" ) ) {

	    INFOTEXT_OTHERSHIPS_ENEMIES.draw(g,true);

	}
	if (INFOTEXT_OTHERSHIPS_DESTROYED!=null && INFOTEXT_OTHERSHIPS_DESTROYED.checkOn( rescue.getShipNumbers(3)+"" ) ) {

	    INFOTEXT_OTHERSHIPS_DESTROYED.draw(g,true);

	}

	if (INFOTEXT_COLONIES_ONBOARD!=null && INFOTEXT_COLONIES_ONBOARD.checkOn( ship.getNoColonies()+"" ) ) {

	    INFOTEXT_COLONIES_ONBOARD.draw(g,true);

	}
	if (INFOTEXT_NUMBER_AWAY_TEAMS!=null && INFOTEXT_NUMBER_AWAY_TEAMS.checkOn( ship.getNoAwayTeams()+"" ) ) {

	    INFOTEXT_NUMBER_AWAY_TEAMS.draw(g,true);

	}
	if (INFOTEXT_SCAN!=null && INFOTEXT_SCAN.checkOn( ship.getScan() ) ) {

	    INFOTEXT_SCAN.draw(g,true);

	}

	if (INFOTEXT_MAN_IMPULSE!=null && INFOTEXT_MAN_IMPULSE.checkOn( ship.getManImpulse()+"" ) ) {

	    INFOTEXT_MAN_IMPULSE.draw(g,true);

	}
	if (INFOTEXT_MAN_WARP!=null && INFOTEXT_MAN_WARP.checkOn( ship.getManWarp()+"" ) ) {

	    INFOTEXT_MAN_WARP.draw(g,true);

	}

	if (INFOTEXT_CONDITION!=null) {

	    int con = ship.getCondition();

	    String condition;
	    Color conColor;

	    if (con==1) {
		condition = " Green";
		conColor = Color.GREEN.darker().darker();
	    }
	    else if (con==2) {
		condition = " Yellow";
		conColor = Color.YELLOW;
	    }
	    else {
		condition = "* RED *";
		conColor = Color.RED.darker().darker();
	    }
	    

	    if (INFOTEXT_CONDITION.checkOn( condition ) ) {

		INFOTEXT_CONDITION.setColor( conColor );

		INFOTEXT_CONDITION.draw(g,true);
	    }

	}

	// ################### INFO BARS

	if (INFOBAR_TOTAL_POWER!=null && INFOBAR_TOTAL_POWER.checkOn( ship.getTotalEnergy(), ship.getMaxtotalenergy() ) ) {

	    INFOBAR_TOTAL_POWER.draw(g,true);

	}

	if (INFOBAR_PHASER_BANKS!=null && INFOBAR_PHASER_BANKS.checkOn( ship.getPhaserBanks(), ship.getMaxphaserbanks() ) ) {

	    INFOBAR_PHASER_BANKS.draw(g,true);

	}

	if (INFOBAR_TORPEDOS_LEFT!=null && INFOBAR_TORPEDOS_LEFT.checkOn( ship.getTorpedosLeft(), ship.getMaxtorpedosleft() ) ) {

	    INFOBAR_TORPEDOS_LEFT.draw(g,true);

	}

	if (INFOBAR_TRACTOR_BEAM!=null && INFOBAR_TRACTOR_BEAM.checkOn( ship.getTractorBeamPower(), ship.getTractorBeamMaxPower() ) ) {

	    INFOBAR_TRACTOR_BEAM.draw(g,true);

	}

	if (INFOBAR_SHIELD_POWER!=null && INFOBAR_SHIELD_POWER.checkOn( ship.getShieldPower(), ship.getMaxshieldpower() ) ) {

	    INFOBAR_SHIELD_POWER.draw(g,true);

	}

    }

    public void paintComponent(Graphics g) {

	// if something is on it should not highlight

	g.drawImage(img,0,0,this.getWidth(),this.getHeight(),this);

	if (map!=null) {

	  if (pp != 255) {

	    g.drawImage(buttonImages[pp].getDownImage() ,buttonImages[pp].getX1() ,buttonImages[pp].getY1() ,this);

	  }
	  else if (cc != 255) {

	    g.drawImage(buttonImages[cc].getUpImage() ,buttonImages[cc].getX1() ,buttonImages[cc].getY1() ,this);

	  }

	  for (int i = 0; i < buttonImages.length ; i++) {

	    if ( buttonImages[i].getOn() ) {

		g.drawImage(buttonImages[i].getImage() ,buttonImages[i].getX1() ,buttonImages[i].getY1() ,this);

	    }

	  }

	}

	for (int i = 0; i < infoTexts.size() ; i++) {
	    ((infoText)infoTexts.elementAt(i)).draw(g,false);
	}

	for (int i = 0; i < infoBars.size() ; i++) {
	    ((infoBar)infoBars.elementAt(i)).draw(g,false);
	}

    }

    public void command(int a) {

	Action action = buttonImages[a].getAction();

	if (action!=null) {

		// can not do this as some things need to be toggled with 1 button, like shields
		//if (action.isEnabled()) {

			action.actionPerformed(null);

			JukeBox.play("mouseclick");

		//}
	}
	else {

		System.out.print("action not set for color "+a+"\n");
	}



    }

    //**********************************************************************
    //                     MouseListener Interface
    //**********************************************************************

    public void mouseMoved(MouseEvent e) {

	int a = map[e.getX()][e.getY()];

	if (a != cc) {

	    Graphics g = getGraphics();

	    // cover up the old highlighted button
	    if (cc < buttonImages.length ) { g.drawImage(buttonImages[cc].getImage() ,buttonImages[cc].getX1() ,buttonImages[cc].getY1() ,this); }

	    cc=a;

	    // draw the new highlighted button
	    if (cc < buttonImages.length) { g.drawImage(buttonImages[cc].getUpImage() ,buttonImages[cc].getX1() ,buttonImages[cc].getY1() ,this); }

	}

    }

    public void mousePressed(MouseEvent e) {

	int a = map[e.getX()][e.getY()];

	if (a < buttonImages.length) {

	    Graphics g = getGraphics();

	    g.drawImage(buttonImages[a].getDownImage() ,buttonImages[a].getX1() ,buttonImages[a].getY1() ,this);

	    if (a < buttonImages.length) { pp=a; }
	}


    }

    public void mouseDragged(MouseEvent e) {

	int x=e.getX();
	int y=e.getY();

	if (x<0 || x>=img.getWidth() || y<0 || y>=img.getHeight() ) { return; }

	int a = map[x][y];

	if (a!=cc && pp!=255) {

	    Graphics g = getGraphics();

	    if (a!=pp) {
		g.drawImage(buttonImages[pp].getImage() ,buttonImages[pp].getX1() ,buttonImages[pp].getY1() ,this);
	    }
	    else {
		g.drawImage(buttonImages[pp].getDownImage() ,buttonImages[pp].getX1() ,buttonImages[pp].getY1() ,this);
	    }

	    cc=a;

	}

    }

    public void mouseReleased(MouseEvent e) {

	int x=e.getX();
	int y=e.getY();

	if (x<0 || x>=img.getWidth() || y<0 || y>=img.getHeight() ) { pp=255; return; }

	int a = map[x][y];

	if (a < buttonImages.length) {

	    if (a==pp) {

		command(a);
	    }

	    // this is needed for the close game command where this couses a nullpointer exception
	    if ( rescue.getShip() !=null ) { Graphics g = getGraphics(); g.drawImage(buttonImages[a].getUpImage() ,buttonImages[a].getX1() ,buttonImages[a].getY1() ,this); }

	}

	pp=255;

    }

    public void mouseExited(MouseEvent e) {

	if (cc!=255) {

	    Graphics g = getGraphics();

	    g.drawImage(buttonImages[cc].getImage() ,buttonImages[cc].getX1() ,buttonImages[cc].getY1() ,this);

	    cc=255;

	}

    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public boolean contains(int x, int y) {

      if (map!=null) {

	if (x<0 || x>=img.getWidth() || y<0 || y>=img.getHeight() ) { tt=255; return false; }
	int a = map[x][y];
	if (a==255) { tt=255; return false; }

	if (a!=tt) {
	    setToolTipText( buttonImages[a].getToolTip() );
	    tt=a;
	}

        return true;

      }

      return false;

    }

    class infoBar {

	private int x;
	private int y;
	private int value;
	private int direction;
	//private int outof;
	private BufferedImage on;
	private BufferedImage off;

	private int nx;
	private int ny;
	private int nw;
	private int nh;

	public infoBar(BufferedImage i1,BufferedImage i2,int ix,int iy,String d) throws Exception {

	    x=ix;
	    y=iy;
	    value=0;

	    if (d.equals("right")) { direction=1; }
	    else if (d.equals("left")) { direction=2; }
	    else if (d.equals("up")) { direction=3; }
	    else if (d.equals("down")) { direction=4; }
	    else { throw new Exception("unknows direction "+d); }

	    on=i2;
	    off=i1;

	}

	// public void setOutOf(int a) { outof=a; }

	public boolean checkOn(int a, int outof) {

	    if (value==a) {
		return false;
	    }
	    else {
		value=a;

		nx=0;
		ny=0;
		nw=on.getWidth();
		nh=on.getHeight();

		if (direction==1) {

		    nw = (int)(nw*((double)value/outof));

		}
		else if (direction==2) {

		    nw = (int)(nw*((double)value/outof));
		    nx = nx+(on.getWidth()-nw);

		}
		else if (direction==3) {

		    nh = (int)(nh*((double)value/outof));
		    ny = ny+(on.getHeight()-nh);

		}
		else if (direction==4) {

		    nh = (int)(nh*((double)value/outof));

		}

		return true;
	    }

	}

	public void draw(Graphics g, boolean clean) {

	    if (clean) { g.drawImage(off,x,y,InterfacePanel.this); }

	    if (nw!=0 && nh!=0) {
		g.drawImage(on.getSubimage(nx,ny,nw,nh),x+nx,y+ny,InterfacePanel.this);
	    }

	}

    }

    class infoText {

	private int x;
	private int y;

	private int textX;
	private int textY;

	private BufferedImage back;
	private String text;
	private Color color;

	public infoText(BufferedImage b,int i,int j,int ti,int tj,Color z) {

	    back=b;
	    text="";

	    x=i;
	    y=j;

	    textX=ti;
	    textY=tj;

	    color=z;

	}

	public boolean checkOn(String a) {

	    if (text.equals(a)) {
		return false;
	    }
	    else {
		text=a;
		return true;
	    }

	}

	public void draw(Graphics g, boolean a) {

	    if (a) { g.drawImage(back,x,y,InterfacePanel.this); }

	    g.setColor( color );

	    g.drawString( text ,textX, textY);

	}

	public void setColor(Color a) {

	    color=a;

	}

    }

    class MapButton {

	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private BufferedImage offImage;
	private BufferedImage onImage;
	private BufferedImage HighLightImage;
	private BufferedImage PressDownImage;

	private boolean on;
	private String tooltip;

	private Action action;

	public MapButton(int a,int b) {
	    x1=a;
	    y1=b;
	    x2=0;
	    y2=0;
	    onImage=null;
	    HighLightImage=null;
	    PressDownImage=null;

	}

	public String getToolTip() {

	    return tooltip;

	}

	public void setAction(Action a) {

	    action=a;

	}

	public Action getAction() {

	    return action;

	}

	public void setToolTip(String a) {

	    tooltip=a;

	}

	public boolean checkChanged() {

	    boolean a = (action!=null)?!action.isEnabled():false;

	    if (a!=on) {
		on=a;
		return true;
	    }
	    return false;

	}

	public boolean getOn() {
	    return on;
	}

	public void setOffImage(BufferedImage a) {
	    offImage=a;
	}

	public void setOnImage(BufferedImage a) {
	    onImage=a;
	}

	public void setUpImage(BufferedImage a) {
	    HighLightImage=a;
	}

	public void setDownImage(BufferedImage a) {
	    PressDownImage=a;
	}

	public void setX1(int a) {
	    x1=a;
	}

	public void setY1(int a) {
	    y1=a;
	}

	public void setX2(int a) {
	    x2=a;
	}

	public void setY2(int a) {
	    y2=a;
	}

	public BufferedImage getImage() {
	    if (on) { return onImage; }
	    return offImage;
	}

	public BufferedImage getUpImage() {
	    if (on) { return onImage; }
	    return HighLightImage;
	}

	public BufferedImage getDownImage() {
	    if (on) { return onImage; }
	    return PressDownImage;
	}

	public int getX1() {
	    return x1;
	}

	public int getY1() {
	    return y1;
	}

	public int getX2() {
	    return x2;
	}

	public int getY2() {
	    return y2;
	}

	public int getWidth() {
	    return (x2-x1+1);
	}

	public int getHeight() {
	    return (y2-y1+1);
	}


    }


}
