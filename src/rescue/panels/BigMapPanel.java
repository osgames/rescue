package rescue.panels;

import rescue.*;

import javax.swing.JPanel;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class BigMapPanel extends JPanel implements MouseListener,MouseWheelListener {

    private RunRescue rescue;

    private int posX;
    private int posY;

    public BigMapPanel(RunRescue rr,int w,int h) {

	rescue=rr;

	addMouseListener(this);
	addMouseWheelListener(this);

	posX=0;
	posY=0;

	Dimension size1 = new Dimension(w,h);

	setPreferredSize(size1);

    }

    public void paintComponent(Graphics g) {

	if ( rescue.mapUpdating() ) {

		int w=getWidth();
		int h=getHeight();

		g.setColor(Color.BLACK);
		g.fillRect(0,0,w,h);

		rescue.drawStars(g);
		rescue.drawShipsBig(g);

	}

    }

    public void setBounds(int x,int y,int width,int height) {

	super.setBounds(x,y,width,height);

	//System.out.print("resize: "+width+" "+height+"\n");

	rescue.setPanelSize(width,height);

    }

    //**********************************************************************
    //                     MouseListener Interface
    //**********************************************************************

    public void mouseWheelMoved(MouseWheelEvent e) {

	if (e.getWheelRotation() < 0) {

	    rescue.zoom(true);

	}
	else {

	    rescue.zoom(false);

	}

    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {

	boolean rightClick = (e.getModifiers() == java.awt.event.InputEvent.BUTTON1_MASK);
	boolean middleClick = (e.getModifiers() == java.awt.event.InputEvent.BUTTON2_MASK );
	boolean leftClick = (e.getModifiers() == java.awt.event.InputEvent.BUTTON3_MASK );

	if (rightClick || leftClick) {
	    rescue.shipImpulse(e.getX(),e.getY());
	}

	if (rightClick) {
	    rescue.selectShips(e.getX(),e.getY());
	}
	else if (leftClick) {
	    rescue.shipImpulseOn();
	}
	else if (middleClick) {

	    double angle = Math.atan( ((double)(e.getX()-(getWidth()/2)))/((double)((getHeight()/2)-e.getY())) );

	    if ( e.getY() > (getHeight()/2) ) {
		angle = angle + Math.toRadians(180);
	    }
	    if (angle < 0) {
		angle = angle + Math.toRadians(360);
	    }

	    rescue.shipManImpulse( angle );


	}

    }

}
