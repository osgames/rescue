package rescue.panels;

import rescue.*;
import rescue.spaceobjects.*;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.JList;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.Component;
import java.util.Vector;
import javax.swing.JScrollBar;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.util.Collections;
import java.awt.Point;

public class ShipsPanel extends JScrollPane implements MouseWheelListener {

    private RunRescue rescue;
    private JList ShipsBox;
    private Vector SpaceObjectPanels;
    private Vector theOthers;
    private int type;
    private int height=30;

    private int show;
    private int sort;

    public ShipsPanel(RunRescue rr,int w,int h,int t) {
	super(VERTICAL_SCROLLBAR_NEVER,HORIZONTAL_SCROLLBAR_NEVER);

	setWheelScrollingEnabled(false); // this only works if the scroll bars are visable

	type=t;

	rescue = rr;

	Dimension size1 = new Dimension(w,h);

	setPreferredSize(size1);

	ShipsBox = new JList();
	ShipsBox.setCellRenderer( new CustomCellRenderer() );
	ShipsBox.setFixedCellHeight(height);
	ShipsBox.setVisibleRowCount(4);
	ShipsBox.addMouseWheelListener(this);
	ShipsBox.setBackground( Color.BLACK );

	getViewport().setView(ShipsBox);

	setBorder(null);

	//refresh();

    }

    public void mouseWheelMoved(MouseWheelEvent e) {

	// This works but when the game is paused it does not update the gui (and the buttons do not disable when the end is reached)
	// calling scrollShip in rescue means that if the game is paused the gui will be updated
	//JScrollBar a = getVerticalScrollBar();
	//a.setValue( a.getValue()+(height*e.getWheelRotation()) );

	if (e.getWheelRotation()<0) {
	    rescue.scrollShips( ((type==1)?true:false) ,true);
	}
	else {
	    rescue.scrollShips( ((type==1)?true:false) ,false);
	}

    }

    public void newGame() {

	    SpaceObjectPanels = new Vector();
	    theOthers = new Vector();

	    ImageObject[] so = rescue.getSpaceObjects();
	    Ship player = rescue.getShip();

	    if (type==1) { theOthers.add( new ShipPanel(player) ); }

	    for (int i = 0; i < so.length ; i++) {

		ImageObject soAtI = so[i];

		if (type==1 && (soAtI instanceof Ship) && soAtI!=player) {

		    SpaceObjectPanels.add( new ShipPanel(soAtI) );

		}
		else if (type==2 && ((soAtI instanceof Planet)||(soAtI instanceof Base)||(soAtI instanceof Wormhole))) {

		    SpaceObjectPanels.add( new ShipPanel(soAtI) );

		}
	    }

	    ShipsBox.setListData(SpaceObjectPanels);

	sort = 0;
	show = 3;

    }

    public void list() {

	// YURA:TODO remove dead ships

	//if ( !rescue.getPause() ) { // no need to sort if game is paused THERE IS A NEED, IF TYPE OF SORT CHANGES

	    Object a = ShipsBox.getSelectedValue();
	    Collections.sort(SpaceObjectPanels);

	    //ShipsBox.setListData(SpaceObjectPanels); // not needed as im using the same vector
	    ShipsBox.setSelectedValue(a,false);
	    //ShipsBox.revalidate(); // reapint better?
	    ShipsBox.repaint();

	//if (type==1 && show!=3) {  }
	// used to be repaint();
	setShow(show);

    }

    public void scroll(boolean up) {

	//System.out.print( getVerticalScrollBar().getValue() + "\n");

	JScrollBar a = getVerticalScrollBar();
	int b = a.getValue();

	if (up) {

	    a.setValue(b-height);

	}
	else {

	    a.setValue(b+height);

	}

    }

    public boolean canScroll(boolean up) {

	int b = getVerticalScrollBar().getValue();

	//System.out.print(b+"\n");

	if (up) {

	    if (b==0) { return false; }
	    else { return true; }

	}
	else {

	    if (b==(height*(SpaceObjectPanels.size()-ShipsBox.getVisibleRowCount())) || SpaceObjectPanels.size() < ShipsBox.getVisibleRowCount()) { return false; }
	    else { return true; }

	}

    }

    public void setSort(int a) { // will need to resort when bording ship
	sort=a;
    }

    public void setShow(int a) {

	// YURA:TODO take into account the leval of the sensor scan (and when it changes
	// YURA:TODO what happens when someones starts to like you? this neds to be updated

	show=a;

	Vector tmp = new Vector();
	Ship player = rescue.getShip();
	boolean redo=false;
	int scan = rescue.getScan();

	// when likes change should this refresh?

	for (int i = 0; i < theOthers.size(); i++) {

	    // find good then add to tmp and remove from others

	    SpaceObject soAtI = ((ShipPanel)theOthers.elementAt(i)).getSO();

	    if ( !(soAtI instanceof Planet) && !(soAtI instanceof Wormhole) && !((MannedObject)soAtI).isAlive() ) {
		theOthers.remove(i); i--;
	    }
	    else if (type==1 && soAtI!=player && scan >= ((MannedObject)soAtI).getCloakPower() && player.getSystem(5)==0 &&

		    (
			(show==0 && ((MannedObject)soAtI).enemyOf( player.getNumber() ) ) ||
			(show==1 && ((MannedObject)soAtI).friendOf( player.getNumber() ) ) ||
			(show==2 && !((MannedObject)soAtI).enemyOf( player.getNumber() ) && !((MannedObject)soAtI).friendOf( player.getNumber() ) ) ||
			(show==3)
		    )

		) {

		tmp.add( theOthers.remove(i) ); i--; redo=true;

	    }
	    else if (type==2 && (soAtI instanceof Wormhole || scan >= ((MannedObject)soAtI).getCloakPower()) && player.getSystem(5)==0 &&

		    (
			(show==0 && (soAtI instanceof Planet) ) ||
			(show==1 && (soAtI instanceof Base) ) ||
			(show==2 && (soAtI instanceof Wormhole) ) ||
			(show==3)
		    )

		) {

		tmp.add( theOthers.remove(i) ); i--; redo=true;

	    }

	}
	for (int i = 0; i < SpaceObjectPanels.size(); i++) {

	    // find bad then remove and add to the others

	    SpaceObject soAtI = ((ShipPanel)SpaceObjectPanels.elementAt(i)).getSO();

	    if ( !(soAtI instanceof Planet) && !(soAtI instanceof Wormhole) && !((MannedObject)soAtI).isAlive() ) {
		SpaceObjectPanels.remove(i); i--; redo=true;
	    }
	    else if (type==1 &&

		    (
			player.getSystem(5)!=0 ||
			scan < ((MannedObject)soAtI).getCloakPower() ||
			(soAtI==player) ||
			(show==0 && !((MannedObject)soAtI).enemyOf( player.getNumber() ) ) ||
			(show==1 && !((MannedObject)soAtI).friendOf( player.getNumber() ) ) ||
			(show==2 && (((MannedObject)soAtI).enemyOf( player.getNumber() ) || ((MannedObject)soAtI).friendOf( player.getNumber() )) )
		    )

		) {

		theOthers.add( SpaceObjectPanels.remove(i) ); i--; redo=true;

	    }
	    else if (type==2 &&

		    (
			player.getSystem(5)!=0 ||
			(!(soAtI instanceof Wormhole) && scan < ((MannedObject)soAtI).getCloakPower()) ||
			(show==0 && !(soAtI instanceof Planet) ) ||
			(show==1 && !(soAtI instanceof Base) ) ||
			(show==2 && !(soAtI instanceof Wormhole) )
		    )

		) {

		theOthers.add( SpaceObjectPanels.remove(i) ); i--; redo=true;

	    }

	}

	if (redo) {

	    ShipsBox.clearSelection();
	    getVerticalScrollBar().setValue(0);

	    SpaceObjectPanels.addAll(tmp);
	    ShipsBox.revalidate();

	}

    }

    public int getSort() {
	return sort;
    }

    public int getShow() {
	return show;
    }

    public void clearSelection() {

	ShipsBox.clearSelection();

    }

    class CustomCellRenderer implements ListCellRenderer {
	public Component getListCellRendererComponent (JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

	    ShipPanel component = (ShipPanel)value;

	    if ( component.setSelected(isSelected) ) {

		component.setBackground(isSelected ? Color.RED : Color.BLACK ); // new Color(117,162,219) new Color(91,115,160)

		if (isSelected) { rescue.newSelection(type, component.getSO() ); }
		//component.setCellHasFocus(cellHasFocus);

	    }

	    return component;
	}
    }

    class ShipPanel extends JPanel implements Comparable {

	private BufferedImage pic;
	private ImageObject mySO;

	private boolean selected;

	public ShipPanel (ImageObject so) {

	    setBackground( Color.BLACK );

	    mySO = so;

	    pic = new BufferedImage(172, 24, BufferedImage.TYPE_3BYTE_BGR);
	    Graphics g = pic.getGraphics();

	    //((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	    g.setColor( Color.BLACK );
	    g.fillRect(0,0,200, 24);

	    g.setColor( Color.WHITE );

	    g.drawString( so.getName(), 24,10);

	    g.drawImage(so.getIcon(),2,2,this);

	}

	public void paintComponent(Graphics g) {

	    super.paintComponent(g);

	    //System.out.print("-");

	    g.drawImage(pic, 3, 3, this);
	    g.setColor( Color.WHITE );
	    g.drawString( mySO.getDistanceToPlayer()+"" , 27,26);

	    if (!(mySO instanceof Wormhole) && rescue.getScan() > 0) {

		g.drawRect(92,17,81,8);
		g.setColor( Color.GREEN.darker() );
		g.fillRect(93,18,(int)(80*(((MannedObject)mySO).getTotalEnergy()/(double)((MannedObject)mySO).getMaxtotalenergy())),7);

	    }

	}

	public ImageObject getSO() {
	    return mySO;
	}

	public boolean setSelected(boolean a) {

	    if (selected==a) { return false; }

	    selected=a;
	    return true;

	}

	public int compareTo(Object o) {

	    if (sort==0) { // smallest distance first

		int a = (int)(mySO.distance( rescue.getShip() ));
		int b = (int)((((ShipPanel)o).getSO()).distance( rescue.getShip() ));

		return a-b;

		//if (a<b) return -1;
		//if (a>b) return 1;
		//return 0;

	    }
	    else { // least energy

		int a = 0;
		int b = 0;


		if (mySO instanceof MannedObject) { a = ((MannedObject)mySO).getTotalEnergy(); }
		if ( ((ShipPanel)o).getSO() instanceof MannedObject ) { b = ((MannedObject)(((ShipPanel)o).getSO())).getTotalEnergy(); }

		return a-b;

	    }

	}

    }

}
