package rescue.gui;

import javax.swing.JApplet;
import rescue.RunRescue;
import java.net.URL;

/**
 * @author Yura Mamyrin
 */

public class RescueApplet extends JApplet {

	public void init() {

		try {

			RunRescue rr = new RunRescue( new URL(getCodeBase(),"missions/") );

			final RescueGUI gui = new RescueGUI(rr , getParameter("missions") );

        		setContentPane( gui );

			rr.addRescueListener(gui);

			new Thread(rr).start();
		}
		catch (Exception ex) {

			throw new RuntimeException(ex);
		}
	}

}
