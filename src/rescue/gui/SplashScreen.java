package rescue.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.ImageIcon;

public class SplashScreen extends Window {
    
    private ImageIcon screen;

    /**
     * Constructor.
     */
    public SplashScreen() {
        super( new Frame() );
        //setBackground(Color.white);

        java.net.URL url = this.getClass().getResource("splash.jpg");
	if (url != null) {
	    screen = new ImageIcon(url);
	    MediaTracker mt = new MediaTracker(this);
	    mt.addImage(screen.getImage(), 0);
	    try {
		mt.waitForAll();
	    } catch(Exception ex) {
	    }
	}
    }

    /**
     * Override the setVisible call.
     * @param val show or not show.
     */
    public void setVisible(boolean val) {
	if (screen == null) {
	    return;
	}
        if (val == true) {
            setSize(400, 300); // screen.getIconWidth(), screen.getIconHeight()
            setLocation(-500, -500);
            super.setVisible(true);
        
            Dimension d = getToolkit().getScreenSize();
            //Insets i = getInsets();
            //int w = 360 + i.left + i.right; // screen.getIconWidth()
            //int h = 160 + i.top + i.bottom; // screen.getIconHeight()
            //setSize(w, h);
            setLocation(d.width / 2 - 200, d.height / 2 - 150);
        } else {
            super.setVisible(false);
        }
    }

    /**
     * Override the paint call.
     * @param g the graphics context.
     */
    public void paint(Graphics g) {
        if (screen != null) {
            //Dimension d = getSize();

            g.drawImage(screen.getImage(), 0, 0, this);

	    g.drawString("ver."+rescue.RunRescue.VERSION,335,295);

            //g.setColor(Color.WHITE);
            //g.fillRect(147, 68, 165, 39);

            //g.setColor(Color.BLACK);
	    //g.drawString("by Yura Mamyrin (yura@yura.net)",148,78);
	    //g.drawString("copyright (c) 2004 yura.net",148,104);

        }
    }

}
