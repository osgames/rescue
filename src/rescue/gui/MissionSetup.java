package rescue.gui;

import rescue.*;
import rescue.qdxml.XMLFileFilter;

import java.awt.Cursor;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JSeparator;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import java.util.Vector;
import java.awt.Graphics;
import java.awt.Dimension;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.swing.JComboBox;
import java.awt.event.ItemListener;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import java.awt.Component;
import java.awt.event.ItemEvent;

/**
 * <p> Mission Dialog </p>
 * @author Yura Mamyrin
 */


public class MissionSetup extends JDialog implements ActionListener {

    private JPanel mainPanel;
    private RunRescue rescue;
    private MissionLoader currentMission;
    private GridBagConstraints gbc;
    private JRadioButton custom;
    private Box thebox;
    private JCheckBox clusterSimilarTypes;
    private Vector changlings;
    private JLabel missionRank;
    private String newGameOptions;

    // ONLY ONE OF THESE WILL EVER EXIST
    private JLabel missionName;
    private JComboBox missionsBox;

    public MissionSetup(Frame parent, boolean modal) {
        super(parent,"Mission Setup", modal);

	mainPanel = new JPanel();

        initGUI();

	getContentPane().add(mainPanel);

    }

    // use this constructor if you already have a mission loaded!
    // and to set the rescue object
    public MissionSetup(Frame parent, boolean modal, RunRescue r,String missions) {
        this(parent, modal);
	rescue=r;


	//###################################################################### TOP

	JPanel topPanel=null;

	if (missions==null) {

		topPanel = new JPanel();
		topPanel.setLayout( new FlowLayout() );

		JButton loadButton = new JButton("Load Mission");
		loadButton.setActionCommand("load");
		loadButton.addActionListener(this);
		topPanel.add(loadButton);

		topPanel.add( new JLabel("Current Mission Name:") );

		missionName = new JLabel();
		topPanel.add(missionName);

	}
	else {

		topPanel = makeDropDownTopPanel();

	}

	getContentPane().add(topPanel,BorderLayout.NORTH);

	//###################################################################### TOP end

	if (missions==null) {

		loadMission( rescue.getMission() );

	}
	else {

		loadMissions(r.getMissionsRootURL(),missions);
	}

    }

    public JPanel makeDropDownTopPanel() {

	JPanel topPanel = new JPanel();
	topPanel.setLayout( new FlowLayout() );

	topPanel.add( new JLabel("Mission:") );


	missionsBox = new JComboBox();

	missionsBox.setRenderer(new DefaultListCellRenderer() {

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

			Component tmp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

			setIcon( ((MissionLoader)value).getIcon() );

			return tmp;
		}

	});

	missionsBox.addItemListener(new ItemListener() {

		public void itemStateChanged(ItemEvent e) {

			if (e.getStateChange()==ItemEvent.SELECTED) {

				loadMission((MissionLoader)e.getItem());

			}

		}

	});


	topPanel.add(missionsBox);

	return topPanel;
    }

    public void loadMissions(final URL base, final String options) {


				new Thread() {

					public void run() {

						// split String, and get first element
						String[] split = options.split("\\,");


						for (int c=0;c<split.length;c++) {

							try {

								MissionLoader ml = new MissionLoader( rescue, base, split[c]);

								// missionLoaders.put(split[c],ml);

								missionsBox.addItem( ml );

							}
							catch(Exception ex) {
								ex.printStackTrace();
							}

						}


					}
				}.start();



    }


    /** This method is called from within the constructor to initialize the dialog. */
    private void initGUI() {

        setResizable(false);


	//###################################################################### LEFT

	JLabel missionSetup = new JLabel("Mission Setup", javax.swing.SwingConstants.CENTER);
	missionSetup.setFont(new Font(null, Font.ITALIC, 20) );

	// thing gets inserted here, but later

	clusterSimilarTypes = new JCheckBox("Cluster Similar Types");

	//###################################################################### RIGHT TOP

	JPanel GameTypeButtons = new JPanel();
	GameTypeButtons.setLayout(new javax.swing.BoxLayout(GameTypeButtons, javax.swing.BoxLayout.Y_AXIS));
        GameTypeButtons.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(),"Select a mission preset",javax.swing.border.TitledBorder.LEADING,javax.swing.border.TitledBorder.TOP,new java.awt.Font("SansSerif",0,11),new java.awt.Color(60,60,60)));

	ButtonGroup GameTypeButtonGroup = new ButtonGroup();

	JRadioButton cadet = new JRadioButton("The Cadet's Game", true);
	cadet.setActionCommand("cadet");
	cadet.addActionListener( this );

	JRadioButton lieutenant = new JRadioButton("The Lieutenant's Game");
	lieutenant.setActionCommand("lieutenant");
	lieutenant.addActionListener( this );

	JRadioButton captain = new JRadioButton("The Captain's Game");
	captain.setActionCommand("captain");
	captain.addActionListener( this );

	JRadioButton admiral = new JRadioButton("The Admiral's Game");
	admiral.setActionCommand("admiral");
	admiral.addActionListener( this );

	custom = new JRadioButton("Custom");
	custom.addActionListener( this );

	GameTypeButtonGroup.add ( cadet );
	GameTypeButtonGroup.add ( lieutenant );
	GameTypeButtonGroup.add ( captain );
	GameTypeButtonGroup.add ( admiral );
	GameTypeButtonGroup.add ( custom );

        GameTypeButtons.add( cadet );
        GameTypeButtons.add( lieutenant );
        GameTypeButtons.add( captain );
        GameTypeButtons.add( admiral );
        GameTypeButtons.add( Box.createVerticalStrut(10) );
        GameTypeButtons.add( custom );

	//###################################################################### RIGHT BOTTOM

	missionRank = new JLabel("Mission Rank:", javax.swing.SwingConstants.CENTER);

	JSeparator separator = new JSeparator();

	JButton cancelButton = new JButton(" Cancel ");
	cancelButton.setActionCommand("cancel");
	cancelButton.addActionListener(this);

	JButton okButton = new JButton("OK");
	okButton.setActionCommand("ok");
	okButton.addActionListener(this);




















	mainPanel.setLayout( new GridBagLayout() );
	gbc = new GridBagConstraints();
        gbc.insets = new java.awt.Insets(5, 5, 5, 5); // int top, int left, int bottom, int right (external padding)
        gbc.fill = GridBagConstraints.BOTH;

        //gbc.gridx = 0; // col
        //gbc.gridy = 0; // row
        //gbc.gridwidth = 2; // width
        //gbc.gridheight = 1; // height
	//mainPanel.add(topPanel, gbc);




        gbc.gridx = 0; // col
        gbc.gridy = 0; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height
	mainPanel.add(missionSetup, gbc);

        gbc.gridx = 0; // col
        gbc.gridy = 2; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height
	mainPanel.add(clusterSimilarTypes, gbc);





	JPanel panel1 = new JPanel( new GridBagLayout() );

        gbc.gridx = 0; // col
        gbc.gridy = 0; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height
	panel1.add(GameTypeButtons,gbc);

        gbc.gridx = 0; // col
        gbc.gridy = 1; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height
	panel1.add(missionRank,gbc);



	JPanel panel2 = new JPanel( new GridBagLayout() );

        gbc.gridx = 0; // col
        gbc.gridy = 0; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height
	panel2.add( new JPanel() ,gbc);

        gbc.gridx = 0; // col
        gbc.gridy = 1; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height
	panel2.add(cancelButton,gbc);

        gbc.gridx = 0; // col
        gbc.gridy = 2; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height
	panel2.add(okButton,gbc);



	JPanel box1 = new JPanel( new BorderLayout() );
	box1.add( separator, BorderLayout.SOUTH );

	JPanel box2 = new JPanel( new BorderLayout() );
	box2.add( panel1 ,BorderLayout.NORTH);
	box2.add( box1 );
	box2.add( panel2, BorderLayout.SOUTH );

        gbc.gridx = 1; // col
        gbc.gridy = 0; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 3; // height
	mainPanel.add(box2, gbc);


        addWindowListener(
            new java.awt.event.WindowAdapter() {
                public void windowClosing(WindowEvent evt) {
                    closeDialog(evt);
                }
            }
	);

	// set up the gbc for the mission setup panels
        gbc.gridx = 0; // col
        gbc.gridy = 1; // row
        gbc.gridwidth = 1; // width
        gbc.gridheight = 1; // height

    }

    public void loadMission(MissionLoader a) {

	//System.out.print("loading new mission...\n");

	currentMission=a;

	currentMission.loadImagesForSetup();

	clusterSimilarTypes.setSelected( currentMission.getCluster() );

	if (missionName!=null) {
		missionName.setIcon( currentMission.getIcon() );
		missionName.setText( currentMission.getMissionName() );
	}

	Vector v = currentMission.getMissionObjects();
	missionObjectPanel mop;

	Vector allies = new Vector();
	Vector enemies = new Vector();
	Vector nutral = new Vector();

	String po = ((MissionObject)currentMission.getPlayerObject()).getName();

	for(int i=0; i< v.size() ; i++) {

	    MissionObject cmo = (MissionObject)v.elementAt(i);

	    if (cmo.getMin() != cmo.getMax()) {

		mop = new missionObjectPanel(cmo);

		if (cmo.isAllies(po)) { allies.add(mop); }
		else if (cmo.isEnemies(po)) { enemies.add(mop); }
		else { nutral.add(mop); }

	    }

	}


	Box missionSetupBox = new Box( javax.swing.BoxLayout.Y_AXIS );

	if (allies.size() > 0) {

	    addLabel("ALLIES",missionSetupBox);

	    for(int i=0; i< allies.size() ; i++) {
		missionSetupBox.add( (JPanel)allies.elementAt(i) );
	    }

	}
	if (enemies.size() > 0) {

	    addLabel("ENEMIES",missionSetupBox);

	    for(int i=0; i< enemies.size() ; i++) {
		missionSetupBox.add( (JPanel)enemies.elementAt(i) );
	    }
	}
	if (nutral.size() > 0) {

	    addLabel("NEUTRAL",missionSetupBox);

	    for(int i=0; i< nutral.size() ; i++) {
		missionSetupBox.add( (JPanel)nutral.elementAt(i) );
	    }
	}

	custom.setSelected(true);

	if (thebox!=null) { mainPanel.remove(thebox); }

	mainPanel.add(missionSetupBox,gbc);
	validate();
	pack();
	repaint();

	thebox = missionSetupBox;

	    Dimension frameSize = getOwner().getSize();
	    Dimension aboutSize = getPreferredSize();
	    int x = getOwner().getLocation().x + (frameSize.width - aboutSize.width) / 2;
	    int y = getOwner().getLocation().y + (frameSize.height - aboutSize.height) / 2;
	    if (x < 0) x = 0;
	    if (y < 0) y = 0;
	    setLocation(x, y);

	changlings = allies;
	changlings.addAll(enemies);
	changlings.addAll(nutral);

	checkMissionRank();

    }

    public void addLabel(String a, Box b) {

	FlowLayout f = new FlowLayout( FlowLayout.LEADING );
	f.setHgap(20);
	JPanel p = new JPanel( f );
	p.add( new JLabel( "<html><u>" +a+ "</u></html>" ) );
	b.add(p);

    }

    public void actionPerformed(ActionEvent e) {

	if (e.getActionCommand().equals("load")) {

		    JFileChooser fc1 = new JFileChooser("missions");
		    XMLFileFilter filter1 = new XMLFileFilter();
		    fc1.setFileFilter(filter1);

                    int returnVal = fc1.showOpenDialog(this);

                    if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
                        java.io.File file = fc1.getSelectedFile();
                        // Write your code here what to do with selected file

			try {

			    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			    loadMission( new MissionLoader(rescue, file.getParentFile().toURI().toURL(), file.getName()) );

			    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

			}
			catch(Exception a) {

			    JOptionPane.showMessageDialog(null, "Error Loading Mission File: "+file.getName()+".\n"+a.getMessage(), "Load error!", JOptionPane.ERROR_MESSAGE );

			}

                    }


	}
	else if (e.getActionCommand().equals("cancel")) {

	    closeDialog(null);

	}
	else if (e.getActionCommand().equals("ok")) {

		if (rescue!=null) {

			for (int c=0; c < changlings.size() ;c++) {

				((missionObjectPanel)changlings.elementAt(c)).ok();

	    		}
			currentMission.setCluster( clusterSimilarTypes.isSelected() );
			rescue.setMission(currentMission);

		}
		else {

			StringBuffer buffer = new StringBuffer();

			buffer.append(currentMission.getXMLFileName());
			buffer.append("\n");

			for (int c=0; c < changlings.size() ;c++) {

				buffer.append( ((missionObjectPanel)changlings.elementAt(c)).getValue() );
				buffer.append((c==changlings.size()-1)?("\n"):(","));
	    		}

			buffer.append(clusterSimilarTypes.isSelected());

			newGameOptions = buffer.toString();

		}
		closeDialog(null);

	}



	else if (e.getActionCommand().equals("cadet")) {

	    for (int c=0; c < changlings.size() ;c++) {

		((missionObjectPanel)changlings.elementAt(c)).setCurrentPreset(1);

	    }

	    repaint();

	}
	else if (e.getActionCommand().equals("lieutenant")) {

	    for (int c=0; c < changlings.size() ;c++) {

		((missionObjectPanel)changlings.elementAt(c)).setCurrentPreset(2);

	    }
	    repaint();

	}
	else if (e.getActionCommand().equals("captain")) {

	    for (int c=0; c < changlings.size() ;c++) {

		((missionObjectPanel)changlings.elementAt(c)).setCurrentPreset(3);

	    }
	    repaint();

	}
	else if (e.getActionCommand().equals("admiral")) {

	    for (int c=0; c < changlings.size() ;c++) {

		((missionObjectPanel)changlings.elementAt(c)).setCurrentPreset(4);

	    }
	    repaint();

	}



    }

    /** Closes the dialog */
    private void closeDialog(WindowEvent evt) {
        setVisible(false);
        dispose();
    }

    public String getNewGameOptions() {

	return newGameOptions;

    }

    public void setVisible(boolean a) {

	if (a) {

		newGameOptions = null;

	}
	super.setVisible(a);
    }

    public void checkMissionRank() {

	String po = ((MissionObject)currentMission.getPlayerObject()).getName();

	int rank1=0;
	int tot1=0;
	int cnt1=0;

	//int rank2=0;
	//int tot2=0;
	//int cnt2=0;

	for (int c=0; c < changlings.size() ;c++) {

	    int r = ((missionObjectPanel)changlings.elementAt(c)).getCurrentRank(po);

	    if (r==-1) {}
	    else if (r==8) { cnt1=cnt1+r; }
	    else { cnt1=cnt1+r; tot1++; } // if (r<10)
	    //else { r=r-10;   cnt2=cnt2+r; tot2++; }


	}

	rank1 = cnt1/tot1; // average score for the num of enemes chosen
	//rank2 = cnt2/tot2; // average score for the num of friends chosen

	//System.out.print(rank1+" "+rank2+"\n");

	if (rank1 <= 1) {
	    missionRank.setText("Mission Rank: Cadet");
	}
	else if (rank1 <= 2) {
	    missionRank.setText("Mission Rank: Lieutenant");
	}
	else if (rank1 <= 3) {
	    missionRank.setText("Mission Rank: Captain");
	}
	else {
	    missionRank.setText("Mission Rank: Admiral");
	}

    }

    class missionObjectPanel extends JPanel {

	private MissionObject myMO;
	private JSlider slider;
	private BufferedImage icon;
	private int value;

	public missionObjectPanel(MissionObject cmo) {

	    myMO = cmo;

	    Dimension d = new Dimension(200, 30);

	    setMaximumSize(d);
	    setMinimumSize(d);
	    setPreferredSize(d);

	    //image name slider(min,max) number

	    setLayout(null);

	    value = cmo.getNumber();

	    slider = new JSlider(cmo.getMin(),cmo.getMax(),value);
	    slider.setBounds(80,0,100,30);
	    slider.setMajorTickSpacing(1);
	    slider.setSnapToTicks(true);


	    slider.addChangeListener(
		new ChangeListener() {
		    public void stateChanged(ChangeEvent e) {

			if (value != slider.getValue()) { custom.setSelected(true); }

			value = slider.getValue();

			checkMissionRank();

			repaint();

		    }
		}
	    );

	    add(slider);

	    icon = myMO.getIcon();

	    String name = myMO.getName();






	    JTextArea note = new JTextArea( name );

	    note.setLineWrap(true);
	    note.setWrapStyleWord(true);

	    note.setOpaque(false);
	    //note.setForeground((new JLabel()).getForeground());
	    note.setFont((new JLabel()).getFont().deriveFont(10f));
	    note.setEditable(false);

	    if (name.length() > 10) { note.setBounds(25, 2, 55, 30); }
	    else { note.setBounds(25, 11, 55, 30); }

	    Dimension noteSize = new Dimension(55, 30);

	    note.setPreferredSize( noteSize );
	    note.setMinimumSize( noteSize );
	    note.setMaximumSize( noteSize );

	    note.setFocusable(false);

	    add(note);


	}

	public void paintComponent(Graphics g) {

	    if (value != slider.getValue()) { slider.setValue(value); }

	    super.paintComponent(g);

	    g.setColor( java.awt.Color.BLACK );
	    //g.drawString(myMO.getName() ,20,20);
	    g.drawString(value+"" ,185,20);

	    g.drawImage(icon,0,5,this);

	}

	public void ok() {

	    myMO.setNumber( value );

	}

	public int getValue() {
		return value;
	}

	public void setCurrentPreset(int a) {

	    value = myMO.getCurrentPreset(a);

	}

	public int getCurrentRank(String p) {

	    return myMO.getCurrentRank(p,value);

	}

    }
}
