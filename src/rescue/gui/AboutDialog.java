package rescue.gui;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.net.InetAddress;
import java.net.UnknownHostException;


import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

import java.beans.*;

import java.util.Hashtable;
import java.util.HashMap;

import javax.swing.border.*;
import javax.swing.plaf.*;
//import sun.swing.DefaultLookup;
//import sun.swing.UIAction;

/**
 * <p> About Dialog </p>
 * @author Yura Mamyrin
 */


public class AboutDialog extends JDialog {
    private String title = "About";
    private String product;
    private String version;
    private String author = " Yura Mamyrin (yura@yura.net)";
    private String copyright = "Copyright (c) 2004-2008 yura.net";
    private String comments = " Computer Science Project (DCS-334) 2004-2005. \n Rescue! Max is based on a mac game called Rescue! by Tom Spreen. \n Rescue! Max is under the GNU General Public License, see gpl.txt for more details. ";
    private JPanel contentPane = new JPanel();
    private JLabel prodLabel = new JLabel();
    private JLabel verLabel = new JLabel();
    private JLabel authLabel = new JLabel();
    private JLabel copLabel = new JLabel();
    private JTextArea commentField = new JTextArea();
    private JPanel btnPanel = new JPanel();
    private JButton okButton = new JButton();
    private JLabel image = new JLabel();
    // private BorderLayout formLayout = new BorderLayout();
    private GridBagLayout contentPaneLayout = new GridBagLayout();
    private FlowLayout btnPaneLayout = new FlowLayout();

    private JPanel infoPanel = new JPanel();
    private JTextArea info1 = new JTextArea();
    private JTextArea info2 = new JTextArea();

    /**
     * Creates a new AboutDialog
     * @param parent decides the parent of the frame
     * @param modal
     * @param p contains the product
     * @param v contains the GUI version	
     */

    public AboutDialog(Frame parent, boolean modal, String p, String v) {
        super(parent, modal);
	product=p;
	version="Version: " + v;
        initGUI();
        pack();
    }

    /** This method is called from within the constructor to initialize the dialog. */
    private void initGUI() {

	// setSize(280,250); // (214,180)

        addWindowListener(
            new java.awt.event.WindowAdapter() {
                public void windowClosing(WindowEvent evt) {
                    closeDialog(evt);
                }
            });

        // getContentPane().setLayout(formLayout);
        getContentPane().setLayout(new java.awt.GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        contentPane.setLayout(contentPaneLayout);
        contentPane.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));
        prodLabel.setText(product);
        contentPane.add(prodLabel,
            new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, GridBagConstraints.REMAINDER, 1,
            0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

        verLabel.setText(version);
        contentPane.add(verLabel,
            new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, GridBagConstraints.REMAINDER, 1,
            0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

        authLabel.setText("Author:" + author);
        contentPane.add(authLabel,
            new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, GridBagConstraints.REMAINDER, 1,
            0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

        copLabel.setText(copyright);
        contentPane.add(copLabel,
            new GridBagConstraints(GridBagConstraints.RELATIVE, GridBagConstraints.RELATIVE, GridBagConstraints.REMAINDER, 1,
            0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

        // commentField.setWrapStyleWord(true);
        // commentField.setLineWrap(true);

        commentField.setBackground(getBackground());
        commentField.setForeground(copLabel.getForeground());
        commentField.setFont(copLabel.getFont());
	commentField.setText(comments);
        commentField.setEditable(false);

        // image.setText("[Your image]");
	image.setIcon(new javax.swing.ImageIcon( this.getClass().getResource("logo.png") ));
	image.setPreferredSize(new java.awt.Dimension(80,70));
	image.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);


        btnPanel.setLayout(btnPaneLayout);
        okButton.setText(" OK ");
        okButton.addActionListener(
            new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    dispose();
                }
            });
        btnPanel.add(okButton);

        infoPanel.setLayout(new javax.swing.BoxLayout(infoPanel, javax.swing.BoxLayout.X_AXIS));
        info1.setText(" Network Address: \n Operating System: \n Processor: \n Java vendor: \n Java vendor url: \n Java environment: \n Java VM: \n Java version: \n Java path: \n Java class version: ");

	String netInfo;
        try {
	    netInfo = InetAddress.getLocalHost().getHostAddress() + " (" + InetAddress.getLocalHost().getHostName() +")" ;
        } 
        catch (UnknownHostException e) {
	    netInfo = "No Network";
	}
            info2.setText(
		" " + netInfo + " \n" + 
		" " + System.getProperty("os.name") + " " + System.getProperty("os.version") + " " + System.getProperty("sun.os.patch.level") + " (" + System.getProperty("sun.arch.data.model") + "bit) on " + System.getProperty("os.arch") + " \n" +
		" " + System.getProperty("sun.cpu.isalist") + " \n" +
		" " + System.getProperty("java.vendor") + " \n" +
		" " + System.getProperty("java.vendor.url") + " \n" +
		" " + System.getProperty("java.runtime.name") + " \n" +
		" " + System.getProperty("java.vm.name") + " \n" +
		" " + System.getProperty("java.vm.version") + " (" + System.getProperty("java.vm.info") + ") \n" +
		" " + System.getProperty("java.home") + " \n" +
		" " + System.getProperty("java.class.version") + " (Rescue is compiled for 48.0/java1.4 or higher) "

	    );

        infoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(),"System Information",javax.swing.border.TitledBorder.LEADING,javax.swing.border.TitledBorder.TOP,new java.awt.Font("SansSerif",0,11),new java.awt.Color(60,60,60)));

        info1.setBackground(getBackground());
        info1.setForeground(copLabel.getForeground());
        info1.setFont(copLabel.getFont());
        info1.setEditable(false);

        info2.setBackground(getBackground());
        info2.setForeground(copLabel.getForeground());
        info2.setFont(copLabel.getFont());
        info2.setEditable(false);

        infoPanel.add(info1);
        infoPanel.add(info2);


        c.insets = new java.awt.Insets(3, 3, 3, 3);


        c.gridx = 0; // col
        c.gridy = 0; // row
        c.gridwidth = 1; // width
        c.gridheight = 1; // height
        getContentPane().add(image, c);


        c.gridx = 1; // col
        c.gridy = 0; // row
        c.gridwidth = 1; // width
        c.gridheight = 1; // height
        getContentPane().add(contentPane, c);


        c.gridx = 0; // col
        c.gridy = 3; // row
        c.gridwidth = 2; // width
        c.gridheight = 1; // height
        getContentPane().add(btnPanel, c);


        c.fill = GridBagConstraints.BOTH;


        c.gridx = 0; // col
        c.gridy = 2; // row
        c.gridwidth = 2; // width
        c.gridheight = 1; // height
        getContentPane().add(infoPanel, c);


        c.gridx = 0; // col
        c.gridy = 1; // row
        c.gridwidth = 2; // width
        c.gridheight = 1; // height
        getContentPane().add(commentField, c);


        setTitle(title);
        setResizable(false);

    }

    /** Closes the dialog */
    private void closeDialog(WindowEvent evt) {
        setVisible(false);
        dispose();
    }
}
