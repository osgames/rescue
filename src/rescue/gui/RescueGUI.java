package rescue.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import rescue.*;
import rescue.panels.*;

/**
 * <p> Rescue GUI </p>
 * @author Yura Mamyrin
 */

public class RescueGUI extends JPanel implements ActionListener, RescueListener, Runnable {

    private RunRescue rescue;
    private Component bigmap;
    private Vector panels;

    private Component[] inGameMenus;
    private Component[] outGameMenus;
    private Vector Windows;

    private JDesktopPane desktop;
    private JMenu windowsMenu;

//    private boolean mode;

    private Image icon;
    private String missions;

    public RescueGUI(RunRescue rr,String m) {

	missions = m;

	setLayout(new BorderLayout());

//	mode=false;

	icon = Toolkit.getDefaultToolkit().getImage( this.getClass().getResource("icon.png") );

	rescue = rr;

	JButton button;

	inGameMenus = new Component[9];
	outGameMenus = new Component[6];

	desktop = new JDesktopPane();

	desktop.setDragMode(JDesktopPane.LIVE_DRAG_MODE);

	// ############# DO TOOLBAR
	JToolBar mainToolbar = new JToolBar();
	mainToolbar.setRollover(true);

	button = new JButton("New Game");
	button.setActionCommand("new");
	button.addActionListener(this);
	mainToolbar.add(button);
	outGameMenus[2]=button;


	inGameMenus[8]= mainToolbar.add( rescue.getAction("pause") );

	inGameMenus[2]= mainToolbar.add( rescue.getAction("endgame") );

	mainToolbar.addSeparator();

	button = new JButton("Preferences...");
	button.setActionCommand("preferences");
	button.addActionListener(this);
	mainToolbar.add(button);
	outGameMenus[3]=button;

	button = new JButton("Mission Setup...");
	button.setActionCommand("mission");
	button.addActionListener(this);
	mainToolbar.add(button);
	outGameMenus[4]=button;
/*
	button = new JButton("High Scores");
	button.setActionCommand("scores");
	button.addActionListener(this);
	mainToolbar.add(button);
*/
	mainToolbar.addSeparator();

	mainToolbar.add( rescue.getAction("quit") );

	mainToolbar.add( rescue.getAction("help") );

	button = new JButton("About");
	button.setActionCommand("about");
	button.addActionListener(this);
	mainToolbar.add(button);

	//JLabel versionLabel = new JLabel("  " +RunRescue.NAME + " Version " + RunRescue.VERSION);
	//mainToolbar.add(versionLabel);

	JPanel toolpanel = new JPanel(new BorderLayout());


	// ############ DO MENU
	add( createMenuBar(), BorderLayout.NORTH );
	add(toolpanel);



	toolpanel.add(mainToolbar, BorderLayout.NORTH );
	toolpanel.add(desktop);

	//setContentPane(desktop);

        //pack();
	//setSize(750,620);// not really should be here


	for(int i=0; i< inGameMenus.length ; i++) {
	    inGameMenus[i].setEnabled(false);
	}

    }

    public Image getIcon() {

	return icon;

    }

    public void newGame(MissionLoader cm) {

	Vector w = cm.getWindows();

	bigmap = cm.getBigMap();
	panels = cm.getPanels();

	Windows = new Vector();
	String name;
	JMenuItem menuItem;

	JFrame main=null;

	for(int i=0; i< w.size() ; i++) {

	    Component frame;
	    ContainerPanel containerPanel = (ContainerPanel)w.elementAt(i);
	    name = containerPanel.getName();

	    if ( rescue.getMode() ) {

		Window sframe;

		if (i==0) {

		    main = new JFrame(name);

		    // YURA:TODO while i still have not fixed the resizing
		    main.setResizable(false);

		    main.setIconImage( icon );
		    sframe = main;

		    main.setContentPane( containerPanel );

		}
		else {

		    sframe = new JDialog(main,name);

		    // YURA:TODO while i still have not fixed the resizing
		    ((JDialog)sframe).setResizable(false);

		    ((JDialog)sframe).setContentPane( containerPanel );

		}


		sframe.setLocation( containerPanel.getPreferredLocation() );
		sframe.pack();

		sframe.addWindowListener(
		    new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
			    //System.out.print("EXIT\n");
			    rescue.closeCurrentMission();
			}
		    }
		);

		Windows.add(sframe);

		frame = sframe;

	    }
	    else {

		SFrame sframe = new SFrame( name ,containerPanel );
		sframe.pack();

		// YURA:TODO while i still have not fixed the resizing
		sframe.setResizable(false);

		sframe.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
		sframe.setFrameIcon( new ImageIcon(icon) );

		Windows.add( sframe );

		desktop.add( sframe );


		menuItem = new JMenuItem(name);
		//menuItem.setMnemonic(KeyEvent.VK_U);
		menuItem.setActionCommand("window "+i);
		menuItem.addActionListener(this);

		windowsMenu.add(menuItem);

		frame = sframe;
	    }


	    frame.addComponentListener( new AspectRatio(containerPanel.getWidth(),containerPanel.getHeight()) );

	}

	if ( rescue.getMode() ) {

	    Window a;

	    for(int i=0; i< Windows.size() ; i++) {

		a = ((Window)Windows.elementAt(i));

		a.setVisible(true);

	    }

	    // does not work at the beggining of the game
	    findParentFrame(this).setVisible(false);

	}
	else {

	    windowsMenu.addSeparator();

	    menuItem = new JMenuItem("Use Default Positions");
            menuItem.setMnemonic(KeyEvent.VK_U);
            //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
            //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
            menuItem.setActionCommand("w default");
            menuItem.addActionListener(this);
	    windowsMenu.add(menuItem);
	    //menu.setEnabled(false);

	    defaultWindowPositions();

	    for(int i=0; i< inGameMenus.length ; i++) {
		inGameMenus[i].setEnabled(true);
	    }
	    for(int i=0; i< outGameMenus.length ; i++) {
		outGameMenus[i].setEnabled(false);
	    }

	}

	repaint();

    }
    public void closeGame() {

	if ( rescue.getMode() ) {

	    for(int i=0; i< Windows.size() ; i++) {

		((Window)Windows.elementAt(i)).setVisible(false);
		((Window)Windows.elementAt(i)).dispose();

	    }

	    findParentFrame(this).setVisible(true);

	}
	else {

	    for(int i=0; i< Windows.size() ; i++) {

		((SFrame)Windows.elementAt(i)).dispose();

		desktop.remove( ((SFrame)Windows.elementAt(i)) );

	    }

	    windowsMenu.removeAll();

	    for(int i=0; i< inGameMenus.length ; i++) {
		inGameMenus[i].setEnabled(false);
	    }
	    for(int i=0; i< outGameMenus.length ; i++) {
		outGameMenus[i].setEnabled(true);
	    }

	}

	Windows = null;
	bigmap = null;

    }

    public void dead() {

	new Thread(this).start();
    }

    public void run() {

	try {
		Thread.sleep(1000);

	} catch (InterruptedException e) {}

	Frame frame;

	if ( rescue.getMode() ) {
		frame = (Frame)Windows.elementAt(0);
	}
        else {
                frame = findParentFrame(this);
        }

	JOptionPane.showMessageDialog(frame, "You are dead!" , "Dead", JOptionPane.PLAIN_MESSAGE );

	rescue.closeCurrentMission();

    }

    // YURA: This method ONLY works when mode is FALSE
    public void defaultWindowPositions() {

	SFrame a;

	for(int i=0; i< Windows.size() ; i++) {

	    a = ((SFrame)Windows.elementAt(i));

	    a.setLocation( a.getPreferredLocation() );
	    a.setSize( a.getPreferredSize() );
	    a.setVisible(true);
	    try { a.setIcon( false ); } catch (Exception e) {}

	}

    }

    protected JMenuBar createMenuBar() {

        JMenuBar menuBar = new JMenuBar();

        //Set up the lone menu.
        JMenu menu;
        JMenuItem menuItem;
        JMenuItem menuItem2;







	menu = new JMenu("Game");
        menu.setMnemonic(KeyEvent.VK_G);

	menuItem = new JMenuItem("New");
        menuItem.setMnemonic(KeyEvent.VK_N);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("new");
        menuItem.addActionListener(this);
	menu.add(menuItem);
	outGameMenus[0]=menuItem;

	inGameMenus[6] = menu.add( rescue.getAction("pause") );

	inGameMenus[7]= menu.add( rescue.getAction("endgame") );

	menu.addSeparator();

	menuItem = new JMenuItem("Preferences...");
        menuItem.setMnemonic(KeyEvent.VK_P);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("preferences");
        menuItem.addActionListener(this);
	menu.add(menuItem);
	outGameMenus[5]=menuItem;

	menuItem = new JMenuItem("Mission Setup...");
        menuItem.setMnemonic(KeyEvent.VK_M);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("mission");
        menuItem.addActionListener(this);
	menu.add(menuItem);
	outGameMenus[1]=menuItem;
/*
	menuItem = new JMenuItem("High Scores");
        menuItem.setMnemonic(KeyEvent.VK_H);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("scores");
        menuItem.addActionListener(this);
	menu.add(menuItem);
*/
	menu.addSeparator();

        menu.add( rescue.getAction("quit") );

        menuBar.add(menu);







	menu = new JMenu("Select");
        menu.setMnemonic(KeyEvent.VK_S);


	menu.add( rescue.getAction("select_all") );
	menu.add( rescue.getAction("select_enemies") );
	menu.add( rescue.getAction("select_manual") );


        menuBar.add(menu);
	inGameMenus[0]=menu;







	menu = new JMenu("Options");
        menu.setMnemonic(KeyEvent.VK_O);

	menuItem = new JMenu("Sound");
        menuItem.setMnemonic(KeyEvent.VK_S);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        //menuItem.setActionCommand("s");
        //menuItem.addActionListener(this);
	menu.add(menuItem);

		//menuItem2





/*
	menuItem = new JMenu("Game Speed");
        menuItem.setMnemonic(KeyEvent.VK_G);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        //menuItem.setActionCommand("g");
        //menuItem.addActionListener(this);
	menu.add(menuItem);

		//menuItem2
*/
	menu.addSeparator();

	menu.add( rescue.getAction("headsup") );

        menuBar.add(menu);
	inGameMenus[1]=menu;






/*
	menu = new JMenu("Computer");
        menu.setMnemonic(KeyEvent.VK_C);

	menuItem = new JMenu("Find");
        menuItem.setMnemonic(KeyEvent.VK_F);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        //menuItem.setActionCommand("auto shields");
        //menuItem.addActionListener(this);
	menu.add(menuItem);

		menuItem2 = new JMenuItem("Nearest Outpost");
        	menuItem2.setMnemonic(KeyEvent.VK_O);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("f outpost");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		menuItem2 = new JMenuItem("Nearest Starbase");
        	menuItem2.setMnemonic(KeyEvent.VK_S);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("f starbase");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		menuItem2 = new JMenuItem("Nearest Enemy");
        	menuItem2.setMnemonic(KeyEvent.VK_E);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("f enemy");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		menuItem2 = new JMenuItem("Nearest Wormhole");
        	menuItem2.setMnemonic(KeyEvent.VK_W);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("f wormhole");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		((JMenu)menuItem).addSeparator();

		menuItem2 = new JCheckBoxMenuItem("Find Only Active Outposts");
        	menuItem2.setMnemonic(KeyEvent.VK_W);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("f active");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

	menuItem = new JMenu("Chase");
        menuItem.setMnemonic(KeyEvent.VK_C);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        //menuItem.setActionCommand("auto shields");
        //menuItem.addActionListener(this);
	menu.add(menuItem);

		menuItem2 = new JMenuItem("Nearest Enemy");
        	menuItem2.setMnemonic(KeyEvent.VK_O);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("c nearest");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		menuItem2 = new JMenuItem("Weakest Enemy");
        	menuItem2.setMnemonic(KeyEvent.VK_W);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("c weakest");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		menuItem2 = new JMenuItem("Strongest Enemy");
        	menuItem2.setMnemonic(KeyEvent.VK_S);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("c strongest");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		menuItem2 = new JMenuItem("Nearest Asteroid");
        	menuItem2.setMnemonic(KeyEvent.VK_W);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("c asteroid");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

		((JMenu)menuItem).addSeparator();

		menuItem2 = new JCheckBoxMenuItem("Fly With Nearest Friendly");
        	menuItem2.setMnemonic(KeyEvent.VK_F);
        	//menuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        	//        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        	menuItem2.setActionCommand("c fly");
        	menuItem2.addActionListener(this);
		menuItem.add(menuItem2);

	menu.addSeparator();

	menuItem = new JCheckBoxMenuItem("Auto Snap-On Shields");
        menuItem.setMnemonic(KeyEvent.VK_S);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("auto shields");
        menuItem.addActionListener(this);
	menu.add(menuItem);

	menuItem = new JCheckBoxMenuItem("Auto Select Nearest Enemy");
        menuItem.setMnemonic(KeyEvent.VK_N);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("auto enemy");
        menuItem.addActionListener(this);
	menu.add(menuItem);

	menu.addSeparator();

	menuItem = new JMenuItem("Set Auto-Destruct");
        menuItem.setMnemonic(KeyEvent.VK_A);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("destruct");
        menuItem.addActionListener(this);
	menu.add(menuItem);

        menuBar.add(menu);
	inGameMenus[9]=menu; // when uncomenting this remember to set inGameMenus to have 10 elements
*/





	menu = new JMenu("Starfleet");
        menu.setMnemonic(KeyEvent.VK_S);

	menu.add( rescue.getAction("dock") );

	menu.add( rescue.getAction("board") );


	menu.addSeparator();

	menu.add( rescue.getAction("beam_down") );
	menu.add( rescue.getAction("beam_up") );

	menu.addSeparator();

	JMenu menu2 = new JMenu("Evasive Maneuvers");
        menu2.setMnemonic(KeyEvent.VK_E);

		menu2.add( rescue.getAction("evasive 1") );
		menu2.add( rescue.getAction("evasive 2") );
		menu2.add( rescue.getAction("evasive 3") );

	menu.add(menu2);

/*	// YURA:TODO DO THE COMUNICATION SYSTEM
	menu.addSeparator();

	menuItem = new JMenuItem("Communicate...");
        menuItem.setMnemonic(KeyEvent.VK_C);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("communicate");
        menuItem.addActionListener(this);
	menu.add(menuItem);

	menuItem = new JMenuItem("Send Distress Signal");
        menuItem.setMnemonic(KeyEvent.VK_S);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("distress");
        menuItem.addActionListener(this);
	menu.add(menuItem);
*/
        menuBar.add(menu);
	inGameMenus[3]=menu;







	menu = new JMenu("Engineering");
        menu.setMnemonic(KeyEvent.VK_E);

/*
	menuItem = new JCheckBoxMenuItem("Auto Prioritize Repairs");
        menuItem.setMnemonic(KeyEvent.VK_P);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("prioritize");
        menuItem.addActionListener(this);
	menu.add(menuItem);

	menuItem = new JCheckBoxMenuItem("Recharge Phaser Banks");
        menuItem.setMnemonic(KeyEvent.VK_R);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("recharge");
        menuItem.addActionListener(this);
	menu.add(menuItem);
*/


	menu.add( rescue.getAction("divert") );

	menu.addSeparator();

	menu.add( rescue.getAction("reserve") );

        menuBar.add(menu);
	inGameMenus[4]=menu;







	menu = new JMenu("Window");
        menu.setMnemonic(KeyEvent.VK_W);

	windowsMenu = menu;

        menuBar.add(menu);
	inGameMenus[5]=menu;






	menu = new JMenu("Help");
	menu.setMnemonic(KeyEvent.VK_H);

	menu.add( rescue.getAction("help") );

	menuItem = new JMenuItem("Send Bug Report");
	menuItem.setActionCommand("bug report");
	menuItem.addActionListener(this);
	menu.add(menuItem);

	menuItem = new JMenuItem("About Rescue!");
        menuItem.setMnemonic(KeyEvent.VK_R);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("aboutr");
        menuItem.addActionListener(this);
	menu.add(menuItem);


	menuItem = new JMenuItem("About Rescue! Max");
        menuItem.setMnemonic(KeyEvent.VK_A);
        //menuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
        //        KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("about");
        menuItem.addActionListener(this);
	menu.add(menuItem);

        menuBar.add(menu);




        return menuBar;
    }

    /**
     * Exit the Application
     */
    private void exitForm(WindowEvent evt) {
        System.exit(0);
    }

    private int counter=0;
    public void update() {

	try { // this is here so that a error in the gui updating does not crash the game

	    bigmap.repaint();

	    counter++;

	    if (counter>=10 || rescue.getPause() ) {

		for(int i=0; i< panels.size() ; i++) {
		    ((Component)panels.elementAt(i)).list();
		}
		counter=0;
	    }
	}
	catch(Exception e) {

	    System.out.print("error updating gui: "+e+"\n");

	    //e.printStackTrace();

	    counter=0;
	}

    }

    public void actionPerformed(ActionEvent e) {

	if (e.getActionCommand().equals("new")) {

	    rescue.loadCurrentMission();
	    return;

	}
	else if (e.getActionCommand().equals("mission")) {

	    MissionSetup mDialog = new MissionSetup( findParentFrame(this), true, rescue,missions );

	    mDialog.setVisible(true);
	    return;
	}
	else if (e.getActionCommand().equals("preferences")) {

	    PrefDialog aboutDialog = new PrefDialog( findParentFrame(this) , true, rescue, this, rescue.getKeymap() );
	    Dimension frameSize = getSize();
	    Dimension aboutSize = aboutDialog.getPreferredSize();
	    int x = getLocation().x + (frameSize.width - aboutSize.width) / 2;
	    int y = getLocation().y + (frameSize.height - aboutSize.height) / 2;
	    if (x < 0) x = 0;
	    if (y < 0) y = 0;
	    aboutDialog.setLocation(x, y);

	    aboutDialog.setVisible(true);
	    return;
	}
	else if (e.getActionCommand().equals("about")) {

	    AboutDialog aboutDialog = new AboutDialog( findParentFrame(this) , true, RunRescue.NAME, RunRescue.VERSION );
	    Dimension frameSize = getSize();
	    Dimension aboutSize = aboutDialog.getPreferredSize();
	    int x = getLocation().x + (frameSize.width - aboutSize.width) / 2;
	    int y = getLocation().y + (frameSize.height - aboutSize.height) / 2;
	    if (x < 0) x = 0;
	    if (y < 0) y = 0;
	    aboutDialog.setLocation(x, y);

	    aboutDialog.setVisible(true);
	    return;
	}
	else if (e.getActionCommand().equals("aboutr")) {

	    JDialog aboutDialog = new JDialog( findParentFrame(this) ,"Rescue!",true);

	    aboutDialog.getContentPane().add( new JLabel(new javax.swing.ImageIcon( this.getClass().getResource("credits.png") )) );
	    aboutDialog.setResizable(false);
	    aboutDialog.pack();

	    Dimension frameSize = getSize();
	    Dimension aboutSize = aboutDialog.getPreferredSize();
	    int x = getLocation().x + (frameSize.width - aboutSize.width) / 2;
	    int y = getLocation().y + (frameSize.height - aboutSize.height) / 2;
	    if (x < 0) x = 0;
	    if (y < 0) y = 0;
	    aboutDialog.setLocation(x, y);

	    aboutDialog.setVisible(true);
	    return;
	}
	else if (e.getActionCommand().equals("bug report")) {
		System.err.println("bug report");
	}
	else if (e.getActionCommand().equals("w default")) {

	    defaultWindowPositions();
	    return;
	}
	else if (e.getActionCommand().startsWith("window ")) {

	    int i = Integer.parseInt( e.getActionCommand().substring(7) );

	    ((SFrame)Windows.elementAt(i)).setVisible(true);
	    try { ((SFrame)Windows.elementAt(i)).setIcon( false ); ((SFrame)Windows.elementAt(i)).setSelected(true); } catch (Exception a) {}

	    return;

	}
	else {

	    System.out.print("unknown command "+e.getActionCommand()+"\n");
	    return;
	}

    }

    /**
     * This runs the program
     * @param argv
     */
    public static void main(String[] argv) {

        try {
            net.yura.grasshopper.PopupBug.initSimple(RunRescue.NAME, RunRescue.VERSION, "");
        }
        catch (Throwable th) {
            System.out.println("grasshopper failed to load "+th);
        }

	SplashScreen ss = new SplashScreen();
	ss.setVisible(true);

	// set up system Look&Feel
	try {

/*	    // YURA gtk theme just fucks things up in the mission setup dialog

	    String os = System.getProperty("os.name");
	    String jv = System.getProperty("java.version");

            if ( jv.startsWith("1.4.2") && os != null && os.startsWith("Linux")) {
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
	    }
	    else {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }
*/

	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	}
	catch (Exception e) {
	    e.printStackTrace();
	}

	try {

	    RunRescue rr = new RunRescue( new File("missions").toURI().toURL());

	    final RescueGUI gui = new RescueGUI(rr, null );

	    JFrame frame = new JFrame(RunRescue.NAME);

	    frame.addWindowListener(
		new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				gui.exitForm(evt);
			}
		}
	    );

	    frame.setContentPane(gui);
	    frame.setIconImage( gui.getIcon() );

	    //rr.addRescueListener(gui);

            //Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            //Dimension frameSize = gui.getSize();
            //frameSize.height = ((frameSize.height > screenSize.height) ? screenSize.height : frameSize.height);
            //frameSize.width = ((frameSize.width > screenSize.width) ? screenSize.width : frameSize.width);
            //gui.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);

	    // getMaximizedBounds ?

	    Rectangle screenSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
            frame.setBounds(screenSize);
            frame.setVisible(true);

	    rr.addRescueListener(gui);

	    ss.setVisible(false);
	    ss.dispose();

	    // after gui is created this thread becomes the main game thread
	    rr.run();

	} catch(Exception e) {

	    StackTraceElement[] st = e.getStackTrace();

	    String info="";

	    for (int i = 0; i < st.length ; i++) {
		info = info + st[i] + "\n";
	    }

	    JOptionPane.showMessageDialog(null, "Error Loading default config file: " + e.getMessage() + "\n\n" + e.toString() + "\n" + info, "Load error!", JOptionPane.ERROR_MESSAGE );
	    System.exit(0);
	}
    }

    public static Frame findParentFrame(Container c) {
	return (Frame)javax.swing.SwingUtilities.getAncestorOfClass(Frame.class, c);
    }
}
