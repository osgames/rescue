package rescue.gui;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JDialog;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Window;
import javax.swing.RootPaneContainer;
import javax.swing.JInternalFrame;

public class AspectRatio extends ComponentAdapter {

	private double ratio;

	public AspectRatio(int w,int h) {

		ratio = w/(double)h;

	}

	public void componentResized(ComponentEvent e) {

//		System.out.println("RESIZE!! "+ratio);

		Component c = e.getComponent();

		if (c instanceof RootPaneContainer) {

			RootPaneContainer f = (RootPaneContainer)c;

			fixSize(f.getContentPane(),ratio);



			if (c instanceof Window) { ((Window)f).pack(); }
			else if (c instanceof JInternalFrame) { ((JInternalFrame)f).pack(); }
		}

	}

	public static void fixSize(Container cp,double r) {


			int w = cp.getWidth();
			int h = cp.getHeight();

			int dw = (int)Math.round(h*r);
			int dh = (int)Math.round(w/r);

			if (dw > w) {
				w = dw;
			}
			if (dh > h) {
				h = dh;
			}

			cp.setPreferredSize(new Dimension(w, h));

	}

}
