package rescue.gui;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import javax.swing.Action;
import javax.swing.KeyStroke;

// UI should have a table and a change button under it, then a save and cencel too
public class Keymap implements KeyEventDispatcher {

	private final List actions;

	public Keymap(List a) {
		actions = a;
	}

        public List getActions() {
                return actions;
        }

	/**
	 * some problems here, the Alt-Gr key is mapped to Ctrl+Alt
	 * and there is NO way to tell them apart, isAltGraphDown also acts the same way with both
	 */
	public boolean dispatchKeyEvent(KeyEvent e) {

		int id = e.getID();

		if (
			(setupAction!=null) ||
			(id == KeyEvent.KEY_PRESSED)
		) {

			int keycode = e.getKeyCode();
			int modifiers = e.getModifiers();

			// if the code itself is a modifier, then set the modifier to 0
			if (	keycode==KeyEvent.VK_CONTROL ||
				keycode==KeyEvent.VK_SHIFT ||
				keycode==KeyEvent.VK_ALT ||
				keycode==KeyEvent.VK_ALT_GRAPH ||
				keycode==KeyEvent.VK_META) {

				// if Alt-Gr is pressed then the keycode is actually Ctrl, but we want it to be Alt
				if ((modifiers & KeyEvent.ALT_MASK ) != 0) {
					keycode=KeyEvent.VK_ALT;
				}
				modifiers = 0;
			}

			KeyStroke ks = KeyStroke.getKeyStroke(keycode, modifiers);

			if (setupAction!=null) {

				if (id == KeyEvent.KEY_RELEASED) {
					setupAction.putValue(setupName, ks );
					setupAction = null;
					setupName = null;
                                        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(this);
				}
				return true;

			}
			else if (id == KeyEvent.KEY_PRESSED) {

				for (int c=0;c<actions.size();c++) {

					Action action = (Action)actions.get(c);

					KeyStroke akey = (KeyStroke)action.getValue(Action.ACCELERATOR_KEY);

					if (ks.equals(akey)) {

						action.actionPerformed(null);
						return true;
					}

				}

			}
		}

		return false;

	}

	public Action setupAction;
	public String setupName;
	public void setupKeyForAction(Action action,String name) {
		setupAction = action;
		setupName = name;
                KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
	}





	public static void loadKeys(List actions, InputStream file) throws Exception {

		Properties keysconfig = new Properties();
		keysconfig.load( file );

		for (int c=0;c<actions.size();c++) {
			Action action = (Action)actions.get(c);

			String keys = keysconfig.getProperty( (String)action.getValue(Action.ACTION_COMMAND_KEY) );
			KeyStroke ks;

			if (keys!=null && !"".equals(keys)) {

				ks=KeyStroke.getKeyStroke( (int)keys.charAt(0) , (int)keys.charAt(1) );
			}
			else {
				ks=null;
			}

			action.putValue( Action.ACCELERATOR_KEY , ks );
		}

	}

	public static void saveKeys(List actions, OutputStream file) throws Exception {

		Properties keysconfig = new Properties();

		for (int c=0;c<actions.size();c++) {
			Action action = (Action)actions.get(c);
			String name = (String)action.getValue(Action.ACTION_COMMAND_KEY);
			KeyStroke keyStroke = (KeyStroke)action.getValue(Action.ACCELERATOR_KEY);

			if (keyStroke!=null) {
				keysconfig.setProperty(name, ((char)keyStroke.getKeyCode()) +""+ ((char)keyStroke.getModifiers()) );
			}
			else {
				keysconfig.setProperty(name, "" );
			}
		}

		keysconfig.store( file, "keys" );

	}


}
