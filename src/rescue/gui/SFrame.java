package rescue.gui;

import rescue.panels.ContainerPanel;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;

public class SFrame extends JInternalFrame {

    private ContainerPanel panel;
    // private double ratio;

    public SFrame(String a,ContainerPanel p) {
	super(a,true,true,false,true);

	panel=p;

	setContentPane(panel);
    }

    public Point getPreferredLocation() {

	return panel.getPreferredLocation();

    }
/*
    public void setBounds(int x,int y,int width,int height) {
/
	Dimension a = panel.getSize();

	if (a.width != 0 && a.width == a.height) {

	    Dimension b = getSize();

	    int w = width - b.width;
	    int h = height - b.height;

	    if (w > 0) { h=w; }
	    else if (w < 0) { h=w; }
	    else if (h > 0) { w=h; }
	    else if (h < 0) { w=h; }

	    width = b.width+w;
	    height = b.height+h;

	}
/

	if (width == getWidth() && height == getHeight() ){}

	else if (width > getWidth() ) { height=(int)(width/ratio); }
	else if (height > getHeight() ) { width=(int)(height*ratio); }

	else if (width < getWidth() && height <= (int)(width/ratio) ) { height=(int)(width/ratio); }
	else if (height < getHeight() && width <= (int)(height*ratio) ) { width=(int)(height*ratio); }

	else { return; }

	super.setBounds(x,y,width,height);

    }

    public void pack() {
	super.pack();

	Dimension d = getPreferredSize();

	ratio=d.getWidth()/d.getHeight();

    }
*/
}
