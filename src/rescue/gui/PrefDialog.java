package rescue.gui;

import rescue.*;

import java.util.Vector;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;

import javax.swing.table.AbstractTableModel;
import javax.swing.Action;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.BoxLayout;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import java.net.URL;
import java.io.FileOutputStream;
import java.io.File;
import java.util.List;

/**
 * <p> About Dialog </p>
 * @author Yura Mamyrin
 */


public class PrefDialog extends JDialog implements ActionListener,PropertyChangeListener {

    private static final String TEMP_ACCELERATOR_KEY = "TEMP_AcceleratorKey";

    private RunRescue rescue;
    private RescueGUI gui;
    private JCheckBox inout;
    private JSpinner delay;
    private Keymap keymap;

    public PrefDialog(Frame parent, boolean modal, RunRescue r, RescueGUI g, Keymap km) {
        super(parent,"Preferences", modal);

	rescue=r;
	gui=g;
        keymap=km;

        initGUI();
        pack();
    }

    /** This method is called from within the constructor to initialize the dialog. */
    private void initGUI() {

        //setResizable(false);


	//Rescue! Max Startup Prefererences
	//Initial window positions - default/saved
	//Initial sound setting - on/off
	//Initial skill level setting - Cadet/...
	//[v] Begin new game at startup

	//set Default Preferences
	//Save Window Positions

	JButton cancelButton = new JButton("Cancel");
	cancelButton.setActionCommand("cancel");
	cancelButton.addActionListener(this);

	JButton okButton = new JButton("OK");
	okButton.setActionCommand("ok");
	okButton.addActionListener(this);

	JButton changeButton = new JButton("Change");
	changeButton.setActionCommand("change");
	changeButton.addActionListener(this);

	JButton clearButton = new JButton("Clear");
	clearButton.setActionCommand("clear");
	clearButton.addActionListener(this);

	JPanel bottomPanel = new JPanel();
	bottomPanel.add(cancelButton);
	bottomPanel.add(changeButton);
	bottomPanel.add(clearButton);
	bottomPanel.add(okButton);

	inout = new JCheckBox("Use External Frames", rescue.getMode() );

	JPanel optionsPanel = new JPanel();
	delay = new JSpinner();
	delay.setValue(new Integer( rescue.getDelay() ));
	optionsPanel.add(new JLabel("Delay in milliseconds:") );
	optionsPanel.add( delay );

	JPanel options = new JPanel();
	//options.setLayout(new BoxLayout(options,BoxLayout.X_AXIS));
	options.add(inout);
	options.add(optionsPanel);

	JPanel keymaping = new JPanel(new BorderLayout());
	keymaping.add(makeKeyTable());

	getContentPane().add(options, java.awt.BorderLayout.NORTH );
	getContentPane().add(keymaping, java.awt.BorderLayout.CENTER);
	getContentPane().add(bottomPanel, java.awt.BorderLayout.SOUTH);

        addWindowListener(
            new java.awt.event.WindowAdapter() {
                public void windowClosing(WindowEvent evt) {
                    closeDialog();
                }
            }
	);

    }

    public void actionPerformed(ActionEvent e) {

	if (e.getActionCommand().equals("ok")) {

		rescue.setMode( inout.isSelected() );
		rescue.setDelay( ((Integer)delay.getValue()).intValue() );

		List actions = rescue.getActions();
		for (int c=0;c<actions.size();c++) {
			Action action = (Action)actions.get(c);
			action.putValue( Action.ACCELERATOR_KEY , action.getValue(TEMP_ACCELERATOR_KEY) );
		}

		try {
			// in java 1.5 use toURI() instead of getFile()
			Keymap.saveKeys(actions, new FileOutputStream( new File(new URL(rescue.getMissionsRootURL() ,RunRescue.KEYMAP_FILENAME).getFile() ) ) );

		}
		catch(Exception ex) {
			System.out.println("unable to save keys: "+ex);
		}

		closeDialog();
	}
	else if (e.getActionCommand().equals("change")) {

		Action action = getCurrentAction();
		action.addPropertyChangeListener(this);
		keymap.setupKeyForAction(action, TEMP_ACCELERATOR_KEY );

	}
	else if (e.getActionCommand().equals("clear")) {

		Action action = getCurrentAction();
		action.putValue( TEMP_ACCELERATOR_KEY , null );
		repaint();

	}
	else if (e.getActionCommand().equals("cancel")) {

		closeDialog();

	}

    }

    public void propertyChange(PropertyChangeEvent evt) {

	Action action = getCurrentAction();
	action.removePropertyChangeListener(this);

	Object keyStroke = action.getValue( TEMP_ACCELERATOR_KEY );

	List actions = rescue.getActions();
	for (int c=0;c<actions.size();c++) {
		Action action2 = (Action)actions.get(c);

		if (action!=action2 && keyStroke.equals( action2.getValue( TEMP_ACCELERATOR_KEY ) )) {
			action2.putValue( TEMP_ACCELERATOR_KEY , null );
		}
	}

	repaint();
    }

    private Action getCurrentAction() {
	return (Action)rescue.getActions().get( table.getSelectedRow() );
    }

    /** Closes the dialog */
    private void closeDialog() {

	List actions = rescue.getActions();
	for (int c=0;c<actions.size();c++) {
		((Action)actions.get(c)).putValue( TEMP_ACCELERATOR_KEY , null );
	}

        setVisible(false);
        dispose();
    }

	private JTable table;
	private JScrollPane makeKeyTable() {

		List actions = rescue.getActions();
		for (int c=0;c<actions.size();c++) {
			Action action = (Action)actions.get(c);
			action.putValue( TEMP_ACCELERATOR_KEY , action.getValue(Action.ACCELERATOR_KEY) );
		}


		AbstractTableModel tableModel = new AbstractTableModel() {

			private final String[] columnNames = { "Action", "Command", "Key" };

			public int getColumnCount() {
				return columnNames.length;
			}

			public int getRowCount() {

				return rescue.getActions().size();
  			}

			public String getColumnName(int col) {
				return columnNames[col];
			}

			public Object getValueAt(int row, int col) {

				Action action = (Action)rescue.getActions().get(row);

				switch(col) {

					case 0: return action.getValue( Action.NAME );
					case 1: return action.getValue( Action.ACTION_COMMAND_KEY );
					case 2: return action.getValue( TEMP_ACCELERATOR_KEY );
					default: throw new RuntimeException();

				}

			}

		};

		table = new JTable(tableModel);

		table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

		return new JScrollPane(table);


	}

}
