package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;
import java.awt.image.BufferedImage;
import java.awt.Color;

public abstract class SpaceObject extends Point {

    protected ImageObject[] SpaceObjects;
    protected Vector torpedos;

    public SpaceObject(int ix,int iy, ImageObject[] so, Vector t) {

	x=ix;
	y=iy;

	SpaceObjects = so;
	torpedos = t;

    }

    public void drawBigShape(Graphics g, boolean highLight) { }

    public void advance() { }

    public double getAngle(Point p) {

	    double angle =  Math.atan( ((double)(p.x-x))/((double)(y-p.y)) );

	    if ( p.y > y ) {
		angle = angle + Math.toRadians(180);
	    }
	    if (angle < 0) {
		angle = angle + Math.toRadians(360);
	    }

	    return angle;

    }

    public int getDistanceToPlayer() {

	return (int)distance( SpaceObjects[ SpaceObjects.length-1 ] );

    }

}
