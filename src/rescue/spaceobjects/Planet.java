package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Vector;

public class Planet extends MannedObject {

    private int counter;
    private int hitCounter;

    public Planet(int num,String t ,String n ,int ix ,int iy, ImageObject[] so, BufferedImage a, BufferedImage b, BufferedImage c, BufferedImage d, Vector z) {
	super(num,t,n,ix,iy,so,a,b,c,d,z);

	people=1;
	counter=50;

	hitCounter=0;

    }

    public void drawBigShape(Graphics g, boolean highLight) {

	g.drawImage(image,x-(image.getWidth()/2),y-(image.getHeight()/2),null);
	super.drawBigShape(g,highLight);

    }

    public void drawSmallShape(Graphics g, double sx, double sy, int scan) {

	g.drawImage(small,  (int)(x*sx)-(small.getWidth()/2)  ,(int)(y*sy)-(small.getHeight()/2),null);

	if (!isAlive()) { // if dead

	    g.setColor( Color.BLACK );
	    g.fillRect((int)(x*sx)-(small.getWidth()/2)+1  ,(int)(y*sy)-(small.getHeight()/2)+1 ,3,3 );

	}
	else if (hitCounter!=0) {

	    g.setColor( Color.RED );
	    g.drawRect((int)(x*sx)-(small.getWidth()/2)-1  ,(int)(y*sy)-(small.getHeight()/2)-1 ,6,6 );

	}

	//super.drawSmallShape(g,sx,sy,scan); // NOT NEEDED as all painting is done here

    }

    public void gotHit(MannedObject who, int hitpower, int hitlocation) {

	super.gotHit(who,hitpower,hitlocation);

	hitCounter=10;

    }

    public void advance() {

	//here
	if ( isAlive() ) {

	    if (people != 0 && counter>0 && teams != 0) {
		counter--;
	    }

	    if (people != 0 && counter==0 && teams != 0 && hasSelected( (Ship)SpaceObjects[SpaceObjects.length-1] ) ) {
		((Ship)SpaceObjects[SpaceObjects.length-1]).addPeople(people);
		people=0;
	    }
	}

	if (hitCounter!=0) { hitCounter--; }

	super.advance();
    }

}
