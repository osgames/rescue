package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;
import java.awt.image.BufferedImage;
import java.awt.Color;
//import java.util.Arrays;

public abstract class MannedObject extends ImageObject {

    // life of object
    protected int systemPhaserBanks=0;
    protected int systemTorpedoTubes=0;
    protected int systemShields=0;
    protected int systemComputer=0;
    protected int systemTransporters=0;
    protected int systemTractorBeam=0;
    // end life

    // INFO loaded from xml
    protected int INFOmax_phaserIntensity;
    protected int INFOmax_phaserbanks;
    protected int INFOmax_torpedoSalvo;
    protected int INFOmax_torpedosleft;

    protected int INFOmax_shieldsPower;
    protected int INFOmax_tractorPower;
    protected int INFOmax_totalenergy;
    protected int INFOmax_cloakPower;
    // INFO loaded from xml

    protected String type;
    protected int number;

    protected int[] SpaceObjectsLikes;

    protected int firingPhaser;
    protected MannedObject phaserTarget;

    protected int phaserIntensity; // 1 - 3 // MIN (1) - MED (2) - MAX (3)
    protected int TorpedoSalvo; // 1 - 3 // 1 or 2 or 3

    protected int target;

    protected int shieldPower;
    protected boolean ShieldsUp;

//    protected SpaceObject team;
    protected int teams;
    protected int people;

    protected int totalEnergy;
    protected int phaserBanks;
    protected int torpedosLeft;

    protected Ship tractoredShip;
    protected int tractorBeamPower;

    protected boolean cloaked;
    protected int cloakPower;

    protected boolean alive;

    protected int reserve;
    protected int reserveMAX=1000;

    public MannedObject(int num,String t ,String n ,int ix ,int iy, ImageObject[] so, BufferedImage a, BufferedImage b, BufferedImage c, BufferedImage d, Vector z) {

	super(ix,iy,so,z,a,b,c,d,n);

	number = num;
	type=t;
	x=ix;
	y=iy;
	SpaceObjects=so;

	phaserTarget=null;

	firingPhaser=0;

	target=0; // this is what to aim for on the ship

	alive=true;

	torpedos=z;

	// this will need to be changed for network play
	if (num != -1 && num == so.length-1) { teams=10; }
	else { teams = 0; }

	reserve=reserveMAX;

    }

    public void setInfo(int a, int b,int e, int f, int g, int h, int i, int j) {

        INFOmax_phaserIntensity=a;
        INFOmax_torpedoSalvo=b;
        INFOmax_tractorPower=e;
        INFOmax_cloakPower=f;

	INFOmax_totalenergy=g;
	INFOmax_phaserbanks=h;
	INFOmax_torpedosleft=i;
	INFOmax_shieldsPower=j;


	if (INFOmax_cloakPower>0) { cloakPower=INFOmax_cloakPower; cloaked=true; }
	if (INFOmax_shieldsPower>0) { shieldPower=INFOmax_shieldsPower; ShieldsUp=true; } // YURA:TODO this should not always be up

	totalEnergy = INFOmax_totalenergy;
	phaserBanks = INFOmax_phaserbanks;
	torpedosLeft = INFOmax_torpedosleft;
	tractorBeamPower = INFOmax_tractorPower;
	TorpedoSalvo = INFOmax_torpedoSalvo;
	phaserIntensity = INFOmax_phaserIntensity;

    }

    public int getShieldPower() {
	return shieldPower;
    }
    public int getTotalEnergy() {
	return totalEnergy;
    }
    public int getPhaserBanks() {
	return phaserBanks;
    }
    public int getTorpedosLeft() {
	return torpedosLeft;
    }

    public void setNoAwayTeams(int a) {
	teams=a;
    }

    public int getNoAwayTeams() {
	return teams;
    }

    public boolean doYouLike(MannedObject a) {

	return (SpaceObjectsLikes[a.getNumber()] > 0)?(true):(false);

    }

    public int getNumber() {
	return number;
    }

    public void setNumber(int a) {
	number=a;
    }

    public int getPeople() {

	return people;

    }
    public void addPeople(int a) {

	people = people + a;

    }

    public void beamTeamFrom(MannedObject from) {

	if ( from.beamTeamUp() ) {
	    teams++;

	    // if you taking over the ship!
	    if (this instanceof Ship && from instanceof Ship && SpaceObjectsLikes[SpaceObjectsLikes.length-1] < 1) {

		SpaceObjectsLikes = from.copyLikeInfo();

		// now tell everyone that "this" have same info as "from"

		for(int i=0; i< SpaceObjects.length ; i++) { //for(SpaceObject so : SpaceObjects) { // java 1.5 only

		    if (SpaceObjects[i] instanceof MannedObject) { ((MannedObject)SpaceObjects[i]).setLike(number,from.getNumber()); }

		    //so.setLike(number,from.getNumber());

		}

	    }


	}

    }
    public boolean beamTeamUp() {

	if (teams>0) { teams--; return true; }
	return false;

    }
    public int[] copyLikeInfo() {

	int[] a = new int[SpaceObjectsLikes.length];

	System.arraycopy(SpaceObjectsLikes,0,a,0,a.length);

	return a;
    }
    public void setLike(int a,int b){

	SpaceObjectsLikes[a] = SpaceObjectsLikes[b];

    }

    public void TractorShip(Ship a) {

	if (a!=null && systemTractorBeam==0) {

	    a.setOwner(this);
	    tractoredShip = a;

	}
	else if (tractoredShip!=null) {

	    tractoredShip.setOwner(null);
	    tractoredShip = null;
	}

    }

    public void TorpedoHit(MannedObject who) {

	//System.out.print("I am "+name+" and i just got a Torpedo in my ass from "+who.getName()+"\n");

	//int z = SpaceObjects.indexOf(who);
	//System.out.print("Torpedo hit from: "+ who.getName() +" ("+z+")\n");

	// 10 is the strenght of the torpedo

	gotHit(who,6,0);

    }

    public void PhaserHit(MannedObject who,int a,int t) {

	//System.out.print("I am "+name+" and i just got a Phaser of power "+a+" in my ass from "+who.getName()+"\n");

	//int z = SpaceObjects.indexOf(who);
	//System.out.print("Phaser hit from: "+ who.getName() +" ("+z+")\n");

	// YURA: maybe phaser of power 1 should more damage more then just 1 off
	// a = 1 or 2 or 3

	gotHit(who,a,t);

    }


    // when phasers hit the strength is 1 or 2 or 3 and when torpedo hits the strength is 6
    public void gotHit(MannedObject who, int hitpower, int hitlocation) {

	//System.out.print("1: "+hitpower+" "+shieldPower+"\n");

	// find everyone who hates me and put there like for "WHO" up!
	// find everyone who likes me and put there like for "WHO" down! (i like me)

	// YURA:TODO, THIS IS SLOW!!! BUT IS THE ONLY PLACE THAT IT USED!!
	//int c=-1; // = Arrays.binarySearch((SpaceObject[])SpaceObjects,(SpaceObject)who); // indexOf
	//int b=-1; // = Arrays.binarySearch((SpaceObject[])SpaceObjects,(SpaceObject)this); // indexOf

	//for(int i=0; i< SpaceObjects.length ; i++) {

	    //if (who == SpaceObjects[i]) { c = i; }
	    //if (this == SpaceObjects[i]) { b = i; }

	//}

	int c = who.getNumber();
	int b = number;

	for(int i=0; i< SpaceObjectsLikes.length ; i++) {

	    // im going to tell you that i got shot by "who"(c) and i am "this"(b)
	    if (SpaceObjects[i] instanceof MannedObject) { ((MannedObject)SpaceObjects[i]).youShouldLike(c,b); }

	    //if ((SpaceObject)SpaceObjects.elementAt(i)) LIKES_ME ((SpaceObject)SpaceObjects.elementAt(i)) HATE "WHO"(in LikeVector at "a");
	    //if ((SpaceObject)SpaceObjects.elementAt(i)) HATES_ME ((SpaceObject)SpaceObjects.elementAt(i)) LIKE "WHO"(in LikeVector at "a");

	}

	// ############ now do the damage! (shields)

	//the shield power from 0.01 to 1
	double spower = Math.max(shieldPower,1)/100d;

	// the initial damage from 5 to 30
	int damage = hitpower*5;

	if (ShieldsUp) {

	    int hitshields = (int)(damage*spower);

	    shieldPower = shieldPower-damage;
	    damage = damage - hitshields;

	    if (shieldPower < 5) { shieldPower=0; }

	}

	//damage = (int)Math.round( 2*(hitpower*10)*(weekness) ); // damage WILL be /2 later

	// ############ now do the damage! (ship) us up the damage int

	//System.out.print("2: "+damage+" "+shieldPower+"\n");

	while (damage!=0) {

	    if (hitlocation==0) {

		do {

		    hitlocation = (int)Math.round( Math.random() * 9 )+1;

		} while (getSystem(hitlocation)==10); // if it is a BASE then this will be OK (to damage system that does not exist)


	    }

	    if (hitlocation==1 || hitlocation==2 || hitlocation==5 || hitlocation==6) { // warp + impulse + scans

		if (this instanceof Ship) {
		    damage = ((Ship)this).shipDamage(hitlocation,damage);
		}

	    }
	    else if (hitlocation==3) { // phaser

		systemPhaserBanks = systemPhaserBanks - damage;
		if (systemPhaserBanks < -10) { damage = damage - (systemPhaserBanks+10); systemPhaserBanks=-10; }

	    }
	    else if (hitlocation==4) { // torpedo

		systemTorpedoTubes = systemTorpedoTubes - damage;
		if (systemTorpedoTubes < -10) { damage = damage - (systemTorpedoTubes+10); systemTorpedoTubes=-10; }

	    }
	    else if (hitlocation==7) { // shields

		systemShields = systemShields - damage;
		if (systemShields < -10) { damage = damage - (systemShields+10); systemShields=-10; }

	    }
	    else if (hitlocation==8) { // computer

		systemComputer = systemComputer - damage;
		if (systemComputer < -10) { damage = damage - (systemComputer+10); systemComputer=-10; }

	    }
	    else if (hitlocation==9) { // transporters

		systemTransporters = systemTransporters - damage;
		if (systemTransporters < -10) { damage = damage - (systemTransporters+10); systemTransporters=-10; }

	    }
	    else if (hitlocation==10) { // tractor

		systemTractorBeam = systemTractorBeam - damage;
		if (systemTractorBeam < -10) { damage = damage - (systemTractorBeam+10); systemTractorBeam=-10; }

	    }

	    // though fires other things get damaged
	    damage = damage/4; // if hitlocation 0 then its just devided by 3 b4 doing anything.
	    hitlocation = (int)Math.round( Math.random() * 10 );

	}


/*

	if (ShieldsUp && shieldPower > 0) {
	    shieldPower=shieldPower-10;

	    if (shieldPower < 0) { tmpTotalPower=tmpTotalPower+shieldPower; shieldPower=0; }

	}
	else {
	    tmpTotalPower=tmpTotalPower-50;
	}
*/

	// ######## setup the explosion

	checkdead();







	    if (ShieldsUp && shieldPower>0) { // shields hit // for 10


		torpedos.add( new ParticleEffect(x,y,SpaceObjects,torpedos,20,Color.YELLOW,0,3,0, getSize()/2 ,false,5) );

		//g.fillOval(x-s,y-s,s*2,s*2);

	    }
	    else if (!alive) { // explode // for 100

		torpedos.add( new ParticleEffect(x,y,SpaceObjects,torpedos,50,Color.RED,0,3,0, getSize() ,false,5) );

		//g.fillOval(x-(s*2),y-(s*2),s*4,s*4);

	    }
	    else { // small hit // for 10

		torpedos.add( new ParticleEffect(x,y,SpaceObjects,torpedos,20,Color.RED,0,3,0, getSize()/2 ,false,5) );

		//g.fillOval(x-s,y-s,s*2,s*2);

	    }




	//if (alive) {
	//    hitCounter=10;
	//}
	//else {
	//    hitCounter=100;
	//}

    }

    // YURA:TODO bases should have extra life too.
    public void checkdead() {

	int a = getLifeInfo();

	//if (this instanceof Ship) {

	//    if (a==-100) { alive=false; }

	//    return;

	//}

	if (a<=-60) {
	    alive=false;
	}

    }

    public int getLifeInfo() {

	int a = systemPhaserBanks + systemTorpedoTubes + systemShields + systemComputer + systemTransporters + systemTractorBeam;

	if (this instanceof Ship) {

	    a = a + ((Ship)this).shipGetLifeInfo();
	}

	return a;

    }

    public boolean checkFixNeeded() {

	int a = systemPhaserBanks + systemTorpedoTubes + systemShields + systemComputer + systemTransporters + systemTractorBeam;

	if (this instanceof Ship) {

	    a = a + ((Ship)this).shipGetLifeInfo();

	}

	if (a==0) {
	    return false;

	}

	return true;

    }

    /**
     * a = who you should like
     * b = who is saying it
     *
     * I just got shot by "a" and i am "b", make your mind up
     * You should like "a" IF you hate "b"
     */
    public void youShouldLike(int a,int b) {

	//YURA: if i hate "a" more then 5 then i will not start to like it

	//System.out.print("youShouldLike a="+a+" b="+b+"\n");

	if ( SpaceObjectsLikes[b] < 0 ) { // if i hate "b" then i should like "a"

	    SpaceObjectsLikes[a] = SpaceObjectsLikes[a]+1;
	    //System.out.print("I am "+name+" and i like "+((SpaceObject)SpaceObjects.elementAt(a)).getName() +" now\n");
	}
	else if ( SpaceObjectsLikes[b] > 0 ) { // if i like "b" then i hate "a" more

	    SpaceObjectsLikes[a] = SpaceObjectsLikes[a]-1;
	    //System.out.print("I am "+name+" and i hate "+((SpaceObject)SpaceObjects.elementAt(a)).getName() +" now\n");
	}

    }

    public boolean enemyOf(int a) {

	if ( SpaceObjectsLikes[a] < 0 ) { return true; }

	return false;

    }

    public boolean friendOf(int a) {

	if ( SpaceObjectsLikes[a] > 0 ) { return true; }

	return false;

    }

    public String getType() {
	return type;
    }

    public void setLikes(int[] v) {
	SpaceObjectsLikes=v;

    }

    public boolean isAlive() {

	//if (tmpTotalPower > 0) { return true; }
	//return false;

	return alive;

    }

    public void drawBigShape(Graphics g, boolean highLight) {

	//g.setColor( Color.WHITE );
	//g.fillOval(x-5,y-5,10,10);

	if (firingPhaser!=0) {

	    g.setColor( Color.RED );
	    g.drawLine(x,y,phaserTarget.x,phaserTarget.y);

	}

	if (tractoredShip!=null) {

	    g.setColor( Color.LIGHT_GRAY );
	    g.drawLine(x,y,tractoredShip.x,tractoredShip.y);

	}

	if (highLight) {

	    int s = getSize();
	    g.setColor(new Color(255,0,0));
	    g.drawOval(x-s,y-s,s*2,s*2);

	}

    }

    public void drawSmallShape(Graphics g, double sx, double sy, int scan) {

	if ( alive && (!cloaked || scan>=cloakPower ) ) { 

	    super.drawSmallShape(g,sx,sy,scan);

	}

    }

    public int getCloakPower() {
	return cloakPower;
    }

    public boolean hasSelected(Point p) {

	return (alive && super.hasSelected(p));

    }

    public void FireTorpedo(MannedObject p) {

      if (torpedosLeft > 0 && systemTorpedoTubes==0) {

	    if (TorpedoSalvo > torpedosLeft) { TorpedoSalvo=torpedosLeft; }

	    torpedosLeft=torpedosLeft-TorpedoSalvo;

	    if (TorpedoSalvo==1) {
		FireTorpedo(p,getAngle(p));
	    }
	    else if (TorpedoSalvo==2) {

		double c=getAngle(p);

		//FireTorpedo(p, (Math.toDegrees(c) < 355)?(c+Math.toRadians(5)):(c-Math.toRadians(355)) );
		//FireTorpedo(p, (Math.toDegrees(c) >= 5)?(c-Math.toRadians(5)):(c+Math.toRadians(355)) );

		FireTorpedo(p, (Math.toDegrees(c) < 350)?(c+Math.toRadians(10)):(c-Math.toRadians(350))  );
		FireTorpedo(p, (Math.toDegrees(c) >= 10)?(c-Math.toRadians(10)):(c+Math.toRadians(350))  );
	    }
	    else {

		double c=getAngle(p);

		FireTorpedo(p, (Math.toDegrees(c) < 350)?(c+Math.toRadians(10)):(c-Math.toRadians(350))  );
		FireTorpedo(p,c);
		FireTorpedo(p, (Math.toDegrees(c) >= 10)?(c-Math.toRadians(10)):(c+Math.toRadians(350))  );
	    }
	}

    }

    public void FireTorpedo(MannedObject p, double a) {

	// the new torpedo vector system
	torpedos.add( new Torpedo(x,y,SpaceObjects,torpedos,p,this,a) );

    }


    public void FirePhaser(MannedObject p) {

	// YURA:TODO check that phasers are not broken
	// removed check on distance, now it happens in RunRescue, this is to spot AI errors
	if (phaserBanks >= 50 && systemPhaserBanks==0) {

	    if ((phaserIntensity*50) > phaserBanks) { phaserIntensity = phaserBanks/50; }

	    phaserBanks = phaserBanks - (phaserIntensity*50);

	    phaserTarget=p;
	    firingPhaser=10; // YURA TEMP as the strength has nothing to do with how long it takes!!!

	    phaserTarget.PhaserHit(this,phaserIntensity,target);

	}

    }

    // NOT NEEDED AS THERE IS A METHOD IN POINT CALLED "distance"
    // maybe have 2 methods getDistancePoint() getDistanceShape()
    //public int getDistance(Point p) {
	//return (int)Math.sqrt(Math.pow(p.x-x,2)+Math.pow(p.y-y,2));
    //}

    public int getTractorBeamPower() {
	if (tractoredShip!=null) {
	    return tractorBeamPower;
	}
	return 0;
    }
    public int getTractorBeamMaxPower() {
	return INFOmax_tractorPower;
    }

    public int getMaxtotalenergy() {
	return INFOmax_totalenergy;
    }
    public int getMaxphaserbanks() {
	return INFOmax_phaserbanks;
    }
    public int getMaxtorpedosleft() {
	return INFOmax_torpedosleft;
    }
    public int getMaxshieldpower() {
	return INFOmax_shieldsPower;
    }

    public int getEnergy(int nrg) {

	if (totalEnergy>nrg) { totalEnergy = totalEnergy - nrg; return nrg; }
	int a = totalEnergy;
	totalEnergy=0;
	return a;

    }

    public int getPhaser(int nrg) {

	if (phaserBanks>nrg) { phaserBanks = phaserBanks - nrg; return nrg; }
	int a = phaserBanks;
	phaserBanks=0;
	return a;

    }

    public int getTorpedos(int nrg) {

	if (torpedosLeft>nrg) { torpedosLeft = torpedosLeft - nrg; return nrg; }
	int a = torpedosLeft;
	torpedosLeft=0;
	return a;

    }

    public void fixSystems() {

	if (systemPhaserBanks<0) { systemPhaserBanks++; return; }
	if (systemTorpedoTubes<0) { systemTorpedoTubes++; return; }
	if (systemShields<0) { systemShields++; return; }
	if (systemComputer<0) { systemComputer++; return; }
	if (systemTransporters<0) { systemTransporters++; return; }
	if (systemTractorBeam<0) { systemTractorBeam++; return; }

    }

    public int getSystem(int sys) {

	if (sys==3) { return systemPhaserBanks; }
	if (sys==4) { return systemTorpedoTubes; }
	if (sys==7) { return systemShields; }
	if (sys==8) { return systemComputer; }
	if (sys==9) { return systemTransporters; }
	if (sys==10) { return systemTractorBeam; }

	if (this instanceof Ship) {
	    return ((Ship)this).shipGetSystem(sys);
	}
	return 0;
    }

    private int counter=0;
    public void advance() {

	if (alive) {

	    if (counter<50) { counter++; }
	    else if (counter==50) { counter=0; }

	    // YURA:TODO b4 any energy is taked anway you need to test that there is enough!

	    if (phaserBanks < INFOmax_phaserbanks && totalEnergy > 1) { totalEnergy--; phaserBanks++; }

	    if (ShieldsUp) { if (totalEnergy>5) {totalEnergy = totalEnergy-5;} else {ShieldsUp=false;} }


	    if (counter==0 && shieldPower < INFOmax_shieldsPower && totalEnergy > 20) { shieldPower++; totalEnergy = totalEnergy -20; }


	    if (tractoredShip!=null) {

		int te = tractoredShip.tractorEnergyUsed(INFOmax_tractorPower);

		if (totalEnergy>te) {
		    totalEnergy = totalEnergy - te;

		    // energy being used up by tractor beam (max = INFOmax_tractorPower);
		    tractorBeamPower=te;

		}
		else {
		    TractorShip(null);
		}

	    }

	    if (counter==0 && checkFixNeeded() && totalEnergy>300 ) {

		fixSystems();
		totalEnergy = totalEnergy-300;

	    }

	    if (reserve < reserveMAX && totalEnergy > 1) { totalEnergy--; reserve++; }

	    // charge the energy bank
	    if (totalEnergy < INFOmax_totalenergy) { totalEnergy = totalEnergy +10; }
	    if (totalEnergy > INFOmax_totalenergy) { totalEnergy = INFOmax_totalenergy; }

	}

	if (firingPhaser!=0) { firingPhaser--; }

	    // if (firingPhaser==0) { phaserTarget.PhaserHit(this,phaserIntensity,target); }

    }

}
