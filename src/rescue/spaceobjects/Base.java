package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Vector;

public class Base extends MannedObject {

    public Base(int num, String t ,String n ,int ix ,int iy, ImageObject[] so, BufferedImage a, BufferedImage b, BufferedImage c, BufferedImage d, Vector z) {
	super(num,t,n,ix,iy,so,a,b,c,d,z);

	target=1;

    }

    public void drawBigShape(Graphics g, boolean highLight) {

	g.drawImage(image,x-(image.getWidth()/2),y-(image.getHeight()/2),null);
	super.drawBigShape(g,highLight);

    }

    private int counter=0;
    public void advance() {

	// YURA:TODO check that the systems to shoot are working, The base must be much stronger then it is now

      if ( alive ) {

	if (counter==10) { counter=0; }
	else { counter++; }

	MannedObject thehated=null;
	int tmp=0; // how bad is the ship to target

	for(int i=0; i< SpaceObjectsLikes.length ; i++) {

	  if (SpaceObjects[i] instanceof Ship) {

	    int tmp2 = 0;
	    if (((Ship)SpaceObjects[i]).getSystem(4)==0) { tmp2=tmp2+8; }
	    if (((Ship)SpaceObjects[i]).getSystem(3)==0) { tmp2=tmp2+4; }
	    if (((Ship)SpaceObjects[i]).getSystem(1)==0) { tmp2=tmp2+2; }
	    if (((Ship)SpaceObjects[i]).getSystem(1)==0) { tmp2=tmp2+1; }

	    if ( ((Ship)SpaceObjects[i]).isAlive() && 
		((int)distance( SpaceObjects[i] ) < rescue.RunRescue.DISTANCE_TORPEDO) &&
		(SpaceObjectsLikes[i] < 0) &&
		tmp2 >= tmp
		) {

			thehated = (MannedObject)SpaceObjects[i];
			tmp = tmp2;
	    }

	  }

	}

	if (thehated!=null && counter==0 ) {

	    int dis = (int)distance(thehated);

	    if (((Ship)thehated).getSpeed()==0) {

		TorpedoSalvo=INFOmax_torpedoSalvo;
		FireTorpedo(thehated);

	    }
	    else {

		if (dis > rescue.RunRescue.DISTANCE_PHASERS) {

		    TorpedoSalvo=1;
		    FireTorpedo(thehated);

		}
		else {

		    if (target==1) { target=2; }
		    else if (target==2) { target=1; }

		    FirePhaser(thehated);

		}


	    }



	}


      }
      super.advance();
    }

}
