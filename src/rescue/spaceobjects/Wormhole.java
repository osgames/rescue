package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Vector;
import java.awt.Point;

public class Wormhole extends ImageObject {

    private SpaceObject ejected;
    private int life;

    public Wormhole(int num,String t ,String n ,int ix ,int iy, ImageObject[] so, BufferedImage a, BufferedImage b, BufferedImage c, BufferedImage d, Vector z) {
	super(ix,iy,so,z,a,b,c,d,n);

	life=(int)(Math.random() * 1000);

    }

    public void drawBigShape(Graphics g, boolean highLight) {

	g.drawImage(image,x-(image.getWidth()/2),y-(image.getHeight()/2),null);
	super.drawBigShape(g,highLight);

    }

    public void sendObject(SpaceObject a) {

	a.x = x;
	a.y = y;

	ejected = a;

    }

    public void advance() {

	life++;

	if (life==1000) {

	    x=(int)(Math.random() * rescue.RunRescue.WIDTH);
	    y=(int)(Math.random() * rescue.RunRescue.HEIGHT);
	    life=0;

	}

	SpaceObject found=null;

	for(int i=0; i< SpaceObjects.length ; i++) {

	    if ( SpaceObjects[i]!=this && hasSelected( SpaceObjects[i] ) ) {

		found = SpaceObjects[i];

	    }

	}

	if (found != null && found != ejected) {

	    Vector Wormholes = new Vector();

	    for(int i=0; i< SpaceObjects.length ; i++) {

		if ( SpaceObjects[i]!=this && SpaceObjects[i] instanceof Wormhole ) {

		    Wormholes.add(SpaceObjects[i]);

		}

	    }

	    ((Wormhole)Wormholes.elementAt( (int)Math.round(Math.random() * (Wormholes.size()-1) ) )).sendObject(found);

	}
	else if (found == null && ejected !=null) {
	    ejected = null;
	}


    }

}
