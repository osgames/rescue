package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;
import java.util.Vector;

public class Torpedo extends SpaceObject {

    private int counter;
    private MannedObject target;
    private MannedObject from;
    private double direction;

    public Torpedo(int ix,int iy, ImageObject[] so, Vector t,MannedObject p,MannedObject f, double a) {
	super(ix,iy,so,t);

	target=p;
	from=f;
	direction=a;

	// YURA:TODO if wormhole then just go in that direction

	counter=rescue.RunRescue.DISTANCE_TORPEDO/10;

    }

    public void drawBigShape(Graphics g, boolean highLight) {

	g.setColor( Color.RED );
	//g.fillOval(x-10,y-10,20,20);

g.drawLine(

x+(int)Math.round(10 * ( Math.sin( Math.toRadians((counter*10)) ) )),

y+(int)Math.round(10 * ( Math.cos( Math.toRadians((counter*10)) ) )),

x-(int)Math.round(10 * ( Math.sin( Math.toRadians((counter*10)) ) )),

y-(int)Math.round(10 * ( Math.cos( Math.toRadians((counter*10)) ) ))

);

g.drawLine(

x+(int)Math.round(10 * ( Math.sin( Math.toRadians((counter*10)+90) ) )),

y+(int)Math.round(10 * ( Math.cos( Math.toRadians((counter*10)+90) ) )),

x-(int)Math.round(10 * ( Math.sin( Math.toRadians((counter*10)+90) ) )),

y-(int)Math.round(10 * ( Math.cos( Math.toRadians((counter*10)+90) ) ))

);

	g.setColor( Color.YELLOW );

g.drawLine(

x+(int)Math.round(5 * ( Math.sin( Math.toRadians((counter*10)+45) ) )),

y+(int)Math.round(5 * ( Math.cos( Math.toRadians((counter*10)+45) ) )),

x-(int)Math.round(5 * ( Math.sin( Math.toRadians((counter*10)+45) ) )),

y-(int)Math.round(5 * ( Math.cos( Math.toRadians((counter*10)+45) ) ))

);

g.drawLine(

x+(int)Math.round(5 * ( Math.sin( Math.toRadians((counter*10)+135) ) )),

y+(int)Math.round(5 * ( Math.cos( Math.toRadians((counter*10)+135) ) )),

x-(int)Math.round(5 * ( Math.sin( Math.toRadians((counter*10)+135) ) )),

y-(int)Math.round(5 * ( Math.cos( Math.toRadians((counter*10)+135) ) ))

);

    }

    private void adjustSpeed(double angle) {

	double wantturn = rescue.RunRescue.getTurn(angle,direction);

	if (wantturn > Math.toRadians(1) ) {

	    direction = direction + Math.toRadians(1);

	}
	else if (wantturn < Math.toRadians(1) ) {

	    direction = direction - Math.toRadians(1);

	}
	else {

	    direction = direction + wantturn;

	}

	// the new direction can NEVER be less then ZERO or more then or equal to 360!!!
	if (direction <0) { direction = direction + Math.toRadians(360); }
	else if (direction >= Math.toRadians(360) ) { direction = direction - Math.toRadians(360); }

    }

    public void advance() {

	counter--;

	if ( target.isAlive() && target.hasSelected(this) ) { target.TorpedoHit(from); torpedos.remove(this); return; }

	if (counter<=0) { torpedos.remove(this); return; } // System.out.print("DIE!\n");

	adjustSpeed( (target.isAlive())?(getAngle(target)):(direction) );

	// same as moveTo() in Ship
	translate( (int)Math.round(10 * ( Math.sin(direction) )) , -(int)Math.round(10 * ( Math.cos(direction) ) ) );

    }

}
