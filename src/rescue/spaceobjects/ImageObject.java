package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;
import java.awt.image.BufferedImage;
import java.awt.Color;

public abstract class ImageObject extends SpaceObject {

    protected BufferedImage image;
    protected BufferedImage small;
    protected BufferedImage icon;
    protected BufferedImage info;

    protected String name;

    public ImageObject(int ix, int iy, ImageObject[] so, Vector t, BufferedImage a, BufferedImage b, BufferedImage c, BufferedImage d, String n) {
	super(ix,iy,so,t);

	// set up the images
	image=a;
	small=b;
	icon=c;
	info=d;

	name = n;

    }

    //YURA: returns the RADIUS of the object
    public int getSize() {

	if (image.getWidth() > image.getHeight()) { return image.getWidth()/2; }
	else { return image.getHeight()/2; }

    }

    public boolean hasSelected(Point p) {

	return ( distance(p) < getSize() );

    }

    public BufferedImage getInfoImage() {
	return info;
    }

    public BufferedImage getIcon() {
	return icon;
    }

    public String getName() {
	return name;
    }

    public void drawSmallShape(Graphics g, double sx, double sy, int scan) {

	g.drawImage(small,  (int)(x*sx)-(small.getWidth()/2)  ,(int)(y*sy)-(small.getHeight()/2),null);

    }

}
