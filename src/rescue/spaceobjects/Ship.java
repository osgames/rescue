package rescue.spaceobjects;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Vector;
import java.awt.Point;
import java.awt.Graphics2D;
import rescue.IndicatorsListener;

public class Ship extends MannedObject {


    // life of ship
    protected int systemWarpDrive=0;
    protected int systemImpulseDrive=0;
    // end life

    // INFO loaded from xml
    protected boolean jump;
    protected boolean turn;
    // INFO loaded from xml

    protected Point autoImpulse;
    protected Point autoWarp;

    protected double manualImpulse;
    protected double manualWarp;

    protected int impulseSpeed;
    protected int warpSpeed;

    protected int speed;
    protected double direction;

    // if impulse/warp is set to auto or manual
    protected boolean autoImpulseOn;
    protected boolean autoWarpOn;

    // if impulse or warp is on or not
    protected boolean impulseOn;
    protected boolean warpOn;
    protected boolean stop;

    private int counter;
    private int warpWormUpTime;
    private int warpTurnSpeed;

    protected MannedObject owner;

    protected int warpFactor;
    protected double impulseFactor;

    protected int evasive;
    protected boolean player;

























    // life of ship
    protected int systemLongRangeScan=0;
    protected int systemSensorScan=0;
    // end life

    // INFO loaded from xml
    private int INFOmax_warp;
    private double INFOmax_impulse;
    // INFO loaded from xml

    private int SensorScan; // 0=off 1=min 2=mid 3=max

    private int fixPriority=0;

    private IndicatorsListener indicatorsListener;

    public Ship(int num, String t ,String n ,int ix ,int iy, ImageObject[] so, BufferedImage a, BufferedImage b, BufferedImage c, BufferedImage d, Vector z) {

	super(num,t,n,ix,iy,so,a,b,c,d,z);

	speed = 0;
	direction = 0;

	manualImpulse=0;
	manualWarp=0;

	autoImpulseOn=true;
	autoWarpOn=true;

	impulseOn=false;
	warpOn=false;

	stop=true;

	//jump=true; and turn not set here any more, look in missionObject for DEFAULT VALUE

	counter=0;
	warpWormUpTime=50;
	warpTurnSpeed=25;


    }

    public void setOwner(MannedObject a) {

	owner = a;

    }

    // THIS METHOD ADJUSTS THE ANGLE TOO!!!!
    public void adjustSpeed(int distance, int neededSpeed, double angle) {


	    // YURA: for some ships life borg if they r still they can move in any direction
	    if (angle != direction && speed<100 && speed>0 && !(java.lang.Double.isNaN(angle)) ) {

		if (turn) {

		    double t = rescue.RunRescue.getTurn(angle,direction);

		    // YURA:this line needs to be improved
		    // if small angle then high speed is ok

		    double turnShip = Math.toRadians(  ((t>0)?(30):(-30)) * (1/(speed*3d))  );

		    //System.out.print(Math.toDegrees(turnShip)+" ");

		    if ( ( turnShip>0 && (turnShip > t) ) || ( turnShip<0 && (turnShip < t) ) ) { turnShip = t; }

		    //System.out.print(Math.toDegrees(turnShip)+"\n");

		    direction = direction + turnShip;

		    // the new direction can NEVER be less then ZERO or more then or equal to 360!!!
		    if (direction <0) { direction = direction + Math.toRadians(360); }
		    else if (direction >= Math.toRadians(360) ) { direction = direction - Math.toRadians(360); }

		}
		else { // YURA: NEW not tested

		    direction = angle;

		}

	    }
	    // dont think will adjust for small angels change!!!!! (coz ship will not slow down) // but then again when the ship gets close the angle increases
	    // if angle between "angle" and "direction" is small then its ok, or speed must be low
	    // if speed too fast then only turn by a bit, angle = direction + (angle<direction)?(-10):(10) , 10 must be (100-speed)*someSensibleNumber if 100 is max speed of this ship


	    if (!stop && jump && warpOn && (distance<speed) ) {
		stop=true;
	    }
	    else if (!stop && (!jump || impulseOn) && (distance < Math.max( ((speed*speed)/2),1 ) ) ) {
		stop=true;
	    }



	// YURA:this should only slow down if angle of change is bigger then 90?
	if (stop && speed>0 ) {	// or if angle between angle of flight and direction is big //direction != 555555555
	    if (warpOn && jump) { speed=1; counter=0; }
	    speed--;					//System.out.print("reduce speed now (stop)! speed="+speed+" distance="+distance+ " " +Math.toDegrees(direction)+ "\n");
	}
	else if ( ((  (direction - angle) > Math.toRadians(5) || (direction - angle) < Math.toRadians(-5) )) && ((impulseOn && speed>1) || (warpOn && speed>warpTurnSpeed)) ) {
	    if (warpOn && jump) { speed=warpTurnSpeed+1; }
	    speed--;					//System.out.print("reduce speed now (turn)! speed="+speed+" distance="+distance+ " " +Math.toDegrees(direction)+ "\n");

	}
	else if (speed > neededSpeed) {
	    if (warpOn && jump) { speed=neededSpeed; }					//System.out.print("reduce speed now (slow)! speed="+speed+" distance="+distance+ " " +Math.toDegrees(direction)+ "\n");
	    else { speed--; }
	}
	else if ((!stop) && (speed < neededSpeed)) {
	    if (warpOn && jump) {
		if (counter<warpWormUpTime) { counter++; speed++; }
		else if ((int)(direction*1000)==(int)(angle*1000)) { speed=neededSpeed; }

	    }
	    else {
	        speed++;					//System.out.print("increase speed now! speed="+speed+" distance="+distance+ " " +Math.toDegrees(direction)+ "\n");
	    }
	}

	if (stop && speed==0 && impulseOn) {
	    impulseOn=false;
	    changeEvasiveAndFireIndicatorUpdate(0);
	    if (player) { rescue.JukeBox.stop("background"); }
	}
	else if (stop && speed==0 && warpOn) {
	    warpOn=false;
	    if (player) { rescue.JukeBox.stop("background"); }
	}

    }

    public int getSpeed() {
	return speed;
    }

    private boolean leftright;
    public void advanceShipLocation() {

	int eneeded = (impulseOn)?((int)(impulseFactor*4)):(((warpFactor*(warpFactor-1))/2)+3);

	if (owner!=null) {

	    int a = (int)distance(owner);

	    if (a>rescue.RunRescue.DISTANCE_TRACTOR || owner.getSystem(10)!=0) {

		// escape
		owner.TractorShip(null);

	    }
	    else {

		Point p;

		if (owner instanceof Ship && ((Ship)owner).getSpeed() > 0) {

	// YURA:TODO with this method another ship CAN NOT tractor the players ship as moveto wont work if the ships not facing the direction its traveling
	// does this mean there should be 2 direction doubles? 1 for ship and 1 for direction of travel.
		    p = ((Ship)owner).moveTo();

		    translate(p.x,p.y);

		}
		else if (a>0) {

		    autoImpulse = owner;
		    autoImpulseOn=true;
		    impulseOn=true;
		    stop=false;

		    adjustSpeed( a, 1 , getAngle(owner) ); // used 1 instead of impulseSpeed

		    p = moveTo();

		    translate(p.x,p.y);

		}


	    }


	}
	else if (impulseOn) {

	    if (!((totalEnergy > eneeded && systemImpulseDrive==0 && alive)|| !alive)) {

		stop=true;

	    }

		// thse methods ignor what you want and do what u need
		if (evasive==1) {

		    adjustSpeed( 1000000, impulseSpeed, direction+Math.toRadians(1) );

		}
		else if (evasive==2) {

		    if (autoImpulse==null) { autoImpulse = new Point(x+50, y+50); }

		    if (distance(autoImpulse) > rescue.RunRescue.DISTANCE_TORPEDO ) {

			manualImpulse = this.getAngle(autoImpulse) + Math.toRadians(10);

			if (manualImpulse > Math.toRadians(360) ) { manualImpulse = manualImpulse - Math.toRadians(360); }

		    }

		    adjustSpeed( 1000000, impulseSpeed, manualImpulse );
		}

		else if (evasive==3) {

		    int diz = (int)Math.round(Math.toDegrees(direction));

		    if (diz==0 || diz==90 || diz==180 || diz==270 || diz==360) {

			//System.out.print("diz: "+diz+"\n");

			leftright = (Math.random()<0.5)?(true):(false);
		    }

		    if (leftright) {

			adjustSpeed( 1000000, impulseSpeed, direction+Math.toRadians(-1) );

		    }
		    else {

			adjustSpeed( 1000000, impulseSpeed, direction+Math.toRadians(1) );

		    }
		}
		else if (autoImpulseOn) {
		    adjustSpeed( (int)distance(autoImpulse), impulseSpeed, getAngle(autoImpulse) );
		}
		else {
		    adjustSpeed(1000000 , impulseSpeed, manualImpulse);
		}

		Point p = moveTo();

		translate(p.x,p.y);

		// ENERGY STUFF
		if (!stop && alive) { totalEnergy=totalEnergy-eneeded; }


	}
	else if (warpOn) {

	    if (!(totalEnergy > eneeded && systemWarpDrive==0 && alive)) {

		stop=true;

	    }
		if (autoWarpOn) {
		    adjustSpeed( (int)distance(autoWarp), warpSpeed, getAngle(autoWarp) );
		}
		else {
		    adjustSpeed(1000000 , warpSpeed, manualWarp);
		}

		Point p = moveTo();

		translate(p.x,p.y);

		// ENERGY STUFF
		if (!stop) { totalEnergy=totalEnergy-eneeded; }


	}
    }

    /** this method returns a point that is the diference from the current point to where the ship will end up with its present speed and direction */
    public Point moveTo() {

	//if ((warpOn && systemWarpDrive==0)||(impulseOn && systemImpulseDrive==0)) {

	    return new Point( (int)Math.round(speed * ( Math.sin(direction) )) , -(int)Math.round(speed * ( Math.cos(direction) ) ) );

	//}
	//return new Point(0,0);

    }



































    public void fixSystems() {

	int fix;

	if (fixPriority==0 || (fixPriority!=0 && getSystem(fixPriority)==0) ) {

	    int[] tmp = new int[10];

	    // fill array with the system values
	    for(int i=0; i< tmp.length; i++) {

		tmp[i]=getSystem(i+1);

	    }

	    // find the lowest number
	    int low=tmp[0];
	    int lowi=0;
	    for(int i=1; i< tmp.length; i++) {

		if (tmp[i]<low) { low=tmp[i]; lowi=i; }

	    }

	    fix = lowi+1;

	}
	else {

	    fix = fixPriority;

	}

	if (getSystem(fix) < 0) { fixSystem(fix); }

    }

    public void fixSystem(int fix) {

	if (fix==1) { systemWarpDrive++; return; }
	if (fix==2) { systemImpulseDrive++; return; }
	if (fix==3) { systemPhaserBanks++; return; }
	if (fix==4) { systemTorpedoTubes++; return; }
	if (fix==5) { systemLongRangeScan++; return; }
	if (fix==6) { systemSensorScan++; return; }
	if (fix==7) { systemShields++; return; }
	if (fix==8) { systemComputer++; return; }
	if (fix==9) { systemTransporters++; return; }
	if (fix==10) { systemTractorBeam++; return; }

	System.out.print("ERROR, asked to fix system "+fix+"\n");

    }

    public String getSystemText(int sys) {

	int a = getSystem(sys);

	if (a==0) { return "OK"; }

	return (-a)+" rpr";

    }

    public int shipGetSystem(int sys) {

	if (sys==1) { return systemWarpDrive; }
	if (sys==2) { return systemImpulseDrive; }
	if (sys==5) { return systemLongRangeScan; }
	if (sys==6) { return systemSensorScan; }

	return 100;

    }

    public void setPlayer(boolean a,IndicatorsListener il) {
	player = a;
	indicatorsListener=il;
    }

    public void setInfo(int a, double b,int c,int d) {

	INFOmax_warp=a;
	INFOmax_impulse=b;

	jump=(c==1)?(true):(false);
	turn=(d==1)?(true):(false);

	warpFactor = INFOmax_warp; // initial warp factor
	impulseFactor = INFOmax_impulse; // initial impulse speed

	warpSpeed = (int)( rescue.RunRescue.FAKE_SPEED_OF_LIGHT * Math.pow( warpFactor, (10.0 / 3.0) ) );
	impulseSpeed = (int)( rescue.RunRescue.FAKE_SPEED_OF_LIGHT * impulseFactor );

    }

    public int shipDamage(int hitlocation,int damage) {

	    if (hitlocation==1) { // phaser

		systemWarpDrive = systemWarpDrive - damage;
		if (systemWarpDrive < -10) { damage = damage - (systemWarpDrive+10); systemWarpDrive=-10; } // -- makes a +

	    }
	    else if (hitlocation==2) { // torpedo

		systemImpulseDrive = systemImpulseDrive - damage;
		if (systemImpulseDrive < -10) { damage = damage - (systemImpulseDrive+10); systemImpulseDrive=-10; }

	    }
	    else if (hitlocation==5) { // shields

		systemLongRangeScan = systemLongRangeScan - damage;
		if (systemLongRangeScan < -10) { damage = damage - (systemLongRangeScan+10); systemLongRangeScan=-10; }

	    }
	    else if (hitlocation==6) { // computer

		systemSensorScan = systemSensorScan - damage;
		if (systemSensorScan < -10) { damage = damage - (systemSensorScan+10); systemSensorScan=-10; }

	    }

	    return damage;

    }

    // used for working out if ship is alive
    public int shipGetLifeInfo() {

	return systemWarpDrive + systemImpulseDrive + systemLongRangeScan + systemSensorScan;

    }

    // ############################################################ paint methods

    public void drawBigShape(Graphics g, boolean highLight) {

	if (player) {

	    if (autoImpulse!=null) {
		g.setColor(Color.WHITE);
		g.drawLine(autoImpulse.x+2,autoImpulse.y+2,autoImpulse.x-2,autoImpulse.y-2);
		g.drawLine(autoImpulse.x+2,autoImpulse.y-2,autoImpulse.x-2,autoImpulse.y+2);
	    }

	}

	//g.setColor(Color.RED);
	//g.drawOval(x-5,y-5,10,10);
	//g.drawLine(x,y,x+(int)(30*Math.sin(direction)),y-(int)(30*Math.cos(direction)));


	// DRAW THE SHIP ITSELF
	if (alive) {

	    if (turn) {

		Graphics2D g2 = (Graphics2D)g.create();
		g2.rotate(direction,x,y);
		g2.drawImage(image,x-(image.getWidth()/2),y-(image.getHeight()/2),null);

	    }
	    else {

		g.drawImage(image,x-(image.getWidth()/2),y-(image.getHeight()/2),null);
	    }

	}

	super.drawBigShape(g,highLight);

    }

    public void drawSmallShape(Graphics g, double sx, double sy, int scan) {

	if (player) {

	    g.setColor(Color.DARK_GRAY);
	    g.drawOval((int)(x*sx)-4,(int)(y*sy)-4,8,8);

	    if (autoWarp!=null) {

		g.setColor(Color.WHITE);

		g.drawLine((int)(autoWarp.x*sx)+1,(int)(autoWarp.y*sy)+1,(int)(autoWarp.x*sx)-1,(int)(autoWarp.y*sy)-1);
		g.drawLine((int)(autoWarp.x*sx)+1,(int)(autoWarp.y*sy)-1,(int)(autoWarp.x*sx)-1,(int)(autoWarp.y*sy)+1);

	    }

	}

	super.drawSmallShape(g,sx,sy,scan);

    }

    // ########################################### user set methods

    public void stop() {
	stop=true;
    }

    public void setEvasive(int a) {
	if (warpOn==false) {
	    changeEvasiveAndFireIndicatorUpdate(a);
	    impulseOn=true;
	    stop=false;
	}
    }

    public int getEvasive() {
	return evasive;
    }

    public void useReserve() {

	int needed = INFOmax_totalenergy-totalEnergy;

	if (needed >= reserve) { totalEnergy = totalEnergy + reserve; reserve=0; }
	else { totalEnergy = totalEnergy + needed; reserve = reserve - needed; }

    }

    public void divertPhaser() {

	int needed = INFOmax_totalenergy-totalEnergy;

	if (needed >= phaserBanks) { totalEnergy = totalEnergy + phaserBanks; phaserBanks=0; }
	else { totalEnergy = totalEnergy + needed; phaserBanks = phaserBanks - needed; }
	

    }

    public void setFixPriority(int a) {

	fixPriority=a;

    }

    public void setScan(boolean a) {

	if (a && SensorScan<getMaxScan() ) {

	    SensorScan++;
	    return;

	}
	if (!a && SensorScan>0 ) {

	    SensorScan--;
	    return;

	}

    }
    public int getMaxScan() {
	return 3;
    }

    public void sendPeople(Ship a) {

	a.addPeople(people);
	people=0;

    }

    public void shipDockWith(MannedObject base) {

	totalEnergy = totalEnergy + base.getEnergy(INFOmax_totalenergy-totalEnergy);
	phaserBanks = phaserBanks + base.getPhaser(INFOmax_phaserbanks-phaserBanks);
	torpedosLeft = torpedosLeft + base.getTorpedos(INFOmax_torpedosleft-torpedosLeft);

	if (base instanceof Base) {

	    base.addPeople(people);
	    people=0;

	    shieldPower = INFOmax_shieldsPower;

	    int a=0;

	    while(checkFixNeeded() && a < 10) {
		fixSystems(); a++;
	    }
	}
    }

    public void setTarget(int a) {
	target=a;
    }

    public void changeShields() {
	ShieldsUp = !ShieldsUp;
    }

    public void setAutoImpulse(Point p) {
	if (warpOn==false) {
	    autoImpulse=p;
	    autoImpulseOn=true;
	    changeEvasiveAndFireIndicatorUpdate(0);
	}
    }
    public void setAutoWarp(Point p) {
	autoWarp=p;
	autoWarpOn=true;
	changeEvasiveAndFireIndicatorUpdate(0);
    }

    public void setManImpulse(double z) {
	manualImpulse = z;
	changeEvasiveAndFireIndicatorUpdate(0);
    }
    public void setManWarp(double z) {
	manualWarp = z;
	changeEvasiveAndFireIndicatorUpdate(0);
    }

    private void changeEvasiveAndFireIndicatorUpdate(int e) {
	evasive=e;
	if (indicatorsListener!=null) {
		indicatorsListener.updateEvasiveIndicators(evasive);
	}
    }

    public void setAutoImpulseOn(boolean a) {

	if (a && impulseOn && autoImpulse==null) {
	    return;
	}
	autoImpulseOn=a; // should be more here, speed should drop for turns and stuff
    }
    public void setAutoWarpOn(boolean a) {

	if (a && warpOn && autoWarp==null) {
	    return;
	}
	autoWarpOn=a; // should be more here
    }

    public void setImpulseFactor(int a) {
	if ( (a/4d) <= INFOmax_impulse) {
	    impulseFactor=a/4d;
	    impulseSpeed = (int)( rescue.RunRescue.FAKE_SPEED_OF_LIGHT * impulseFactor );
	}
    }
    public void setImpulseUpDown(boolean up) {

	if ((up && impulseFactor == INFOmax_impulse) || (!up && impulseFactor==0.25)) {
	    return;
	}
	else if (up) {
	    impulseFactor=impulseFactor+0.25;
	}
	else {
	    impulseFactor=impulseFactor-0.25;
	}

	impulseSpeed = (int)( rescue.RunRescue.FAKE_SPEED_OF_LIGHT * impulseFactor );

	//if (up && impulseSpeed<32) {
	//    impulseSpeed = impulseSpeed*2;
	//}
	//else if (!up && impulseSpeed>4) {
	//    impulseSpeed = impulseSpeed/2;
	//}

    }
    public double getMaxImpulseFactor() {
	return INFOmax_impulse;
    }

    public void setWarpFactor(int a) {
	if (a <= INFOmax_warp) {
	    warpFactor=a;
	    warpSpeed = (int)( rescue.RunRescue.FAKE_SPEED_OF_LIGHT * Math.pow( warpFactor, (10.0 / 3.0) ) );
	}
    }
    public void setWarpUpDown(boolean up) {


	if ((up && warpFactor == INFOmax_warp) || (!up && warpFactor==1)) {
	    return;
	}
	else if (up) {
	    warpFactor++;
	}
	else {
	    warpFactor--;
	}

	warpSpeed = (int)( rescue.RunRescue.FAKE_SPEED_OF_LIGHT * Math.pow( warpFactor, (10.0 / 3.0) ) );

	//if (warpSpeed==32) return "1";
	//if (warpSpeed==64) return "2";
	//if (warpSpeed==128) return "3";
	//if (warpSpeed==256) return "4";
	//if (warpSpeed==512) return "5";
	//if (warpSpeed==1024) return "6";
	//if (warpSpeed==2048) return "7";
	//if (warpSpeed==4096) return "8";
	//if (warpSpeed==8192) return "9";

	//if (up && warpSpeed<8192) {
	//    warpSpeed = warpSpeed*2;
	//}
	//else if (!up && warpSpeed>32) {
	//    warpSpeed = warpSpeed/2;
	//}
    }
    public int getMaxWarpFactor() {
	return INFOmax_warp;
    }

    public void setTorpedoSalvo(int a) {
	if (a <= INFOmax_torpedoSalvo) {
	    TorpedoSalvo=a;
	}
    }
    public void setTorpedoSalvoUpDown(boolean up) {
	if (up && TorpedoSalvo< INFOmax_torpedoSalvo ) {
	    TorpedoSalvo++;
	}
	else if (!up && TorpedoSalvo>1) {
	    TorpedoSalvo--;
	}
    }
    public int getMaxTorpedoSalvo() {
	return INFOmax_torpedoSalvo;
    }

    public void setPhaserIntensity(int a) {
	if (a <= INFOmax_phaserIntensity) {
	    phaserIntensity=a;
	}
    }
    public void setPhaserIntensityUpDown(boolean up) {
	if (up && phaserIntensity< INFOmax_phaserIntensity ) {
	    phaserIntensity++;
	}
	else if (!up && phaserIntensity>1) {
	    phaserIntensity--;
	}
    }
    public int getMaxPhaserIntensity() {
	return INFOmax_phaserIntensity;
    }

    public void setImpulseOn(boolean a) {
	if (a) {
	    if (warpOn==false && (!autoImpulseOn || autoImpulse!=null) ) { impulseOn=true; stop=false; rescue.JukeBox.play("background"); }
	}
	else {
	    if (impulseOn) { stop=true; }
	}
    }
    public void setWarpOn(boolean a) {
	if (a) {
	    if (!autoWarpOn || autoWarp!=null) { impulseOn=false; warpOn=true; stop=false; rescue.JukeBox.play("background"); }
	}
	else {
	    if (warpOn) { stop=true; }
	}
    }



    // ########################################### get methods

    public int getCondition() {

	if (getLifeInfo()!=0 || shieldPower!=INFOmax_shieldsPower) { return 3; } // RED!!


	for(int i=0,j=rescue.RunRescue.DISTANCE_TORPEDO*2,k=SpaceObjects.length-1; i< k ; i++) {

	    // YURA:TODO distance is already worked out for the ships list, it should not be worked out so many times
	    if ( SpaceObjects[i] instanceof MannedObject && ((MannedObject)SpaceObjects[i]).isAlive() && distance(SpaceObjects[i]) < j && SpaceObjectsLikes[i] <0) { return 2; } // Yellow

	}

	return 1; // green

    }

    // THESE 2 METHODS SHOULD ONLY BE USED TO DISPLAY INFO
    public int getManImpulse() {
	return (int)Math.round(Math.toDegrees(manualImpulse));
    }
    public int getManWarp() {
	return (int)Math.round(Math.toDegrees(manualWarp));
    }

    public int getFixPriority() {
	return fixPriority;
    }

    public String getScan() {
	if (systemSensorScan==0) {
	    if (SensorScan==0) return "OFF";
	    if (SensorScan==1) return "MIN";
	    if (SensorScan==2) return "MED";
	    if (SensorScan==3) return "MAX";
	    return SensorScan+"";
	}
	else {
	    return "XXX";
	}
    }

    public int getSensorScan() {
	if (systemSensorScan==0) {
	    return SensorScan;
	}
	else {
	    return 0;
	}
    }

    public Ship getTractoredShip() {
	return tractoredShip;
    }

    public int getNoColonies() {
	return people;
    }

    public int getTarget() {
	return target;
    }

    public boolean getShieldsUp() {
	return ShieldsUp;
    }

    public double getSpeedMofC() {
	return ((double)speed)/rescue.RunRescue.FAKE_SPEED_OF_LIGHT;
    }
    public double getDirection() {
	return direction;
    }

    public Point getAutoImpulse() {
	return autoImpulse;
    }
    public Point getAutoWarp() {
	return autoWarp;
    }

    public boolean getAutoImpulseOn() {
	return autoImpulseOn;
    }
    public boolean getAutoWarpOn() {
	return autoWarpOn;
    }

    public String getImpulseSpeed() {

	if (systemImpulseDrive==0) {

	    if (impulseFactor==0.25) return "1/4";
	    if (impulseFactor==0.5) return "1/2";
	    if (impulseFactor==0.75) return "3/4";
	    if (impulseFactor==1) return "MAX";

	    return impulseFactor+"";

	}
	else {
	    return "XXX";
	}
    }
    public String getWarpSpeed() {

	if (systemWarpDrive==0) {
	    return warpFactor+"";
	}
	else {
	    return "XXX";
	}
    }

    public int getWarpFactor() {
	return warpFactor;
    }

    public double getImpulseFactor() {
	return impulseFactor;
    }

    public boolean getImpulseOn() {
	return impulseOn;
    }
    public boolean getWarpOn() {
	return warpOn;
    }

    public String getTorpedoSalvo() {

	if (systemTorpedoTubes==0) {
	    return TorpedoSalvo+"";
	}
	else {
	    return "XXX";
	}
    }

    public String getPhaserIntensity() {

	if (systemPhaserBanks==0) {

	    if (phaserIntensity==1) { return "MIN"; }
	    if (phaserIntensity==2) { return "MED"; }
	    if (phaserIntensity==3) { return "MAX"; }
	    return phaserIntensity+"";

	}
	else {
	    return "XXX";
	}

    }

    public int getPhaserIntensityInt() {
	return phaserIntensity;
    }

    public int getTorpedoSalvoInt() {
	return TorpedoSalvo;
    }

/*
    public int getSystemWarpDrive() {
	return systemWarpDrive;
    }
    public int getSystemImpulseDrive() {
	return systemImpulseDrive;
    }
    public int getSystemLongRangeScan() {
	return systemLongRangeScan;
    }
    public int getSystemSensorScan() {
	return systemSensorScan;
    }
*/


    // ########################################################## ai advance

    // this is the energy that the ship is using to escape

    // nrg - the power im putting into it
    // return - how much power of what im putting in is being used
    public int tractorEnergyUsed(int nrg) {

	// find out if you hate your owner
	if ( SpaceObjectsLikes[ owner.getNumber() ] > 0 || systemImpulseDrive!=0 ) {
	    return 10;
	}
	else {

	    int a = (int)((nrg-10)*( distance(owner) / rescue.RunRescue.DISTANCE_TRACTOR ));

	    if (totalEnergy < a) { return 10; }

	    totalEnergy = totalEnergy - a;

	    a = a +10;

	    return a;
	}

    }

    private int shootcounter=0;
    public void advance() {

	// sort out all the power stuff

      if ( alive && !player ) {
	// just give me someone to hate

	MannedObject thehated=null;
	int hate=0;
	int dis=1000000000;
	int tmp=0;

	for(int i=0; i< SpaceObjectsLikes.length ; i++) {

	    // if ship is alive and ( i hate it most || ( hate is the same but its closer ) )

	    tmp = SpaceObjectsLikes[i];
	    if ( SpaceObjects[i] instanceof MannedObject && ((MannedObject)SpaceObjects[i]).isAlive() && ((tmp < hate) || ( (tmp < 0) && (tmp <= hate) && (int)distance( SpaceObjects[i] ) < dis ) ) ) {

		hate = SpaceObjectsLikes[i];
		thehated = (MannedObject)SpaceObjects[i];
		dis = (int)distance( thehated );

	    }

	}





	if (thehated!=null) {

	  //System.out.print("I am "+name+" and I hate: "+thehated.getName()+" ("+hate+")\n");

	  if ( dis > 1000 ) {

	    if (impulseOn) { impulseOn=false; }

	    autoWarp=thehated;
	    warpOn=true;
	    stop=false;

	  }
	  else if ( dis > 500 ) { // this turns the ship around

	    if (warpOn) { stop=true; }
	    else {
		autoImpulse=thehated;
		autoImpulseOn = true;
		impulseOn=true;
		stop=false;
	    }

	  }
	  else { //if (!stop) {

		if ( autoImpulseOn ) { // this puts the ship in a non-direct course to the enemy
		    manualImpulse = this.getAngle(thehated) + Math.toRadians(10);

		    if (manualImpulse > Math.toRadians(360) ) { manualImpulse = manualImpulse - Math.toRadians(360); }

		    autoImpulseOn = false;
		    impulseOn=true;
		    stop=false;
		}

		// YURA:TODO not good to use dis here as this is the distance to the center (not to the object)

		if (shootcounter == 0 && dis <= rescue.RunRescue.DISTANCE_TORPEDO && dis > rescue.RunRescue.DISTANCE_PHASERS) {
		    FireTorpedo(thehated);
		    shootcounter++;
		}
		else if (shootcounter == 0 && dis <= rescue.RunRescue.DISTANCE_PHASERS) {
		    FirePhaser(thehated);
		    shootcounter++;
		}

		if (shootcounter !=0) {
		    shootcounter++;
		}
		if (shootcounter == 100) {
		    shootcounter=0;
		}


	  }

	}
	else { // if there is no-one to go after

	    stop=true;
	    //if (warpOn) { warpOn=false; }
	    //if (impulseOn) { impulseOn=false; }

	}

      } // is alive
      else if (!alive && impulseOn && autoImpulseOn) { // if dead stop following and just go off in a direction

	    manualImpulse=direction;
	    autoImpulseOn=false;
      }


	// anything specific
	advanceShipLocation();


      super.advance();

    }

}
