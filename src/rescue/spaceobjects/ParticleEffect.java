package rescue.spaceobjects;

import java.awt.geom.Point2D;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;

public class ParticleEffect extends SpaceObject {

    private Color c = null;
    private boolean heavy = false;
//    private double x;
//    private double y;
    private double xdirection;
    private double ydirection;
    private boolean infinite = false;
    private int age = 0;
    private int number = 0;
    private int created = 0;
    private int finished = 0;
    private int release = 1;
    private double spread = 0;
    private Point2D attach;
    private Particle[] particles = null;
    private int attach_xoff, attach_yoff;

/*
    public static ParticleEffect getSmoke(Color c)
        {
        return new ParticleEffect(20,c,false,0,0,0,1,0,10,true,1);
        }

    public static ParticleEffect getBlood(Color c, double x, double y, double xdir, double ydir)
        {
        return new ParticleEffect(50,c,true,x,y,xdir,2,ydir,30,false,5);            
        }
    
    public static ParticleEffect getExplosion(Color c, double x, double y)
        {
        return new ParticleEffect(20,c,true,x,y,0,3,0,30,false,5);
        }
    
    public static ParticleEffect getBlast(double x, double y, double xdirection)
        {
        return new ParticleEffect(20,Color.gray,true,x,y,xdirection,3,1,30,false,5);
        }
*/

    public void attach(Point2D a, int attach_xoff, int attach_yoff)
        {
        this.attach = a;
        this.attach_xoff = attach_xoff;
        this.attach_yoff = attach_yoff;
        }
        
    public ParticleEffect(int ix, int iy, ImageObject[] so, Vector t, 

	int number, Color c, double xdirection, double spread, double ydirection, int age, boolean infinite, int release)
        {

	super(ix,iy,so,t);

        this.number = number;
        this.release = release;
        //this.x = x;
        this.spread = spread;
        //this.y = y;
        this.heavy = false;
        this.c = c;
        this.xdirection = xdirection;
        this.ydirection = ydirection;
        this.age = age;
        this.infinite = infinite;
        this.particles = new Particle[number];
        }

    public boolean finished()
        {
        if (infinite) return false;
        if (finished == number) return true;
        return false;
        }

    public Particle makeParticle()
        {
        double xdr = Math.random()*xdirection + Math.random()*spread*2 - spread;
        double ydr = Math.random()*ydirection + Math.random()*spread*2 - spread;
        double ax = x;
        double ay = y;
        if (attach != null) // attach new particles to the moving ship
            {
            ax = attach.getX() + attach_xoff;
            ay = attach.getY() + attach_yoff;
            }

        return new Particle(ax,ay,xdr,ydr);            
        }

    /**
     * Make some particles, max created is release, set in constructor
     * keeps making particles until created == number, i.e. max made
     */
    public void makeParticles()
        {
        if (created < number)
            {
            int made = 0;
            for (; created < number && made < release; created++)
                {
                made++;
                particles[created] = makeParticle();
                }
            }            
        }
        







    public void drawBigShape(Graphics g, boolean highLight) {

        g.setColor(c);

        for (int i = 0; i < particles.length; i++) {

            if (particles[i] == null) {
                continue;
	    }
            if (particles[i].getAge() < age) {
                particles[i].draw(g);

            }

        }
    }




    public void advance() {

        makeParticles();
        
        for (int i = 0; i < particles.length; i++)
            {
            if (particles[i] == null)
                continue;
            if (particles[i].getAge() < age)
                {
                particles[i].move();
                }
            else if (infinite)
                {
                particles[i] = makeParticle();
                }
            else
                {
                finished++;
                particles[i] = null;
                }
            }

	if (finished()) {

	    torpedos.remove(this);

	}

    }


    public class Particle
        {
        private double x;
        private double y;
        private double xdirection;
        private double ydirection;
        private int age = 0;
        
        public Particle(double x, double y, double xdirection, double ydirection)
            {
            this.x = x;
            this.y = y;
            this.xdirection = xdirection;
            this.ydirection = ydirection;
            
            }
        
        public void move()
            {
            x += xdirection;
            y += ydirection;

            //xdirection *= 1.05; // increase y direction speed
            //ydirection *= 1.05; // increase y direction speed

            //if (heavy)
            //    ydirection += 0.1;
            //else
            //    ydirection -= 0.05;
            age++;
            }
        
        public int getAge()
            {
            return age;
            }
            
        public void draw(Graphics g)
            {
            g.drawRect((int)x,(int)y,1,1);
            }
        }
    }
