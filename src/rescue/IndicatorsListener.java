package rescue;

// this will let the app listen to events happening on the ship, to show idicators of them
public interface IndicatorsListener {

	void updateEvasiveIndicators(int value);

}
