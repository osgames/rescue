package rescue;

import rescue.panels.*;
import rescue.spaceobjects.*;

import rescue.qdxml.DocHandler;
import rescue.qdxml.QDParser;

import java.util.Hashtable;
import java.util.Enumeration;

import java.io.FileReader;
import java.io.File;
import java.util.Vector;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.net.URL;
import java.io.InputStreamReader;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;

/**
  This class is the most basic possible
  implementation of the DocHandler class.
  It simply reports all events to System.out
  as they occur.
*/
public class MissionLoader implements DocHandler {

    private Vector MissionObjects;
    private Vector MissionWindows;
    //private MissionObject player;
    private Component bigmap;
    private String missionName;
    private RunRescue rescue;
    private Vector containers;
    private Vector panels;

    private MissionObject cmo;
    private ContainerPanel cmf;
    private InterfacePanel cip;
    private JToolBar ctb;
    private String XMLFile;
    private URL path;

    private String previewURL;
    private Icon   previewIcon;

    private boolean cluster;
    private boolean external;

    private ShipsPanel shiplist;
    private ShipsPanel baselist;

    private ShipInfoPanel shipinfo;

    private JukeBox jukebox;

    private boolean loadSO;

    public MissionLoader(RunRescue r,URL base,String file) throws Exception {

	XMLFile = file;
	rescue = r;
	path = base;

	MissionObjects = new Vector();

	readXML(true);

    }

    private void readXML(boolean l) throws Exception {

	loadSO = l;

	InputStreamReader isr = new InputStreamReader(RescueIO.getInputStream(new URL(path,XMLFile)));
	QDParser.parse(this,isr);
	isr.close();


    }

    public String getXMLFileName() {

	return XMLFile;
    }

    public Icon getIcon() {

	if (previewURL!=null && previewIcon==null) {

		try {
			previewIcon = new ImageIcon( ImageIO.read( RescueIO.getInputStream(new URL(path,previewURL) ) ) );
		}
		catch(Exception ex) {
			//throw new RuntimeException(ex);
			previewURL = null;
			return null; // no icon
		}

	}
	return previewIcon;

    }

    public void loadImagesForSetup() {

	for(int i=0; i< MissionObjects.size() ; i++) {

		try {

			((MissionObject)MissionObjects.get(i)).loadImagesForSetup();

		}
		catch(Exception ex) {

			ex.printStackTrace();

		}
	}

    }
    public void loadImagesForGame() {

	if (MissionWindows==null) {

		// we need the most basic images
		// just in case we dont have them
		loadImagesForSetup();

		try {

			jukebox = new SunAudioJukeBox();
		}
		catch(Exception ex) {

			System.err.println("unable to create jukebox!");
			ex.printStackTrace();

			// create a null jukebox if we cant create a real one
			jukebox = new JukeBox() {

				public void setUpSound(String a, URL b) { }

				public void playSound(String a) { }

				public void stopSound(String a) { }

			};

		}

		MissionWindows = new Vector();
		containers = new Vector();
		panels = new Vector();

		for(int i=0; i< MissionObjects.size() ; i++) {

			try {

				((MissionObject)MissionObjects.get(i)).loadImagesForGame();

			}
			catch(Exception ex) {

				ex.printStackTrace();

			}
		}

		try {

			// load gui images at this point!
			readXML(false);
		}
		catch(Exception ex) {

			ex.printStackTrace();

		}
	}

    }

    public String toString() {

	return missionName;
    }

    public ImageObject[] getSpaceObjects(Vector t) throws Exception {

	int a=0;
	for(int i=0; i< MissionObjects.size(); i++) {

	    a = a + ((MissionObject)MissionObjects.elementAt(i)).getNumber();

	}


	//Vector v = new Vector();
	ImageObject[] v = new ImageObject[a];

	a=0;
	for(int i=0; i< MissionObjects.size(); i++) {

	    a = ((MissionObject)MissionObjects.elementAt(i)).makeSpaceObjects(v,cluster,a,t);

	}

	// go though all the MissionObjects and set the Likes for all the Objects in that MissionObject
	for(int i=0; i< MissionObjects.size(); i++) {

	    ((MissionObject)MissionObjects.elementAt(i)).setLikes(v);

	}

	// since a new game is being started make sure that the current jukebox is from this mission
	jukebox.load();

	return v;

    }

    public boolean getExternal() {
	return external;
    }

    public void setExternal(boolean a) {
	external=a;
    }

    public String getMissionName() {
	return missionName;
    }

    public MissionObject getPlayerObject() { // this just gets the last object used (should be the player)
	return cmo;
    }
    public Vector getMissionObjects() {
	return MissionObjects;
    }
    public Component getBigMap() {
	return bigmap;
    }
    public Vector getPanels() {
	return panels;
    }

    public Vector getWindows() {
	return MissionWindows;
    }


    public ShipsPanel getShipList() {
	return shiplist;
    }
    public ShipsPanel getBaseList() {
	return baselist;
    }


    public void startElement(String elem,Hashtable h) throws Exception {
        //System.out.println("        start elem: "+elem);

      if (loadSO) {

	// #############################################################################################################
	// ##################################################   S O   ##################################################
	// #############################################################################################################

	if ( elem.equals("mission") ) { // mode==4
	    missionName=(String)h.get("name");
	    previewURL = (String)h.get("preview");
	}
	else if ( elem.equals("object") ) { // mode==1

	    cmo = new MissionObject( (String)h.get("name") ,new URL(path,(String)h.get("image")) ,new URL(path,(String)h.get("small")),( h.get("info")!=null )?(new URL(path,(String)h.get("info")) ):(null),(String)h.get("number"), (String)h.get("type") );
	    MissionObjects.add( cmo );
	    //if (vals[3].equals("player")) { player=cmo; }
	}
	else if ( elem.equals("like") ) { // mode==2
	    cmo.addLike( (String)h.get("who") , (String)h.get("amount") );
	}
	else if ( elem.equals("name") ) { // mode==3
	    cmo.addName( (String)h.get("value") , (( h.get("image")!=null )?( new URL(path,(String)h.get("image")) ):( null )) );
	}
	else if ( elem.equals("options") ) { // mode==5
	    cmo.setMinMax( (String)h.get("min") , (String)h.get("max") );
	}
	else if ( elem.equals("preset") ) { // mode==6
	    cmo.setPreset( (String)h.get("name") , (String)h.get("value") );
	}


	else if ( elem.equals("maxtotalenergy") ) { // mode==7
	    cmo.setInfoMaxTotalEnergy( (String)h.get("value") );
	}
	else if ( elem.equals("maxphaserbanks") ) { // mode==8
	    cmo.setInfoMaxPhaserBanks( (String)h.get("value") );
	}
	else if ( elem.equals("maxtorpedosleft") ) { // mode==9
	    cmo.setInfoMaxTorpedosLeft( (String)h.get("value") );
	}
	else if ( elem.equals("maxshieldpower") ) { // mode==10
	    cmo.setInfoMaxShieldPower( (String)h.get("value") );
	}

	else if ( elem.equals("maxwarp") ) { // mode==11
	    cmo.setInfoWarp( (String)h.get("value") );
	}
	else if ( elem.equals("maximpulse") ) { // mode==12
	    cmo.setInfoImpulse( (String)h.get("value") );
	}
	else if ( elem.equals("jump") ) { // mode==13
	    cmo.setInfoJump( (String)h.get("value") );
	}
	else if ( elem.equals("turn") ) { // mode==14
	    cmo.setInfoTurn( (String)h.get("value") );
	}
	else if ( elem.equals("maxphaserintensity") ) { // mode==15
	    cmo.setInfoPhaser( (String)h.get("value") );
	}
	else if ( elem.equals("maxtorpedosalvo") ) { // mode==16
	    cmo.setInfoTorpedos( (String)h.get("value") );
	}
	else if ( elem.equals("maxtractorpower") ) { // mode==17
	    cmo.setInfoTractor( (String)h.get("value") );
	}
	else if ( elem.equals("maxcloakpower") ) { // mode==18
	    cmo.setInfoCloak( (String)h.get("value") );
	}
	else if ( elem.equals("spaceObjects") ) { // mode==19

	    if ("yes".equals((String)h.get("cluster"))) { cluster=true; }
	    else if ("no".equals((String)h.get("cluster"))) { cluster=false; }
	    else { throw new Exception("unexpected value for cluster: "+(String)h.get("cluster") ); }

	}

	// #############################################################################################################
	// ##################################################   GUI   ##################################################
	// #############################################################################################################

      }
      else {

	if ( elem.equals("container") ) { // mode==101

	    if (cmf!=null) { containers.add(cmf); }

	    if (containers.isEmpty()) {
		cmf = new ContainerPanel((String)h.get("name"),(String)h.get("x"),(String)h.get("y"));
		MissionWindows.add( cmf );
	    }
	    else {
		cmf = new ContainerPanel();
		((ContainerPanel)containers.lastElement()).add(cmf,(String)h.get("constraints"));
	    }
	}
	else if ( elem.equals("toolbar") ) { // mode==102
	    ctb = new JToolBar();
	    ctb.setRollover(true);
	    cmf.add(ctb, (String)h.get("constraints"));
	}


	else if ( elem.equals("bigmap") ) { // mode==103
	    cmf.add( bigmap = new BigMapPanel(rescue,Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h"))) , (String)h.get("constraints"));
	}
	else if ( elem.equals("smallmap") ) { // mode==106
	    SmallMapPanel tmp = new SmallMapPanel(rescue,Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h")));
	    panels.add(tmp);
	    cmf.add( tmp , (String)h.get("constraints"));
	}
	else if ( elem.equals("panel") ) { // mode==109

	    if ( h.get("nob") == null ) { cip = new InterfacePanel(rescue,Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h"))); }
	    else {

		cip = new InterfacePanel(rescue);

	        cip.setupImages(new URL(path,(String)h.get("on")),new URL(path,(String)h.get("off")),new URL(path,(String)h.get("up")),new URL(path,(String)h.get("down")),new URL(path,(String)h.get("map")),Integer.parseInt((String)h.get("nob")));

	    }
	    //else { throw new Exception("abnormal number of parameters for panel creation.\nonly 3 or 7 is allowed"); }

	    panels.add(cip);
	    cmf.add( cip , (String)h.get("constraints"));
	}
	else if ( elem.equals("shiplist") ) { // mode==108
	    shiplist = new ShipsPanel(rescue,Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h")),1);
	    panels.add(shiplist);
	    cmf.add( shiplist , (String)h.get("constraints"));
	}
	else if ( elem.equals("baselist") ) { // mode==114
	    baselist = new ShipsPanel(rescue,Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h")),2);
	    panels.add(baselist);
	    cmf.add( baselist , (String)h.get("constraints"));
	}

	else if ( elem.equals("button") ) { // mode==104
	    JButton button = new JButton((String)h.get("name"));
	    button.setActionCommand((String)h.get("action"));
	    if (ctb!=null) { ctb.add( button ); }
	}
	else if ( elem.equals("separator") ) { // mode==105
	    if (ctb!=null) { ctb.addSeparator(); }
	}
	else if ( elem.equals("text") ) { // mode==107

	    JTextArea text = new JTextArea("Welcome to Rescue! Max");
	    text.setBorder(null);
	    text.setBackground( Color.BLACK );
	    text.setForeground( Color.WHITE );
	    text.setEditable(false);
	    JScrollPane textScroll = new JScrollPane(text);
	    textScroll.setBorder(null);

	    Dimension size = new Dimension(Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h")));

	    textScroll.setPreferredSize(size);
	    textScroll.setMinimumSize(size);
	    textScroll.setMaximumSize(size);

	    cmf.add( textScroll , (String)h.get("constraints"));

	}
	else if ( elem.equals("image") ) { // mode==110

	    cip.setupImages(new URL(path,(String)h.get("on")),new URL(path,(String)h.get("off")),new URL(path,(String)h.get("up")),new URL(path,(String)h.get("down")),new URL(path,(String)h.get("map")),Integer.parseInt((String)h.get("nob")));

	}
	else if ( elem.equals("color") ) { // mode==111
	    cip.setCommandValue((String)h.get("action"),Integer.parseInt((String)h.get("value")), (( h.get("tooltip")!=null )?((String)h.get("tooltip")):(null)) );
	}
	else if ( elem.equals("infotext") ) { // mode==112
	    cip.setInfoText((String)h.get("type"),Integer.parseInt((String)h.get("x")),Integer.parseInt((String)h.get("y")),Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h")),Integer.parseInt((String)h.get("lx")),Integer.parseInt((String)h.get("ly")),( ( h.get("color")!=null )?(getColor((String)h.get("color"))):(Color.BLACK) ) );
	}
	else if ( elem.equals("infobar") ) { // mode==113
	    cip.setInfoBar((String)h.get("type"),Integer.parseInt((String)h.get("x")),Integer.parseInt((String)h.get("y")),Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h")),(String)h.get("direction") );
	}
	else if ( elem.equals("shipinfo") ) { // mode==115
	    shipinfo = new ShipInfoPanel(Integer.parseInt((String)h.get("w")),Integer.parseInt((String)h.get("h")),rescue);
	    panels.add(shipinfo);
	    cmf.add( shipinfo , (String)h.get("constraints"));
	}
	else if ( elem.equals("guitheme") ) { // mode==116

	    if (((String)h.get("external")).equals("yes")) { external=true; }
	    else if (((String)h.get("external")).equals("no")) { external=false; }
	    else { throw new Exception("unexpected value for external: "+(String)h.get("external")); }

	}
	else if ( elem.equals("sound") ) { // mode==117

	    // dont want to be using old files any more as of 1.0.0.2 coz of new features
	    //if (vals.length == 0) { return; } // this is here for compatibility with the old mission files

	    jukebox.setUpSound( (String)h.get("name"), new URL(path,(String)h.get("file")) );
	}

	else if (elem.equals("names") || elem.equals("info") || elem.equals("likes") || elem.equals("sounds")) {
	    return;
	}

      }

	//else {
	//    throw new Exception("unknown tag in xml file: "+elem);
	//}


    }



    public void endElement(String elem) {
        //System.out.println("        end elem: "+elem);

	//if (elem.equals("object")) {
	    //cmo=null; // SHOULD not be here as then the last object is not remembered
	//}

      if (!loadSO) {

	if (elem.equals("container")) {
	    cmf=null;
	    if (!containers.isEmpty()) { cmf = (ContainerPanel)containers.lastElement(); containers.remove(cmf); }
	}
	else if (elem.equals("panel")) {
	    cip=null;
	}
	else if (elem.equals("toolbar")) {
	    ctb=null;
	}
      }
    }



    public void text(String text) {
        //System.out.println("                text: "+text);
    }

    // Implementation of DocHandler is below this line
    public void startDocument() {
        //System.out.println("    start document");
    }
    public void endDocument() {
        //System.out.println("    end document");
    }

    public boolean getCluster() {
	return cluster;
    }

    public void setCluster(boolean a) {
	cluster=a;
    }

    public ShipInfoPanel getShipInfoPanel() {
	return shipinfo;
    }

    public static Color getColor(String s) throws Exception {

	if (s.equals("black"))      { return Color.BLACK; }
	if (s.equals("blue"))       { return Color.BLUE; }
	if (s.equals("cyan"))       { return Color.CYAN; }
	if (s.equals("darkgray"))   { return Color.DARK_GRAY; }
	if (s.equals("gray"))       { return Color.GRAY; }
	if (s.equals("green"))      { return Color.GREEN; }
	if (s.equals("lightgray"))  { return Color.LIGHT_GRAY; }
	if (s.equals("magenta"))    { return Color.MAGENTA; }
	if (s.equals("orange"))     { return Color.ORANGE; }
	if (s.equals("pink"))       { return Color.PINK; }
	if (s.equals("red"))        { return Color.RED; }
	if (s.equals("white"))      { return Color.WHITE; }
	if (s.equals("yellow"))     { return Color.YELLOW; }

	throw new Exception("unknown color "+s);
    }

}
