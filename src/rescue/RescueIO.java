package rescue;

import java.io.InputStream;
import java.io.IOException;
import java.net.URL;

public abstract class RescueIO {

	public abstract InputStream openStream(URL name) throws IOException;

	private static RescueIO rescueIO = new RescueIO() {

		public InputStream openStream(URL url) throws IOException {

			return url.openStream();

		}

	};

	public static InputStream getInputStream(URL name) throws IOException {

		return rescueIO.openStream(name);

	}

	public static void setRescueIO(RescueIO a) {

		rescueIO = a;

	}

}
