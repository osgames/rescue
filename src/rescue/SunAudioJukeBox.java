package rescue;

import java.util.Hashtable;
import java.net.URL;

public class SunAudioJukeBox extends JukeBox {

    public void setUpSound(String a, URL b) { // ,String type

      try {

	Object object;

	sun.audio.AudioStream z = new sun.audio.AudioStream(RescueIO.getInputStream(b));

	if ( a.equals("music") || a.equals("background") ) { // looped sound

	    object = new sun.audio.ContinuousAudioDataStream( z.getData() );

	}
	else {

	    object = new sun.audio.AudioDataStream( z.getData() );

	}

	music.put(a,object);

      }
      catch (Exception ex) {
	System.err.println("error loading music ("+a+") file: "+b);
	ex.printStackTrace();
      }

    }

    public void playSound(String a) {

	Object object = music.get(a);

	if (object!=null && object instanceof sun.audio.AudioDataStream) {

		sun.audio.AudioDataStream b = (sun.audio.AudioDataStream)object;

		sun.audio.AudioPlayer.player.stop(b);
		b.reset();
		sun.audio.AudioPlayer.player.start(b);

	}
	else if (object!=null && object instanceof sun.audio.ContinuousAudioDataStream) {

		sun.audio.ContinuousAudioDataStream c = (sun.audio.ContinuousAudioDataStream)object;

		sun.audio.AudioPlayer.player.start(c);
	}

    }

    public void stopSound(String a) {

	Object object = music.get(a);

	if (object!=null && object instanceof sun.audio.ContinuousAudioDataStream) {

		sun.audio.ContinuousAudioDataStream c = (sun.audio.ContinuousAudioDataStream)object;

		sun.audio.AudioPlayer.player.stop( c );

	}

    }

}
