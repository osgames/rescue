package rescue;

import java.util.Hashtable;
import java.net.URL;

public abstract class JukeBox { // extends Thread

    public static JukeBox me;

    public static JukeBox getInstance() {
	return me;
    }

    public static void play(String a) { // this is used for all sounds
	getInstance().playSound(a);
    }

    public static void stop(String a) { // this is used for sounds that are looped
	getInstance().stopSound(a);
    }





    //################################ NON STATIC STUFF

    protected Hashtable music;

    public JukeBox() {

	music = new Hashtable();

    }

    public void load() {
	me=this;

    }

    public abstract void setUpSound(String a, URL b);

    public abstract void playSound(String a);

    public abstract void stopSound(String a);

}
