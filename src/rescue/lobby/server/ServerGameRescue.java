package rescue.lobby.server;

import org.lobby.server.ServerGame;
import org.lobby.server.ServerGameListener;

import java.util.Iterator;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

public class ServerGameRescue implements ServerGame {

	private ServerGameListener listoner;
	private String[] myplayers;
	private Vector spectators;

	public ServerGameRescue() {


	}

	public void startGame(String startGameOptions, String[] players) {

		myplayers = players;

	}

	public void messageFromUser(String username, String message) {

	}

	public String clientJoined(String username) {

		spectators.add(username);

		return null;

	}

	public void clientLeaves(String username) {

		spectators.remove(username);

	}


	public void objectFromUser(String username, Object message) { } // do nothing


	//###############################################################################
	// shared stuff
	//###############################################################################


	public void addServerGameListener(ServerGameListener l) {

		listoner = l;

	}

	public void removeServerGameListener(ServerGameListener l) {

		if( listoner.equals(l)) {

			listoner = null;
		}
	}

	public void midgameLogin(String from,String to) {


	}

}
