package rescue.lobby.client;

import org.lobby.client.*;

import rescue.gui.MissionSetup;
import rescue.MissionLoader;
import rescue.RescueIO;
import rescue.RunRescue;

import java.io.IOException;
import java.io.InputStream;

import java.awt.Frame;
import java.awt.Dimension;
import java.util.Map;
import java.util.HashMap;
import javax.swing.JDialog;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.applet.Applet;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.awt.FlowLayout;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Component;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import java.util.Hashtable;

public class ClientGameRescue implements LobbyGame {

	private LobbyGameChatListener lgml;
	private String myid;
	private ChatBox chatbox;
	private PlayerList playerlist;

	private String serverOptions;
	private MissionSetup setup;
	private Hashtable missionLoaders;
	private JSpinner humans;
	private JTextField nameTextBox;

	public ClientGameRescue() {

		RescueIO.setRescueIO(new RescueIO() {

			public InputStream openStream(URL url) throws IOException {

				return LobbyClientGUI.openStream(url);

			}

		});

	}

	public void setApplet(Applet a) {


	}

	public GameSetup newGameDialog(Frame parent, final String so,String myname) {

		try {
			if (setup==null && !so.equals(serverOptions)) {

				serverOptions = so;

				setup = new MissionSetup(parent,true);


				JPanel sidePanel = new JPanel();
				sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));

				//###################################################################### TOP


				JPanel topPanel = setup.makeDropDownTopPanel();

				sidePanel.add(topPanel);

				//pack();

				// after the GUI is setup, create a thread to add the missions to the box

				//###################################################################### TOP end

				JPanel peoplePanel = new JPanel();

				peoplePanel.add(new JLabel("Number of Human Players:"));

				humans = new JSpinner( new SpinnerNumberModel(2,1,10,1) );
				peoplePanel.add(humans);

				sidePanel.add(Box.createRigidArea(new Dimension(0,5)));
				sidePanel.add(peoplePanel);

				JPanel namePanel = new JPanel();

				namePanel.add(new JLabel("Game name:"));

				nameTextBox = new JTextField(myname+"'s "+RunRescue.NAME,10);
				namePanel.add(nameTextBox);


				sidePanel.add(Box.createRigidArea(new Dimension(0,5)));
				sidePanel.add(namePanel);

				setup.getContentPane().add(sidePanel,BorderLayout.WEST);


				missionLoaders = new Hashtable();

				setup.loadMissions( new URL(LobbyClientGUI.getCodeBase(),"Rescue/missions/"), serverOptions);

			}

	    		setup.setVisible(true);

			// when user clicks ok or cancel

			String newGameOptions = setup.getNewGameOptions();

			if (newGameOptions!=null) {

				return new GameSetup(nameTextBox.getText(),newGameOptions, ((Integer)humans.getValue()).intValue() );

			}

			return null;

		}
		catch(Exception ex) {
			throw new RuntimeException(ex);
		}

	}

	public ImageIcon getIcon(String options) {

		// return (MissionLoader)missionLoaders.get()

		// YURA:TODO go and get the correct icon from the MissionSetup dialog
		return null;

	}


	public void joinGame(String gameid, String name, String optionString, ChatBox cb,PlayerList pl) {

		chatbox = cb;
		playerlist = pl;

	}

	public void gameMessage(String message) {


	}

	public void gameObject(Object object) {


	}

	//#########################################################################
	// shared, will be taken out later
	//#########################################################################

	public PlayerList getPlayerList() {

		return playerlist;

	}

	public ChatBox getChatBox() {

		return chatbox;

	}


	public void addLobbyGameMoveListener(LobbyGameChatListener lgl) {

		lgml = lgl;

	}

	public void removeLobbyGameMoveListener(LobbyGameChatListener lgl) {

		if (lgml==lgl) {

			lgml = null;

		}
	}

	public LobbyGameChatListener getLobbyGameMoveListener() {

		return lgml;

	}


	public void renamePlayer(String from,String to) {


	}
}
