package rescue;

import rescue.spaceobjects.*;
import rescue.panels.ShipsPanel;
import rescue.panels.ShipInfoPanel;
import rescue.gui.Keymap;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.util.Vector;
import java.util.Arrays;
import java.io.IOException;
import java.net.URL;
import java.io.File;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.KeyStroke;

public class RunRescue implements Runnable,IndicatorsListener { // extends Thread

    public static final String DEFAULT_MISSION = "rescuemax.xml";
    public static final String VERSION = "1.0.0.3";
    public static final String NAME="Rescue! Max";
    public static final String KEYMAP_FILENAME = "keys.ini";

    public static final int WIDTH=1000000;
    public static final int HEIGHT=1000000;
    public static int DELAY = 20; // Milliseconds between game refreshes

    public static final int FAKE_SPEED_OF_LIGHT=10;

    public static final int DISTANCE_TRACTOR=250;
    public static final int DISTANCE_PHASERS=250;
    public static final int DISTANCE_TORPEDO=500;

    public static final int SELECT_ALL=0;
    public static final int SELECT_ENEMIES=1;
    public static final int SELECT_MANUAL=2;

    public ImageObject[] SpaceObjects;
    public Vector torpedos;


    private RescueListener gui;

    private boolean pause;
    private boolean heads;

    private Ship playerShip;
    private Vector selectedObjects;
    private int select;

    private Point2D.Double[] sky; // YURA: can not be int points as when u r zoomed out they do nothing!

    private double zoom=1;
    private int windowWidth=1000;
    private int windowHeight=1000;

    private int panelWidth;
    private int panelHeight;

    private boolean loadgameonstartup;
    private MissionLoader currentMission;

    private double fps;
    private long oldtime;

    private double timetotal;
    private int counter;

    private ShipsPanel shiplist;
    private ShipsPanel baselist;

    private ShipInfoPanel shipinfo;

    private URL rooturl;

    private Keymap keymap;

    public RunRescue(URL rurl) throws Exception {

	rooturl = rurl;

	loadgameonstartup=false;

	pause = true; // THIS MUST BE TRUE ALWAYS HERE
	heads = true;

	List actions = makeActions();

	keymap = new Keymap(actions);

	try {
		Keymap.loadKeys(actions, RescueIO.getInputStream( new URL(rooturl,KEYMAP_FILENAME) ) );
	}
	catch(Exception ex) {
		System.out.println("unable to load keys: "+ex);
		//ex.printStackTrace();
	}
	// set the default mission
	setMission( new MissionLoader(this,rooturl,DEFAULT_MISSION) );

	fps = (1000d/DELAY);
	oldtime=System.currentTimeMillis();

	timetotal=0;
	counter=0;



    }


	private final Vector selectListeners = new Vector();
	class SelectAction extends AbstractAction {
		private int type;
		public SelectAction(int type,String name,String label) {
			super(label);
			this.type = type;
			putValue(Action.ACTION_COMMAND_KEY,name);
			selectListeners.add(this);
		}
		public void newSelectState(int state) {
			setEnabled(state!=type);
		}
		public void actionPerformed(ActionEvent e) {
			setSelect(type);
			updateGUI();
		}
	}

	private final Vector evasiveListeners = new Vector();
	class EvasiveAction extends AbstractAction {
		private int type;
		public EvasiveAction(int type,String name,String label) {
			super(label);
			this.type = type;
			putValue(Action.ACTION_COMMAND_KEY,name);
			evasiveListeners.add(this);
		}
		public void newEvasiveState(int state) {
			setEnabled(state!=type);
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setEvasive(type);
			updateGUI();
		}
	}

    private List makeActions() {

	Vector actions = new Vector();

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"impulse_auto");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_A , KeyEvent.SHIFT_MASK ) );
		}
		public boolean isEnabled() {
			return !playerShip.getAutoImpulseOn();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setAutoImpulseOn(true);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"impulse_man");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_M , KeyEvent.SHIFT_MASK ) );
		}
		public boolean isEnabled() {
			return playerShip.getAutoImpulseOn();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setAutoImpulseOn(false);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"impulse_up");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_COMMA , KeyEvent.SHIFT_MASK ) );
		}
		public boolean isEnabled() {
			return playerShip.getImpulseFactor()<playerShip.getMaxImpulseFactor();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setImpulseUpDown(true);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"impulse_down");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_PERIOD , KeyEvent.SHIFT_MASK ) );
		}
		public boolean isEnabled() {
			return playerShip.getImpulseFactor()>0.25;
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setImpulseUpDown(false);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"impulse_start");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_E , 0 ) );
		}
		public boolean isEnabled() {
			return !playerShip.getImpulseOn();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setImpulseOn(true);
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"warp_auto");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_A , 0) );
		}
		public boolean isEnabled() {
			return !playerShip.getAutoWarpOn();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setAutoWarpOn(true);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"warp_man");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_M , 0) );
		}
		public boolean isEnabled() {
			return playerShip.getAutoWarpOn();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setAutoWarpOn(false);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"warp_up");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_COMMA , 0) );
		}
		public boolean isEnabled() {
			return playerShip.getWarpFactor()<playerShip.getMaxWarpFactor();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setWarpUpDown(true);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"warp_down");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_PERIOD , 0 ) );
		}
		public boolean isEnabled() {
			return playerShip.getWarpFactor()>1;
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setWarpUpDown(false);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"warp_start");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_ENTER , 0 ) );
		}
		public boolean isEnabled() {
			return !playerShip.getWarpOn();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setWarpOn(true);
		}
	});


	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"stop");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_X ,0) );
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.stop();
		}
	});

	// GUNS!!!!!

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"phaser_up");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_OPEN_BRACKET ,0) );
		}
		public boolean isEnabled() {
			return playerShip.getPhaserIntensityInt()<playerShip.getMaxPhaserIntensity();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setPhaserIntensityUpDown(true);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"phaser_down");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_CLOSE_BRACKET ,0) );
		}
		public boolean isEnabled() {
			return playerShip.getPhaserIntensityInt()>1;
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setPhaserIntensityUpDown(false);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"phaser_fire");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_P ,0) );
		}
		public void actionPerformed(ActionEvent e) {
			shipFirePhaser();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"torpedo_up");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_OPEN_BRACKET ,KeyEvent.SHIFT_MASK) );
		}
		public boolean isEnabled() {
			return playerShip.getTorpedoSalvoInt()<playerShip.getMaxTorpedoSalvo();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setTorpedoSalvoUpDown(true);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"torpedo_down");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_CLOSE_BRACKET ,KeyEvent.SHIFT_MASK) );
		}
		public boolean isEnabled() {
			return playerShip.getTorpedoSalvoInt()>1;
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setTorpedoSalvoUpDown(false);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"torpedo_fire");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_T ,0) );
		}
		public void actionPerformed(ActionEvent e) {
			shipFireTorpedo();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"shields_up");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_U ,0) );
		}
		public boolean isEnabled() {
			return !playerShip.getShieldsUp();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.changeShields();
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"shields_down");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_U ,0) );
		}
		public boolean isEnabled() {
			return playerShip.getShieldsUp();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.changeShields();
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"tractor");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_NUMBER_SIGN ,0) );
		}
		public boolean isEnabled() {
			return playerShip.getTractoredShip()==null;
		}
		public void actionPerformed(ActionEvent e) {
			shipTractorShip();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"tractor_off");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_NUMBER_SIGN ,0) );
		}
		public boolean isEnabled() {
			return playerShip.getTractoredShip()!=null;
		}
		public void actionPerformed(ActionEvent e) {
			shipTractorShip();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"zoomin");
		}
		public boolean isEnabled() {
			return canZoom(true);
		}
		public void actionPerformed(ActionEvent e) {
			zoom(true);
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"zoomout");
		}
		public boolean isEnabled() {
			return canZoom(false);
		}
		public void actionPerformed(ActionEvent e) {
			zoom(false);
		}
	});

	actions.add(new SelectAction(SELECT_ALL,"select_all","All"));
	actions.add(new SelectAction(SELECT_ENEMIES,"select_enemies","Enemies"));
	actions.add(new SelectAction(SELECT_MANUAL,"select_manual","Manual"));

	actions.add(new AbstractAction("Beam Team Down") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"beam_down");
		}
		public void actionPerformed(ActionEvent e) {
			shipBeamDown();
		}
	});

	actions.add(new AbstractAction("Beam Team Up") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"beam_up");
		}
		public void actionPerformed(ActionEvent e) {
			shipBeamUp();
		}
	});

	actions.add(new AbstractAction("Dock With Starbase") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"dock");
		}
		public void actionPerformed(ActionEvent e) {
			shipDock();
		}
	});

	actions.add(new AbstractAction("Board Ship") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"board");
		}
		public void actionPerformed(ActionEvent e) {
			shipBoard();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"scan_up");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_EQUALS ,0) );
		}
		public boolean isEnabled() {
			return playerShip.getSensorScan()<playerShip.getMaxScan();
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setScan(true);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"scan_down");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_MINUS ,0) );
		}
		public boolean isEnabled() {
			return playerShip.getSensorScan()>0;
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.setScan(false);
			updateGUI();
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"ships_up");
		}
		public boolean isEnabled() {
			return canScrollShips(true,true);
		}
		public void actionPerformed(ActionEvent e) {
			scrollShips(true,true);
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"ships_down");
		}
		public boolean isEnabled() {
			return canScrollShips(true,false);
		}
		public void actionPerformed(ActionEvent e) {
			scrollShips(true,false);
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"base_up");
		}
		public boolean isEnabled() {
			return canScrollShips(false,true);
		}
		public void actionPerformed(ActionEvent e) {
			scrollShips(false,true);
		}
	});

	actions.add(new AbstractAction() {
		{
			putValue(Action.ACTION_COMMAND_KEY,"base_down");
		}
		public boolean isEnabled() {
			return canScrollShips(false,false);
		}
		public void actionPerformed(ActionEvent e) {
			scrollShips(false,false);
		}
	});

	actions.add(new AbstractAction("Use Reserve Energy") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"reserve");
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.useReserve();
			updateGUI();
		}
	});

	actions.add(new AbstractAction("Divert Phaser Energy") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"divert");
		}
		public void actionPerformed(ActionEvent e) {
			playerShip.divertPhaser();
			updateGUI();
		}
	});

	// GAME CONTROL

	actions.add(new AbstractAction("Heads Up Display") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"headsup");
		}
		public void actionPerformed(ActionEvent e) {
			headsUpDisplay();
		}
	});

	actions.add(new AbstractAction("Pause") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"pause");
		}
		public void actionPerformed(ActionEvent e) {
			pause();
		}
	});

	actions.add(new AbstractAction("End") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"endgame");
		}
		public void actionPerformed(ActionEvent e) {
			closeCurrentMission();
		}
	});

	actions.add(new AbstractAction("Quit") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"quit");
		}
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	});

	actions.add(new AbstractAction("Help") {
		{
			putValue(Action.ACTION_COMMAND_KEY,"help");
			putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_F1 , 0) );
		}
		public void actionPerformed(ActionEvent e) {
			help();
		}
	});

	actions.add(new EvasiveAction(1,"evasive 1","Sequence Alpha 7"));
	actions.add(new EvasiveAction(2,"evasive 2","Sequence Omega 8"));
	actions.add(new EvasiveAction(3,"evasive 3","Sequence Delta 9"));

	for (int c=0;c<2; c++) {
		final int value=c;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"ships_sort "+value);
			}
			public boolean isEnabled() {
				return getShipsSort()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				setShipsSort(value);
			}
		});
	}

	for (int c=0;c<2; c++) {
		final int value=c;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"base_sort "+value);
			}
			public boolean isEnabled() {
				return getBaseSort()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				setBaseSort(value);
			}
		});
	}

	for (int c=0;c<4; c++) {
		final int value=c;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"ships_show "+value);
			}
			public boolean isEnabled() {
				return getShipsShow()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				setShipsShow(value);
			}
		});
	}

	for (int c=0;c<4; c++) {
		final int value=c;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"base_show "+value);
			}
			public boolean isEnabled() {
				return getBaseShow()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				setBaseShow(value);
			}
		});
	}

	for (int c=0;c<11; c++) {
		final int value=c;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"target "+value);
			}
			public boolean isEnabled() {
				return playerShip.getTarget()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				playerShip.setTarget(value);
				updateGUI();
			}
		});
	}

	for (int c=0;c<11; c++) {
		final int value=c;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"fix "+value);
			}
			public boolean isEnabled() {
				return playerShip.getFixPriority()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				playerShip.setFixPriority(value);
				updateGUI();
			}
		});
	}

	for (int c=0;c<9; c++) {
		final int value=c+1;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"warp_speed "+value);
				putValue(Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke( KeyEvent.VK_0+value ,0) );
			}
			public boolean isEnabled() {
				return value != playerShip.getWarpFactor();
			}
			public void actionPerformed(ActionEvent e) {
				playerShip.setWarpFactor(value);
				updateGUI();
			}
		});
	}

	for (int c=0;c<4; c++) {
		final int value=c+1;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"impulse_speed "+value);
			}
			public boolean isEnabled() {
				return (int)(playerShip.getImpulseFactor()*4) !=value;
			}
			public void actionPerformed(ActionEvent e) {
				playerShip.setImpulseFactor(value);
				updateGUI();
			}
		});
	}

	for (int c=0;c<3; c++) {
		final int value=c+1;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"phaser_intensity "+value);
			}
			public boolean isEnabled() {
				return playerShip.getPhaserIntensityInt()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				playerShip.setPhaserIntensity(value);
				updateGUI();
			}
		});
	}

	for (int c=0;c<3; c++) {
		final int value=c+1;
		actions.add(new AbstractAction() {
			{
				putValue(Action.ACTION_COMMAND_KEY,"torpedo_salvo "+value);
			}
			public boolean isEnabled() {
				return playerShip.getTorpedoSalvoInt()!=value;
			}
			public void actionPerformed(ActionEvent e) {
				playerShip.setTorpedoSalvo(value);
				updateGUI();
			}
		});
	}

        return actions;

    }

    public void updateEvasiveIndicators(int value) {
	for (int c=0;c<evasiveListeners.size();c++) {
		EvasiveAction sl = (EvasiveAction)evasiveListeners.elementAt(c);
		sl.newEvasiveState(value);
	}
    }

    public URL getMissionsRootURL() {
	return rooturl;
    }

    public void setDelay(int d) {

	DELAY=d;

    }

    public int getDelay() {

	return DELAY;

    }

    public int getSelect() {

	return select;

    }

    public void addRescueListener(RescueListener g) {

	gui = g;

	if (loadgameonstartup) { loadCurrentMission(); pause=false; }

	//this.start();

    }

    public void run() {

	// Lower this thread's priority

	//Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
	long startTime;

	// This is the main loop.

	while (true) {

	    if (!pause) {

		startTime = System.currentTimeMillis();

		//try { // dont know why the fuck this was here

		    updateGame();
		    gui.update();

		//} catch (Exception e) {}

		try {

		    int d = (int)Math.max(0, DELAY-(System.currentTimeMillis() - startTime) );
		    //System.out.print(d+"\n");
		    Thread.sleep(d);

		} catch (InterruptedException e) {}


	    }
	    else {

		try {

		    synchronized(this) {

			wait();
			pause=false;

		    }

		} catch (InterruptedException e) {}


	    }

	}
    }

    public void updateGUI() {
	if (pause) { gui.update(); }
    }

    // ######################################################## Mission file stuff

    public void setMission(MissionLoader ml) {

	ml.loadImagesForGame();

	currentMission = ml;
    }
    public MissionLoader getMission() {
	return currentMission;
    }
    public void loadCurrentMission() {

	try {

	    torpedos = new Vector();

	    SpaceObjects = currentMission.getSpaceObjects(torpedos); // try needed for this

	    playerShip = (Ship)SpaceObjects[SpaceObjects.length-1]; // ((MissionObject)currentMission.getPlayerObject()).getPlayer();
	    playerShip.setPlayer(true,this);

	    selectedObjects = new Vector();

	    shiplist = currentMission.getShipList();
	    baselist = currentMission.getBaseList();
	    shipinfo = currentMission.getShipInfoPanel();

	    shiplist.newGame();
	    baselist.newGame();
	    shipinfo.setSpaceObject(null); // same as newgame()

	    setSelect(SELECT_ENEMIES);

	    gui.newGame(currentMission);

	    // unpause the game
	    synchronized(this) {
		this.notify();
	    }

	    JukeBox.play("music");

	    JukeBox.play("welcome");

            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keymap);
	}
	catch (Exception e) {

	    // YURA:TODO should be some sort of graphical error shown
	    System.out.print("error creating game\n");

	    e.printStackTrace();

	}

    }
    public void closeCurrentMission() {
	pause=true;
	playerShip = null;
	SpaceObjects = null;

	gui.closeGame();

	JukeBox.stop("background"); // if the ship is in motion we must stop this sound too

	JukeBox.stop("music");

        KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keymap);
    }

    // ########################################################### MAIN GUI STUFF

    public boolean getMode() {
	return currentMission.getExternal();
    }

    public void setMode(boolean a) {
	currentMission.setExternal(a);
    }

    public void setShipsSort(int a) {
	shiplist.setSort(a);
	updateGUI();
    }

    public void setBaseSort(int a) {
	baselist.setSort(a);
	updateGUI();
    }

    public void setShipsShow(int a) {
	shiplist.setShow(a);
	updateGUI();
    }

    public void setBaseShow(int a) {
	baselist.setShow(a);
	updateGUI();
    }

    public int getShipsSort() {
	return shiplist.getSort();
    }

    public int getBaseSort() {
	return baselist.getSort();
    }

    public int getShipsShow() {
	return shiplist.getShow();
    }

    public int getBaseShow() {
	return baselist.getShow();
    }

    public void scrollShips(boolean s,boolean up) {

	if (s) {
	    shiplist.scroll(up);
	}
	else {
	    baselist.scroll(up);
	}

	updateGUI();

    }

    public boolean canScrollShips(boolean s,boolean up) {

	if (s) {
	    return shiplist.canScroll(up);
	}
	else {
	    return baselist.canScroll(up);
	}

    }

    public void headsUpDisplay() {

	if (heads==true) {
	    heads=false;
	}
	else {
	    heads=true;
	}

	updateGUI();

    }

    public boolean mapUpdating() {

	long newtime = System.currentTimeMillis();

	timetotal = timetotal + (newtime-oldtime);
	counter++;

	if (counter >= Math.max(Math.round(fps),1) ) { // = 50

	    // math.round gives a MAX_LONG value, the value it got stuck on is 9.223372036854776E16 (MAX_LONG/100d)
	    fps = (Math.round( (1000d/( timetotal / counter ))*100d ) )/100d;

	    if (fps>100) { fps=100; } // This fixis that fps bug

	    timetotal=0;
	    counter=0;

	}

	oldtime=newtime;

	return (playerShip!=null);

    }

    public void pause() {

	if (pause==true && SpaceObjects!=null) {

	    oldtime=System.currentTimeMillis();

	    // unpause the game
	    synchronized(this) {
		this.notify();
	    }

	}
	else {
	    pause=true;
	}

	gui.update();

    }

    public boolean getPause() {
	return pause;
    }

    public void zoom(boolean in) {

	if (in && canZoom(true) ) { zoom = zoom*2; }
	else if (!in && canZoom(false)) { zoom = zoom/2; }
	else { return; }

	makeSky();

	updateGUI();

    }

    public boolean canZoom(boolean in) {

	if (in) {

	    if (zoom>=8) { return false; }
	    else { return true; }

	}
	else {

	    if (zoom<=0.0625) { return false; }
	    else { return true; }

	}


    }

    public void setPanelSize(int w,int h) {

	panelWidth=w;
	panelHeight=h;

	makeSky();

    }

    public void makeSky() {

	sky = new Point2D.Double[(int)Math.round(30/zoom)];

	//System.out.print("nos "+30/zoom+"\n");

	for(int i=0; i< sky.length ; i++) {

	    sky[i] = new Point2D.Double((Math.random() * panelWidth),(Math.random() * panelHeight)) ;

	}

    }

    int shifts=0;
    public void drawStars(Graphics g) {

	Point shift = new Point(0,0);

	if (sky==null) {

	    makeSky();

	}
	else if (playerShip.getImpulseOn() || playerShip.getWarpOn()) {

	    shift = playerShip.moveTo();

		shift.x = (int)(shift.x*(     (double)panelWidth/((double)windowHeight/zoom)    ));
		shift.y = (int)(shift.y*(     (double)panelHeight/((double)windowHeight/zoom)    ));


	}

	// not very god fix for stares 
	int newshifts = Math.abs(shift.x) + Math.abs(shift.y);
	if (shifts>panelWidth && newshifts <panelWidth) {
	    makeSky(); //System.out.print("making sky\n");
	}

	shifts = newshifts;

	g.setColor( Color.WHITE );

	for (int i = 0; i < sky.length ; i++) {

	    //g.fillRect(sky[i].x,sky[i].y, 2,2);

	    g.drawLine((int)sky[i].x-(shift.x/2),(int)sky[i].y-(shift.y/2),(int)(sky[i].x+shift.x),(int)(sky[i].y+shift.y) );

	}

    }

    public void drawShipsBig(Graphics gi) {// panelWidth/(windowWidth/zoom)

	Graphics2D g = (Graphics2D)gi.create();

	//System.out.print("zoom: "+zoom+"\n");

	int mapWindowWidth=(int)(windowWidth/zoom); // this is the virtual window onto the big map
	int mapWindowHeight=(int)(windowHeight/zoom); // this is the virtual window onto the big map

	g.scale((double)panelWidth/mapWindowWidth,(double)panelHeight/mapWindowHeight);

	int x=(int)playerShip.getX()-(mapWindowWidth/2);
	int y=(int)playerShip.getY()-(mapWindowHeight/2);

	g.translate( -x,-y );

	Rectangle screen = new Rectangle(x-1000,y-1000,mapWindowWidth+2000,mapWindowHeight+2000);
	// Vector v = playerShip.getSelectedObjects();

	for (int i = 0; i < SpaceObjects.length; i++) {

	    if ( screen.contains(SpaceObjects[i]) ) {

		if (selectedObjects.contains(SpaceObjects[i])) {
		    SpaceObjects[i].drawBigShape(g,true);
		}
		else {
		    SpaceObjects[i].drawBigShape(g,false);
		}

	    }

	}

	// now draw all the torpedos from the torpedos vector (new torpedo vector system)
	for (int i = 0; i < torpedos.size(); i++) {

	    SpaceObject t = (SpaceObject)torpedos.elementAt(i);

	    if ( screen.contains(t) ) {

		t.drawBigShape(g,false);

	    }

	}

	g.setColor(Color.DARK_GRAY);
	g.drawOval(x+(mapWindowWidth/2)-DISTANCE_PHASERS,y+(mapWindowHeight/2)-DISTANCE_PHASERS,DISTANCE_PHASERS*2,DISTANCE_PHASERS*2);

	if (heads) {

	    gi.setColor( Color.WHITE );
	    gi.drawString("Zoom: x"+zoom, 10, 20 );
	    gi.drawString("Speed: "+playerShip.getSpeedMofC()+ " x c", 10, 32 );
	    gi.drawString("Direction: "+ (int)( Math.toDegrees(playerShip.getDirection()) ), 10, 44 ); // Math.round
	    gi.drawString("Location: x="+playerShip.x+" y="+playerShip.y, 10, 56 );

	    if (playerShip.getEvasive()!=0) {gi.drawString("Evasive Maneuver: "+playerShip.getEvasive(), 10, 68 ); }
	    else if (playerShip.getImpulseOn() && playerShip.getAutoImpulseOn()) { gi.drawString("Distance to impulse target: "+(int)playerShip.distance(playerShip.getAutoImpulse()), 10, 68 ); }
	    else if (playerShip.getWarpOn() && playerShip.getAutoWarpOn()) { gi.drawString("Distance to warp target: "+(int)playerShip.distance(playerShip.getAutoWarp()), 10, 68 ); }

	}

	if (pause) { gi.drawString("Pause", panelWidth-40, 20 ); }

	else {

	    String fpsString = String.valueOf(fps);

	    //if (fpsString.startsWith("9.2233") ) { test=true; }

	    while (fpsString.length() < 5) {
		fpsString = fpsString+"0";
	    }

	    gi.drawString("fps: "+fpsString, panelWidth-55, 20 );

	}

    }

    public void drawShipsSmall(Graphics g,int w,int h) {

	int scan=0;

	if (playerShip.getSystem(6)==0) {
	    scan = playerShip.getSensorScan();
	}

	for (int i = 0; i < SpaceObjects.length; i++) {

	    ((ImageObject)SpaceObjects[i]).drawSmallShape(g, (double)w/(double)WIDTH,(double)h/(double)HEIGHT,scan);

	}

    }

    public int getScan() {
	return playerShip.getSensorScan();
    }

    // ############################################################# GAME UPDATE

    public void updateGame() {

	boolean alivefirst = playerShip.isAlive();

	int z = SpaceObjects.length-1;

	for (int i = 0; i < SpaceObjects.length ; i++) {

	    ImageObject a = SpaceObjects[i];

	    a.advance();

	    if (a.x<0) { a.x=WIDTH+a.x; }
	    if (a.y<0) { a.y=HEIGHT+a.y; }

	    if (a.x>WIDTH) { a.x=a.x-WIDTH; }
	    if (a.y>HEIGHT) { a.y=a.y-HEIGHT; }

	    if (select==SELECT_ENEMIES && a instanceof MannedObject && ((MannedObject)a).enemyOf(z) && !selectedObjects.contains(a) && i!=z) {

		selectedObjects.add(a);

	    }

	}

	// new torpedo vector system
	for (int i = 0; i < torpedos.size(); i++) {

	    ((SpaceObject)torpedos.elementAt(i)).advance();

	}



	for (int i = 0; i < selectedObjects.size() ; i++) {

	    if ( !(((MannedObject)selectedObjects.elementAt(i)).isAlive()) ) {

		selectedObjects.removeElementAt(i);
		i--;

	    }
	    else if (select==SELECT_ENEMIES && !(((MannedObject)selectedObjects.elementAt(i)).enemyOf(z)) ) {

		selectedObjects.removeElementAt(i);
		i--;

	    }

	}

	Point ai = playerShip.getAutoImpulse();
	Point aw = playerShip.getAutoWarp();

	if (ai instanceof MannedObject && !(((MannedObject)ai).isAlive()) ) {

		//if (playerShip.getImpulseOn() && playerShip.getAutoImpulseOn()) { }
		playerShip.setAutoImpulse( new Point(ai.x,ai.y) );

	}
	if (aw instanceof MannedObject && !(((MannedObject)aw).isAlive()) ) {

		//if (playerShip.getWarpOn() && playerShip.getAutoWarpOn()) { }
		playerShip.setAutoWarp( new Point(aw.x,aw.y) );
	}


	if ((sky!=null) &&(playerShip.getImpulseOn() || playerShip.getWarpOn())) {

	    Point shift = playerShip.moveTo();

	    for (int i = 0; i < sky.length ; i++) {

		Point2D.Double cp = sky[i];

		try { // for some totally unknown reason this bit throws NullPointerExceptions sometimes

		    cp.x = cp.x - (shift.x*(     (double)panelWidth/((double)windowHeight/zoom)    ));
		    cp.y = cp.y - (shift.y*(     (double)panelHeight/((double)windowHeight/zoom)    ));

		    if (cp.x < 0 || cp.x > panelWidth) {

			//sky[i] = new Point2D.Double(   (shift.x < 0)?    ((Math.random() * shift.x))   :  (panelWidth-(Math.random() * shift.x))     ,((Math.random() * panelHeight))) ;

			cp.x = (shift.x < 0)?    ((Math.random() * shift.x))   :  (panelWidth-(Math.random() * shift.x));
			cp.y = ((Math.random() * panelHeight));

		    }
		    else if (cp.y < 0 || cp.y > panelHeight) {

			//sky[i] = new Point2D.Double( ((Math.random() * panelWidth)),   (shift.y < 0)?    ((Math.random() * shift.y))   :  (panelHeight-(Math.random() * shift.y)) ) ;

			cp.x = ((Math.random() * panelWidth));
			cp.y = (shift.y < 0)?    ((Math.random() * shift.y))   :  (panelHeight-(Math.random() * shift.y));

		    }

		} catch(Exception e) { System.out.print("A star is lost! This should not happen ever!\n"); }

	    }


	}

	if (alivefirst && !playerShip.isAlive()) {

	    gui.dead();

	}

    }

    // #################################################### get methods

    public Ship getShip() {
	return playerShip;
    }

    // this is only used by the ships list panel
    public ImageObject[] getSpaceObjects() {

	//return new Vector( Arrays.asList(SpaceObjects) );

	return SpaceObjects;

    }

    public int getShipNumbers(int a) {

	int num=0;
	// YURA:TODO THIS IS SLOW, find a better way
	//int z = Arrays.binarySearch(SpaceObjects,playerShip); // indexOf
	int z = SpaceObjects.length-1;

	if (a==1) { // friendlies

	    for (int i = 0; i < SpaceObjects.length-1 ; i++) {

		if ( (SpaceObjects[i] instanceof Ship) && (((Ship)SpaceObjects[i]).isAlive()) && ((Ship)SpaceObjects[i]).friendOf(z) ) {
		    num++;
		}

	    }

	    //if ( playerShip.isAlive() ) { num--; } // YURA: remove 1 so my own ship is not counted

	}
	else if (a==2) { // enemies

	    for (int i = 0; i < SpaceObjects.length-1 ; i++) {

		if ( (SpaceObjects[i] instanceof Ship) && (((Ship)SpaceObjects[i]).isAlive()) && ((Ship)SpaceObjects[i]).enemyOf(z) ) {
		    num++;
		}

	    }

	}
	else if (a==3) { // distroyed

	    for (int i = 0; i < SpaceObjects.length-1 ; i++) {

		if ( (SpaceObjects[i] instanceof Ship) && !(((Ship)SpaceObjects[i]).isAlive()) ) {
		    num++;
		}

	    }

	}

	return num;
    }

    public int getBaseNumbers(int a) {

	int num=0;

	if (a==1) { // rescued

	    for (int i = 0; i < SpaceObjects.length-1 ; i++) {

		if ( (SpaceObjects[i] instanceof Base) && (((Base)SpaceObjects[i]).isAlive()) ) {
		    num = num + (((Base)SpaceObjects[i]).getPeople());
		}

	    }

	}
	else if (a==2) { // distroyed

	    for (int i = 0; i < SpaceObjects.length-1 ; i++) {

		if ( (SpaceObjects[i] instanceof Planet) && !(((Planet)SpaceObjects[i]).isAlive()) && ((Planet)SpaceObjects[i]).getPeople()!=0 ) {
		    num++;
		}

	    }

	}
	else if (a==3) { // remaining

	    for (int i = 0; i < SpaceObjects.length-1 ; i++) {

		if ( (SpaceObjects[i] instanceof Planet) && (((Planet)SpaceObjects[i]).isAlive()) && ((Planet)SpaceObjects[i]).getPeople()!=0 ) {
		    num++;
		}

	    }

	}

	return num;
    }

    // ############################################################### PLAYER SHIP COMMANDS

    public void newSelection(int type, ImageObject so) {

	if (type==1) { // signal came from ships list

		baselist.clearSelection();

	}
	else {

		shiplist.clearSelection();

	}

	//YURA:TODO make all destinations = to so;

	playerShip.setAutoWarp(so);
	playerShip.setAutoImpulse(so);

	shipinfo.setSpaceObject(so);

	updateGUI();

    }

    public void shipDock() {

	if (!playerShip.getShieldsUp()) {

	  for (int i = 0; i < SpaceObjects.length-1 ; i++) {

	    if (SpaceObjects[i].hasSelected(playerShip) && ((SpaceObjects[i] instanceof Base) || (SpaceObjects[i] instanceof Ship)) && ((MannedObject)SpaceObjects[i]).doYouLike(playerShip) ) {

		playerShip.shipDockWith( ((MannedObject)SpaceObjects[i]) );
		return;

	    }

	  }

	}

    }

    public void shipBoard() {

	if (!playerShip.getShieldsUp()) {

	  for (int i = 0; i < SpaceObjects.length-1 ; i++) {

	    if (SpaceObjects[i].hasSelected(playerShip) && (SpaceObjects[i] instanceof Ship ) && ((Ship)SpaceObjects[i]).doYouLike(playerShip)  ) {

		int a = playerShip.getNoAwayTeams();
		int b = ((Ship)SpaceObjects[i]).getNoAwayTeams();

		// YURA:TODO also away teams need to be sent to take over it, and then swapped after the boad

		// YURA: TODO:this should check if the tractor beam is on!
		playerShip.TractorShip(null);

		//int j = SpaceObjects.indexOf(playerShip);
		// YURA:TODO this is slow
		//int j = Arrays.binarySearch(SpaceObjects,playerShip);
		int j = SpaceObjects.length - 1;

		Ship oldship = playerShip;

		playerShip.setPlayer(false,null);
		playerShip = (Ship)SpaceObjects[i];
		playerShip.setPlayer(true,this);

		oldship.sendPeople(playerShip);

		//SpaceObjects.remove(playerShip);
		//SpaceObjects.remove(oldship);

		// YURA the likes have to be reset for the old ship

		//SpaceObjects.insertElementAt(oldship,i);
		//SpaceObjects.insertElementAt(playerShip,j);

		// YURA:TODO this should be auto done
		//oldship.setShields(true);
		oldship.changeShields();

		// the number is swapped for the ships;
		SpaceObjects[i]=oldship; oldship.setNumber(i);
		SpaceObjects[j]=playerShip; playerShip.setNumber(j);


		selectedObjects.remove(playerShip);

		//selectedObjects.add(oldship); // 1 of these
		playerShip.TractorShip(oldship);

		playerShip.setNoAwayTeams(a);
		((Ship)SpaceObjects[i]).setNoAwayTeams(b);

		shiplist.setShow( shiplist.getShow() ); // makes sure that you are not urself in the shipslist
		shipinfo.setSpaceObject(null); // just clears the shipInfo incase its my ship thats selected

		return;

	    }

	  }

	}

    }

    public void shipBeamDown() {

	if (!playerShip.getShieldsUp() && playerShip.getSystem(9)==0) {

	  for (int i = 0; i < SpaceObjects.length-1 ; i++) {

	    if ( SpaceObjects[i] instanceof MannedObject && ((MannedObject)SpaceObjects[i]).hasSelected(playerShip) ) {

		((MannedObject)SpaceObjects[i]).beamTeamFrom(playerShip);
		return;

	    }

	  }

	}

    }

    public void shipBeamUp() {

	if (!playerShip.getShieldsUp() && playerShip.getSystem(9)==0) {

	  for (int i = 0; i < SpaceObjects.length-1 ; i++) {

	    if ( SpaceObjects[i] instanceof MannedObject && ((MannedObject)SpaceObjects[i]).hasSelected(playerShip) ) {

		playerShip.beamTeamFrom( (MannedObject)SpaceObjects[i] );
		return;

	    }

	  }

	}

    }

    public void shipFireTorpedo() {

	MannedObject p = getClosestSelectedObject();

	if (p!=null && playerShip.distance(p) <= DISTANCE_TORPEDO ) {

	    playerShip.FireTorpedo(p);

	}

    }

    public void shipFirePhaser() {

	MannedObject p = getClosestSelectedObject();

	if (p!=null && playerShip.distance(p) <= DISTANCE_PHASERS ) {

	    playerShip.FirePhaser(p);

	}

    }

    public void shipTractorShip() {

	if (playerShip.getTractoredShip()==null) {

	    SpaceObject p = getClosestSelectedObject();

	    if (p != null && p instanceof Ship && playerShip.distance(p) <= DISTANCE_TRACTOR ) {

		playerShip.TractorShip( (Ship)p );
	    }

	}
	else {

	    playerShip.TractorShip(null);

	}

    }


    public MannedObject getClosestSelectedObject() {

	int a=0;
	int b=1000000;
	MannedObject p=null;

	for (int i = 0; i < selectedObjects.size() ; i++) {

	    a = (int)((MannedObject)selectedObjects.elementAt(i)).distance(playerShip)  -  ((MannedObject)selectedObjects.elementAt(i)).getSize();

	    if (a < b) { b=a; p=(MannedObject)selectedObjects.elementAt(i); }

	}

	return p;

    }

    public void setSelect(int a) {

	select = a;

	for (int c=0;c<selectListeners.size();c++) {
		SelectAction sl = (SelectAction)selectListeners.elementAt(c);
		sl.newSelectState(select);
	}


	selectedObjects.removeAllElements();

	if (select==SELECT_ALL) {

	    for (int i = 0; i < SpaceObjects.length-1; i++) {

		if ( SpaceObjects[i] instanceof MannedObject && (((MannedObject)SpaceObjects[i]).isAlive()) ) {
		    selectedObjects.add( SpaceObjects[i] );
		}

	    }


	}
	else if (select==SELECT_ENEMIES) {

	    int z = SpaceObjects.length-1;

	    for (int i = 0; i < SpaceObjects.length-1 ; i++) {

		if ( SpaceObjects[i] instanceof MannedObject && ((MannedObject)SpaceObjects[i]).isAlive() && ((MannedObject)SpaceObjects[i]).enemyOf(z) ) {
		    selectedObjects.add(SpaceObjects[i]);
		}

	    }

	}

    }


    // ############################################ COMMANDS FROM MAPS (BIG AND SMALL)

    public void selectShips(int x,int y) {

	Point p = new Point(  playerShip.x +   (int)(    ((x-(panelWidth/2))*((double)windowWidth/panelWidth))    /zoom), playerShip.y + (int)(      ((y-(panelHeight/2))*((double)windowHeight/panelHeight))    /zoom)    );

	for (int i = SpaceObjects.length-2; i >= 0; i--) {

	    if (SpaceObjects[i].hasSelected(p) && SpaceObjects[i] instanceof MannedObject) {

		if (select!=SELECT_MANUAL || !(selectedObjects.contains( SpaceObjects[i] )) ) {

			selectedObjects.removeAllElements();
			selectedObjects.add( SpaceObjects[i] );

		}
		else {

			selectedObjects.removeAllElements();

		}

		select=SELECT_MANUAL;
		break;
	    }

	}



	updateGUI();

    }

    public void shipManImpulse(double angle) {

	playerShip.setAutoImpulseOn(false);

	playerShip.setManImpulse( angle );
	playerShip.setImpulseOn(true);

    }

    public void shipManWarp(double angle) {

	playerShip.setAutoWarpOn(false);

	playerShip.setManWarp( angle );
	playerShip.setWarpOn(true);

    }

    public void shipWarp(int x,int y, int w, int h) {

	playerShip.setAutoWarp( new Point((int)((x/(double)w)*WIDTH) , (int)((y/(double)h)*HEIGHT)) );

	updateGUI();

    }

    public void shipImpulse(int x,int y) {

	Point p = new Point(  playerShip.x +   (int)(    ((x-(panelWidth/2))*((double)windowWidth/panelWidth))    /zoom), playerShip.y + (int)(      ((y-(panelHeight/2))*((double)windowHeight/panelHeight))    /zoom)    );

	playerShip.setAutoImpulse( p );

	updateGUI();

    }

    public void shipWarpOn() {

	playerShip.setWarpOn(true);

    }

    public void shipImpulseOn() {

	playerShip.setImpulseOn(true);

    }

    public void help() {

	try {

		File file = new File("help/index.htm");
		BrowserLauncher.openURL("file://" + file.getAbsolutePath() );

	}
	catch (Exception e) {

		System.out.print("cant open help: "+e.getMessage()+"\n");

	}

    }


    public static double getTurn(double angle, double direction) {

	double tt;

	// if the diference in angles is more then 180 then change the move to angle to a negative 1
	if (Math.abs(direction-angle) > Math.toRadians(180)) {

		if (angle > direction) { // has to turn anti-clockwise, so it needs a small number
		    tt = -(Math.toRadians(360)-angle); // 270 -> -90
		}
		else { // has to turn clockwise, so we need to give it a big number
		    tt = Math.toRadians(360)+angle; // 45 -> 405
		}
	}
	else { tt = angle; } // 90 -> 90

	return tt - direction;

    }

    public List getActions() {
	return keymap.getActions();
    }
    public Keymap getKeymap() {
        return keymap;
    }

    public Action getAction(String name) {
        List actions = getActions();

	for (int i=0;i<actions.size();i++) {

		Action action = (Action)actions.get(i);

		if (  name.equals((String)action.getValue( Action.ACTION_COMMAND_KEY ))  ) {

			return action;
		}
	}

	throw new RuntimeException("unknown command: \""+name+"\"");

    }


}
